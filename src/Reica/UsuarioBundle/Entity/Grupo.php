<?php

namespace Reica\UsuarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\Group as BaseGroup;

/**
 * Grupo
 *
 * @ORM\Table(name="us_grupo")
 * @ORM\Entity
 */
class Grupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;
    /**
     * @var string $name
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;
    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */

    private $descripcion;


    /**
     * @var integer $usuarios
     *
     * @ORM\ManyToMany(targetEntity="Reica\UsuarioBundle\Entity\Usuario", mappedBy="grupos")
     */
    protected  $usuarios;
    /**
     * @var integer $acciones
     *
     * @ORM\ManyToMany(targetEntity="Reica\UsuarioBundle\Entity\Role", inversedBy="grupos", cascade={"persist"})
     * @ORM\JoinTable(name="us_grupo_rol")
     */
    protected  $roles;

    function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add roles
     *
     * @param \Reica\UsuarioBundle\Entity\Role $roles
     * @return Grupo
     */
    public function addRole(\Reica\UsuarioBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Reica\UsuarioBundle\Entity\Role $roles
     */
    public function removeRole(\Reica\UsuarioBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Grupo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Grupo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add usuario
     *
     * @param \Reica\UsuarioBundle\Entity\Usuario $usuario
     * @return Grupo
     */
    public function addUsuario(\Reica\UsuarioBundle\Entity\Usuario $usuario)
    {
        $this->usuario[] = $usuario;
    
        return $this;
    }

    /**
     * Remove usuario
     *
     * @param \Reica\UsuarioBundle\Entity\Usuario $usuario
     */
    public function removeUsuario(\Reica\UsuarioBundle\Entity\Usuario $usuario)
    {
        $this->usuario->removeElement($usuario);
    }

    /**
     * Get usuario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
    public  function __toString(){
        return $this->nombre;
    }
}