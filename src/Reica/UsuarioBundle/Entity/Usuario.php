<?php
namespace Reica\UsuarioBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;


/**
 * @ORM\Entity
 * @ORM\Table(name="us_usuario")
 */
class Usuario extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Reica\UsuarioBundle\Entity\Grupo",inversedBy="usuarios")
     * @ORM\JoinTable(name="us_usuario_grupo",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $grupos;
    /**
     * @ORM\ManyToMany(targetEntity="Reica\UsuarioBundle\Entity\Role",inversedBy="usuarios")
     * @ORM\JoinTable(name="us_usuario_rol",
     *      joinColumns={@ORM\JoinColumn(name="usuario_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="rol_id", referencedColumnName="id")}
     * )
     */
    protected $rolesPropios;

    /**
    * @var string
    *
    * @ORM\Column(name="nombre", type="string", length=255)
    */
    private $nombre;

    /**
    * @var string
    *
    * @ORM\Column(name="apellidoMaterno", type="string", length=255)
    */
    private $apellidoMaterno;

    /**
    * @var string
    *
    * @ORM\Column(name="apellidoPaterno", type="string", length=255)
    */
    private $apellidoPaterno;
    /**
    * @var string
    *
    * @ORM\Column(name="titulo", type="string", length=255)
    */
    private $titulo;
    /**
    * @var string
    *
    * @ORM\Column(name="cargo", type="string", length=255)
    */
    private $cargo;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
  public function __toString(){
      return $this->titulo." ".$this->nombre." ".$this->apellidoPaterno." ".$this->cargo;
  }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return \DateTime
     */

    public function getExpiresAt()
    {
        $this->expiresAt;
    }

    public function getRoles()
    {
        $rolesArray = [];
        $rolesPropios = [];
        if ($this->getGrupos()) {
        foreach ($this->getGrupos() as $group) {
            foreach ($group->getRoles() as $roles) {
                $rolesArray[] = $roles->getIdentificador();
            }
        }
    }

    $rolesP = $this->getRolesPropios();

    if (empty($rolesP)) {
        $rolesPropios[] = 'ROLE_USER';
    } else {
        foreach ($rolesP->toArray() as $rol) {
            $rolesPropios[] = $rol->getIdentificador();
        }
    }

    $roles = array_merge($rolesPropios, $rolesArray);

    return array_unique($roles);
}


    /**
     * Add grupos
     *
     * @param \Reica\UsuarioBundle\Entity\Grupo $grupos
     * @return Usuario
     */
    public function addGrupo(\Reica\UsuarioBundle\Entity\Grupo $grupos)
    {
        $this->grupos[] = $grupos;

        return $this;
    }

    /**
     * Remove grupos
     *
     * @param \Reica\UsuarioBundle\Entity\Grupo $grupos
     */
    public function removeGrupo(\Reica\UsuarioBundle\Entity\Grupo $grupos)
    {
        $this->grupos->removeElement($grupos);
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupos()
    {
        return $this->grupos;
    }

      /**
     * Add rolesPropios
     *
     * @param \Reica\UsuarioBundle\Entity\Role $rolesPropios
     * @return Usuario
     */
    public function addRolesPropio(\Reica\UsuarioBundle\Entity\Role $rolesPropios)
    {
        $this->rolesPropios[] = $rolesPropios;

        return $this;
    }

    /**
     * Remove rolesPropios
     *
     * @param \Reica\UsuarioBundle\Entity\Role $rolesPropios
     */
    public function removeRolesPropio(\Reica\UsuarioBundle\Entity\Role $rolesPropios)
    {
        $this->rolesPropios->removeElement($rolesPropios);
    }

    /**
     * Get rolesPropios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRolesPropios()
    {
        return $this->rolesPropios;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidoMaterno
     *
     * @param string $apellidoMaterno
     * @return Usuario
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get apellidoMaterno
     *
     * @return string
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set apellidoPaterno
     *
     * @param string $apellidoPaterno
     * @return Usuario
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get apellidoPaterno
     *
     * @return string
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Usuario
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     * @return Usuario
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }
}
