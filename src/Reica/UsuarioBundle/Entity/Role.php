<?php

namespace Reica\UsuarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reica\UsuarioBundle\Entity
 *
 * @ORM\Table(name="us_role")
 * @ORM\Entity
 */
class Role
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="identificador", type="string", length=255)
     */
    private $identificador;

    /**
     * @var string $description
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var integer $roles
     *
     * @ORM\ManyToMany(targetEntity="Reica\UsuarioBundle\Entity\Grupo", mappedBy="roles")
     * @ORM\JoinTable(name="us_grupo_rol")
     */
    private $grupos;
    /**
     * @var integer $roles
     *
     * @ORM\ManyToMany(targetEntity="Reica\UsuarioBundle\Entity\Usuario", mappedBy="rolesPropios")
     * @ORM\JoinTable(name="us_usuario_rol")
     */
    private $usuarios;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identificador
     *
     * @param string $identificador
     * @return Role
     */
    public function setIdentificador($identificador)
    {
        $this->identificador = $identificador;
    
        return $this;
    }

    /**
     * Get identificador
     *
     * @return string 
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Role
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add grupos
     *
     * @param \Reica\UsuarioBundle\Entity\Grupo $grupos
     * @return Role
     */
    public function addGrupo(\Reica\UsuarioBundle\Entity\Grupo $grupos)
    {
        $this->grupos[] = $grupos;
    
        return $this;
    }

    /**
     * Remove grupos
     *
     * @param \Reica\UsuarioBundle\Entity\Grupo $grupos
     */
    public function removeGrupo(\Reica\UsuarioBundle\Entity\Grupo $grupos)
    {
        $this->grupos->removeElement($grupos);
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupos()
    {
        return $this->grupos;
    }
    public function __toString(){
        return  $this->identificador;
    }

    /**
     * Add usuarios
     *
     * @param \Reica\UsuarioBundle\Entity\Usuario $usuarios
     * @return Role
     */
    public function addUsuario(\Reica\UsuarioBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios[] = $usuarios;
    
        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \Reica\UsuarioBundle\Entity\Usuario $usuarios
     */
    public function removeUsuario(\Reica\UsuarioBundle\Entity\Usuario $usuarios)
    {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }
}
