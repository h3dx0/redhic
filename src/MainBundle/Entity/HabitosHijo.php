<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HabitosHijo
 *
 * @ORM\Table(name="habitos_hijo")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\HabitosHijoRepository")
 */
class HabitosHijo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hh_desc_esfv", type="text",  nullable=true)
     */
    private $hhDescEsfv;

    /**
     * @var string
     *
     * @ORM\Column(name="hh_desc_esfa", type="text",  nullable=true)
     */
    private $hhDescEsfa;

    /**
     * @var string
     *
     * @ORM\Column(name="hh_desc_preocupacion", type="text",nullable=true)
     */
    private $hhDescPreocupacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hhDescEsfv
     *
     * @param string $hhDescEsfv
     * @return HabitosHijo
     */
    public function setHhDescEsfv($hhDescEsfv)
    {
        $this->hhDescEsfv = $hhDescEsfv;

        return $this;
    }

    /**
     * Get hhDescEsfv
     *
     * @return string 
     */
    public function getHhDescEsfv()
    {
        return $this->hhDescEsfv;
    }

    /**
     * Set hhDescEsfa
     *
     * @param string $hhDescEsfa
     * @return HabitosHijo
     */
    public function setHhDescEsfa($hhDescEsfa)
    {
        $this->hhDescEsfa = $hhDescEsfa;

        return $this;
    }

    /**
     * Get hhDescEsfa
     *
     * @return string 
     */
    public function getHhDescEsfa()
    {
        return $this->hhDescEsfa;
    }

    /**
     * Set hhDescPreocupacion
     *
     * @param string $hhDescPreocupacion
     * @return HabitosHijo
     */
    public function setHhDescPreocupacion($hhDescPreocupacion)
    {
        $this->hhDescPreocupacion = $hhDescPreocupacion;

        return $this;
    }

    /**
     * Get hhDescPreocupacion
     *
     * @return string 
     */
    public function getHhDescPreocupacion()
    {
        return $this->hhDescPreocupacion;
    }
}
