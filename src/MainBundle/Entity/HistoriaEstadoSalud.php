<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MainBundle\Entity\NFrecuencia;
use MainBundle\Entity\HistoriaClinica;

/**
 * HistoriaEstadoSalud
 *
 * @ORM\Table(name="historia_estado_salud")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\HistoriaEstadoSaludRepository")
 */
class HistoriaEstadoSalud
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;  

    /**
     * @var string
     *
     * @ORM\Column(name="hes_trat_medicamento", type="string", length=255)
     */
    private $hesTratMedicamento;   

    /**
     * @ORM\ManyToOne(targetEntity="HistoriaClinica", inversedBy="historiaSalud")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $historiaClinica;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hesEnfermedad
     *
     * @param string $hesEnfermedad
     * @return HistoriaEstadoSalud
     */
    public function setHesEnfermedad($hesEnfermedad)
    {
        $this->hesEnfermedad = $hesEnfermedad;

        return $this;
    }

    /**
     * Get hesEnfermedad
     *
     * @return string 
     */
    public function getHesEnfermedad()
    {
        return $this->hesEnfermedad;
    }

    /**
     * Set hesEdad
     *
     * @param integer $hesEdad
     * @return HistoriaEstadoSalud
     */
    public function setHesEdad($hesEdad)
    {
        $this->hesEdad = $hesEdad;

        return $this;
    }

    /**
     * Get hesEdad
     *
     * @return integer 
     */
    public function getHesEdad()
    {
        return $this->hesEdad;
    }

    /**
     * Set hesTratMedicamento
     *
     * @param string $hesTratMedicamento
     * @return HistoriaEstadoSalud
     */
    public function setHesTratMedicamento($hesTratMedicamento)
    {
        $this->hesTratMedicamento = $hesTratMedicamento;

        return $this;
    }

    /**
     * Get hesTratMedicamento
     *
     * @return string 
     */
    public function getHesTratMedicamento()
    {
        return $this->hesTratMedicamento;
    }

    /**
     * Set frecuencia
     *
     * @param \MainBundle\Entity\NFrecuencia $frecuencia
     * @return HistoriaEstadoSalud
     */
    public function setFrecuencia(\MainBundle\Entity\NFrecuencia $frecuencia = null)
    {
        $this->frecuencia = $frecuencia;

        return $this;
    }

    /**
     * Get frecuencia
     *
     * @return \MainBundle\Entity\NFrecuencia 
     */
    public function getFrecuencia()
    {
        return $this->frecuencia;
    }

    /**
     * Set historiaClinica
     *
     * @param \MainBundle\Entity\HistoriaClinica $historiaClinica
     * @return HistoriaEstadoSalud
     */
    public function setHistoriaClinica(\MainBundle\Entity\HistoriaClinica $historiaClinica = null)
    {
        $this->historiaClinica = $historiaClinica;

        return $this;
    }

    /**
     * Get historiaClinica
     *
     * @return \MainBundle\Entity\HistoriaClinica 
     */
    public function getHistoriaClinica()
    {
        return $this->historiaClinica;
    }
}
