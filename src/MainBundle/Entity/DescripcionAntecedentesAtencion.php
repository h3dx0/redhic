<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescripcionAntecedentesAtencion
 *
 * @ORM\Table(name="descripcion_antecedentes_atencion")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\DescripcionAntecedentesAtencionRepository")
 */
class DescripcionAntecedentesAtencion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_electroencefalograma", type="string", length=255, nullable=true)
     */
    private $exaDescElectroencefalograma;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_tac", type="string", length=255, nullable=true)
     */
    private $exaDescTac;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_potenciales_emocados", type="string", length=255, nullable=true)
     */
    private $exaDescPotencialesEmocados;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_valoracion_genetica", type="string", length=255, nullable=true)
     */
    private $exaDescValoracionGenetica;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_tamiz_metabolica", type="string", length=255, nullable=true)
     */
    private $exaDescTamizMetabolica;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_resonancia_magnatica", type="string", length=255, nullable=true)
     */
    private $exaDescResonanciaMagnatica;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_eval_nenopsicologia", type="string", length=255, nullable=true)
     */
    private $exaDescEvalNenopsicologia;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_eval_lenguaje", type="string", length=255, nullable=true)
     */
    private $exaDescEvalLenguaje;

    /**
     * @var string
     *
     * @ORM\Column(name="exa_desc_otra", type="string", length=255, nullable=true)
     */
    private $exaDescOtra;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exaDescElectroencefalograma
     *
     * @param string $exaDescElectroencefalograma
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescElectroencefalograma($exaDescElectroencefalograma)
    {
        $this->exaDescElectroencefalograma = $exaDescElectroencefalograma;

        return $this;
    }

    /**
     * Get exaDescElectroencefalograma
     *
     * @return string 
     */
    public function getExaDescElectroencefalograma()
    {
        return $this->exaDescElectroencefalograma;
    }

    /**
     * Set exaDescTac
     *
     * @param string $exaDescTac
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescTac($exaDescTac)
    {
        $this->exaDescTac = $exaDescTac;

        return $this;
    }

    /**
     * Get exaDescTac
     *
     * @return string 
     */
    public function getExaDescTac()
    {
        return $this->exaDescTac;
    }

    /**
     * Set exaDescPotencialesEmocados
     *
     * @param string $exaDescPotencialesEmocados
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescPotencialesEmocados($exaDescPotencialesEmocados)
    {
        $this->exaDescPotencialesEmocados = $exaDescPotencialesEmocados;

        return $this;
    }

    /**
     * Get exaDescPotencialesEmocados
     *
     * @return string 
     */
    public function getExaDescPotencialesEmocados()
    {
        return $this->exaDescPotencialesEmocados;
    }

    /**
     * Set exaDescValoracionGenetica
     *
     * @param string $exaDescValoracionGenetica
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescValoracionGenetica($exaDescValoracionGenetica)
    {
        $this->exaDescValoracionGenetica = $exaDescValoracionGenetica;

        return $this;
    }

    /**
     * Get exaDescValoracionGenetica
     *
     * @return string 
     */
    public function getExaDescValoracionGenetica()
    {
        return $this->exaDescValoracionGenetica;
    }

    /**
     * Set exaDescTamizMetabolica
     *
     * @param string $exaDescTamizMetabolica
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescTamizMetabolica($exaDescTamizMetabolica)
    {
        $this->exaDescTamizMetabolica = $exaDescTamizMetabolica;

        return $this;
    }

    /**
     * Get exaDescTamizMetabolica
     *
     * @return string 
     */
    public function getExaDescTamizMetabolica()
    {
        return $this->exaDescTamizMetabolica;
    }

    /**
     * Set exaDescResonanciaMagnatica
     *
     * @param string $exaDescResonanciaMagnatica
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescResonanciaMagnatica($exaDescResonanciaMagnatica)
    {
        $this->exaDescResonanciaMagnatica = $exaDescResonanciaMagnatica;

        return $this;
    }

    /**
     * Get exaDescResonanciaMagnatica
     *
     * @return string 
     */
    public function getExaDescResonanciaMagnatica()
    {
        return $this->exaDescResonanciaMagnatica;
    }

    /**
     * Set exaDescEvalNenopsicologia
     *
     * @param string $exaDescEvalNenopsicologia
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescEvalNenopsicologia($exaDescEvalNenopsicologia)
    {
        $this->exaDescEvalNenopsicologia = $exaDescEvalNenopsicologia;

        return $this;
    }

    /**
     * Get exaDescEvalNenopsicologia
     *
     * @return string 
     */
    public function getExaDescEvalNenopsicologia()
    {
        return $this->exaDescEvalNenopsicologia;
    }

    /**
     * Set exaDescEvalLenguaje
     *
     * @param string $exaDescEvalLenguaje
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescEvalLenguaje($exaDescEvalLenguaje)
    {
        $this->exaDescEvalLenguaje = $exaDescEvalLenguaje;

        return $this;
    }

    /**
     * Get exaDescEvalLenguaje
     *
     * @return string 
     */
    public function getExaDescEvalLenguaje()
    {
        return $this->exaDescEvalLenguaje;
    }

    /**
     * Set exaDescOtra
     *
     * @param string $exaDescOtra
     * @return DescripcionAntecedentesAtencion
     */
    public function setExaDescOtra($exaDescOtra)
    {
        $this->exaDescOtra = $exaDescOtra;

        return $this;
    }

    /**
     * Get exaDescOtra
     *
     * @return string 
     */
    public function getExaDescOtra()
    {
        return $this->exaDescOtra;
    }
}
