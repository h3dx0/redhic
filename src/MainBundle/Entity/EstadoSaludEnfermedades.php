<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstadoSaludEnfermedades
 *
 * @ORM\Table(name="estado_salud_enfermedades")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\EstadoSaludEnfermedadesRepository")
 */
class EstadoSaludEnfermedades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="enfermedad1", type="text", nullable=true)
     */
    private $enfermedad1;

    /**
     * @var string
     *
     * @ORM\Column(name="enfermedad2", type="text", nullable=true)
     */
    private $enfermedad2;

    /**
     * @var string
     *
     * @ORM\Column(name="enfermedad3", type="text", nullable=true)
     */
    private $enfermedad3;

    /**
     * @var string
     *
     * @ORM\Column(name="enfermedad4", type="text",  nullable=true)
     */
    private $enfermedad4;

    /**
     * @var string
     *
     * @ORM\Column(name="enfermedad5", type="text",  nullable=true)
     */
    private $enfermedad5;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enfermedad1
     *
     * @param string $enfermedad1
     * @return EstadoSaludEnfermedades
     */
    public function setEnfermedad1($enfermedad1)
    {
        $this->enfermedad1 = $enfermedad1;

        return $this;
    }

    /**
     * Get enfermedad1
     *
     * @return string 
     */
    public function getEnfermedad1()
    {
        return $this->enfermedad1;
    }

    /**
     * Set enfermedad2
     *
     * @param string $enfermedad2
     * @return EstadoSaludEnfermedades
     */
    public function setEnfermedad2($enfermedad2)
    {
        $this->enfermedad2 = $enfermedad2;

        return $this;
    }

    /**
     * Get enfermedad2
     *
     * @return string 
     */
    public function getEnfermedad2()
    {
        return $this->enfermedad2;
    }

    /**
     * Set enfermedad3
     *
     * @param string $enfermedad3
     * @return EstadoSaludEnfermedades
     */
    public function setEnfermedad3($enfermedad3)
    {
        $this->enfermedad3 = $enfermedad3;

        return $this;
    }

    /**
     * Get enfermedad3
     *
     * @return string 
     */
    public function getEnfermedad3()
    {
        return $this->enfermedad3;
    }

    /**
     * Set enfermedad4
     *
     * @param string $enfermedad4
     * @return EstadoSaludEnfermedades
     */
    public function setEnfermedad4($enfermedad4)
    {
        $this->enfermedad4 = $enfermedad4;

        return $this;
    }

    /**
     * Get enfermedad4
     *
     * @return string 
     */
    public function getEnfermedad4()
    {
        return $this->enfermedad4;
    }

    /**
     * Set enfermedad5
     *
     * @param string $enfermedad5
     * @return EstadoSaludEnfermedades
     */
    public function setEnfermedad5($enfermedad5)
    {
        $this->enfermedad5 = $enfermedad5;

        return $this;
    }

    /**
     * Get enfermedad5
     *
     * @return string 
     */
    public function getEnfermedad5()
    {
        return $this->enfermedad5;
    }
}
