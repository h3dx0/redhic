<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MainBundle\Entity\NPeriodo;

/**
 * TratamientoMedico
 *
 * @ORM\Table(name="tratamiento_medico")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\TratamientoMedicoRepository")
 */
class TratamientoMedico
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trat_med_enfermedad", type="string", length=255, nullable=true)
     */
    private $tratMedEnfermedad;
 /**
     * @var string
     *
     * @ORM\Column(name="trat_med_especialista", type="string", length=255, nullable=true)
     */
    private $tratMedEspecialista;

    /**
     * @var string
     *
     * @ORM\Column(name="trat_med_motivo", type="string", length=255, nullable=true)
     */
    private $tratMedMotivo;

    /**
     * @var string
     *
     * @ORM\Column(name="trat_med_dosis", type="string", length=255, nullable=true)
     */
    private $tratMedDosis;

    /**
     * @var int
     *
     * @ORM\Column(name="trat_med_edad", type="integer", nullable=true)
     */
    private $tratMedEdad;

    /**
     * @ORM\OneToOne(targetEntity="NFrecuencia", mappedBy="frecuenciaTM")
     */
    private $frecuencia;

    /**
     * @ORM\OneToOne(targetEntity="NPeriodo", mappedBy="periodoTM")
     */
    private $periodo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tratMedEspecialista
     *
     * @param string $tratMedEspecialista
     * @return TratamientoMedico
     */
    public function setTratMedEspecialista($tratMedEspecialista)
    {
        $this->tratMedEspecialista = $tratMedEspecialista;

        return $this;
    }

    /**
     * Get tratMedEspecialista
     *
     * @return string 
     */
    public function getTratMedEspecialista()
    {
        return $this->tratMedEspecialista;
    }

    /**
     * Set tratMedMotivo
     *
     * @param string $tratMedMotivo
     * @return TratamientoMedico
     */
    public function setTratMedMotivo($tratMedMotivo)
    {
        $this->tratMedMotivo = $tratMedMotivo;

        return $this;
    }

    /**
     * Get tratMedMotivo
     *
     * @return string 
     */
    public function getTratMedMotivo()
    {
        return $this->tratMedMotivo;
    }

    /**
     * Set tratMedDosis
     *
     * @param string $tratMedDosis
     * @return TratamientoMedico
     */
    public function setTratMedDosis($tratMedDosis)
    {
        $this->tratMedDosis = $tratMedDosis;

        return $this;
    }

    /**
     * Get tratMedDosis
     *
     * @return string 
     */
    public function getTratMedDosis()
    {
        return $this->tratMedDosis;
    }

    /**
     * Set tratMedEdad
     *
     * @param integer $tratMedEdad
     * @return TratamientoMedico
     */
    public function setTratMedEdad($tratMedEdad)
    {
        $this->tratMedEdad = $tratMedEdad;

        return $this;
    }

    /**
     * Get tratMedEdad
     *
     * @return integer 
     */
    public function getTratMedEdad()
    {
        return $this->tratMedEdad;
    }

    /**
     * Set frecuencia
     *
     * @param \MainBundle\Entity\NFrecuencia $frecuencia
     * @return TratamientoMedico
     */
    public function setFrecuencia(\MainBundle\Entity\NFrecuencia $frecuencia = null)
    {
        $this->frecuencia = $frecuencia;

        return $this;
    }

    /**
     * Get frecuencia
     *
     * @return \MainBundle\Entity\NFrecuencia 
     */
    public function getFrecuencia()
    {
        return $this->frecuencia;
    }

    /**
     * Set periodo
     *
     * @param \MainBundle\Entity\NPeriodo $periodo
     * @return TratamientoMedico
     */
    public function setPeriodo(\MainBundle\Entity\NPeriodo $periodo = null)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get periodo
     *
     * @return \MainBundle\Entity\NPeriodo 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set tratMedEnfermedad
     *
     * @param string $tratMedEnfermedad
     * @return TratamientoMedico
     */
    public function setTratMedEnfermedad($tratMedEnfermedad)
    {
        $this->tratMedEnfermedad = $tratMedEnfermedad;

        return $this;
    }

    /**
     * Get tratMedEnfermedad
     *
     * @return string 
     */
    public function getTratMedEnfermedad()
    {
        return $this->tratMedEnfermedad;
    }
}
