<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProtocoloEvaluacion
 *
 * @ORM\Table(name="protocolo_evaluacion")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ProtocoloEvaluacionRepository")
 */
class ProtocoloEvaluacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="formatoAtencionTemprana", type="boolean", nullable=true)
     */
    private $formatoAtencionTemprana;

    /**
     * @var bool
     *
     * @ORM\Column(name="evaluacionDesarrollo", type="boolean", nullable=true)
     */
    private $evaluacionDesarrollo;

    /**
     * @var bool
     *
     * @ORM\Column(name="neuropsicologia", type="boolean", nullable=true)
     */
    private $neuropsicologia;

    /**
     * @var bool
     *
     * @ORM\Column(name="protocolaTea", type="boolean", nullable=true)
     */
    private $protocolaTea;

    /**
     * @var bool
     *
     * @ORM\Column(name="observacionEscolar", type="boolean", nullable=true)
     */
    private $observacionEscolar;

    /**
     * @var bool
     *
     * @ORM\Column(name="comunicacionLenguaje", type="boolean", nullable=true)
     */
    private $comunicacionLenguaje;

    /**
     * @var bool
     *
     * @ORM\Column(name="inteligencia", type="boolean", nullable=true)
     */
    private $inteligencia;

    /**
     * @var bool
     *
     * @ORM\Column(name="habilidadesSociales", type="boolean", nullable=true)
     */
    private $habilidadesSociales;

    /**
     * @var bool
     *
     * @ORM\Column(name="protocoloTda", type="boolean", nullable=true)
     */
    private $protocoloTda;

    /**
     * @var bool
     *
     * @ORM\Column(name="emocionales", type="boolean", nullable=true)
     */
    private $emocionales;

    /**
     * @var bool
     *
     * @ORM\Column(name="motricidad", type="boolean", nullable=true)
     */
    private $motricidad;

    /**
     * @var bool
     *
     * @ORM\Column(name="pedagogiaAcademica", type="boolean", nullable=true)
     */
    private $pedagogiaAcademica;

    /**
     * @var bool
     *
     * @ORM\Column(name="autonomia", type="boolean", nullable=true)
     */
    private $autonomia;

    /**
     * @var bool
     *
     * @ORM\Column(name="protocoloAprendizaje", type="boolean", nullable=true)
     */
    private $protocoloAprendizaje;

    /**
     * @var bool
     *
     * @ORM\Column(name="sensoriales", type="boolean", nullable=true)
     */
    private $sensoriales;   


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formatoAtencionTemprana
     *
     * @param boolean $formatoAtencionTemprana
     * @return ProtocoloEvaluacion
     */
    public function setFormatoAtencionTemprana($formatoAtencionTemprana)
    {
        $this->formatoAtencionTemprana = $formatoAtencionTemprana;

        return $this;
    }

    /**
     * Get formatoAtencionTemprana
     *
     * @return boolean
     */
    public function getFormatoAtencionTemprana()
    {
        return $this->formatoAtencionTemprana;
    }

    /**
     * Set evaluacionDesarrollo
     *
     * @param boolean $evaluacionDesarrollo
     * @return ProtocoloEvaluacion
     */
    public function setEvaluacionDesarrollo($evaluacionDesarrollo)
    {
        $this->evaluacionDesarrollo = $evaluacionDesarrollo;

        return $this;
    }

    /**
     * Get evaluacionDesarrollo
     *
     * @return boolean
     */
    public function getEvaluacionDesarrollo()
    {
        return $this->evaluacionDesarrollo;
    }

    /**
     * Set neuropsicologia
     *
     * @param boolean $neuropsicologia
     * @return ProtocoloEvaluacion
     */
    public function setNeuropsicologia($neuropsicologia)
    {
        $this->neuropsicologia = $neuropsicologia;

        return $this;
    }

    /**
     * Get neuropsicologia
     *
     * @return boolean
     */
    public function getNeuropsicologia()
    {
        return $this->neuropsicologia;
    }

    /**
     * Set protocolaTea
     *
     * @param boolean $protocolaTea
     * @return ProtocoloEvaluacion
     */
    public function setProtocolaTea($protocolaTea)
    {
        $this->protocolaTea = $protocolaTea;

        return $this;
    }

    /**
     * Get protocolaTea
     *
     * @return boolean
     */
    public function getProtocolaTea()
    {
        return $this->protocolaTea;
    }

    /**
     * Set observacionEscolar
     *
     * @param boolean $observacionEscolar
     * @return ProtocoloEvaluacion
     */
    public function setObservacionEscolar($observacionEscolar)
    {
        $this->observacionEscolar = $observacionEscolar;

        return $this;
    }

    /**
     * Get observacionEscolar
     *
     * @return boolean
     */
    public function getObservacionEscolar()
    {
        return $this->observacionEscolar;
    }

    /**
     * Set comunicacionLenguaje
     *
     * @param boolean $comunicacionLenguaje
     * @return ProtocoloEvaluacion
     */
    public function setComunicacionLenguaje($comunicacionLenguaje)
    {
        $this->comunicacionLenguaje = $comunicacionLenguaje;

        return $this;
    }

    /**
     * Get comunicacionLenguaje
     *
     * @return boolean
     */
    public function getComunicacionLenguaje()
    {
        return $this->comunicacionLenguaje;
    }

    /**
     * Set inteligencia
     *
     * @param boolean $inteligencia
     * @return ProtocoloEvaluacion
     */
    public function setInteligencia($inteligencia)
    {
        $this->inteligencia = $inteligencia;

        return $this;
    }

    /**
     * Get inteligencia
     *
     * @return boolean
     */
    public function getInteligencia()
    {
        return $this->inteligencia;
    }

    /**
     * Set habilidadesSociales
     *
     * @param boolean $habilidadesSociales
     * @return ProtocoloEvaluacion
     */
    public function setHabilidadesSociales($habilidadesSociales)
    {
        $this->habilidadesSociales = $habilidadesSociales;

        return $this;
    }

    /**
     * Get habilidadesSociales
     *
     * @return boolean
     */
    public function getHabilidadesSociales()
    {
        return $this->habilidadesSociales;
    }

    /**
     * Set protocoloTda
     *
     * @param boolean $protocoloTda
     * @return ProtocoloEvaluacion
     */
    public function setProtocoloTda($protocoloTda)
    {
        $this->protocoloTda = $protocoloTda;

        return $this;
    }

    /**
     * Get protocoloTda
     *
     * @return boolean
     */
    public function getProtocoloTda()
    {
        return $this->protocoloTda;
    }

    /**
     * Set emocionales
     *
     * @param boolean $emocionales
     * @return ProtocoloEvaluacion
     */
    public function setEmocionales($emocionales)
    {
        $this->emocionales = $emocionales;

        return $this;
    }

    /**
     * Get emocionales
     *
     * @return boolean
     */
    public function getEmocionales()
    {
        return $this->emocionales;
    }

    /**
     * Set motricidad
     *
     * @param boolean $motricidad
     * @return ProtocoloEvaluacion
     */
    public function setMotricidad($motricidad)
    {
        $this->motricidad = $motricidad;

        return $this;
    }

    /**
     * Get motricidad
     *
     * @return boolean
     */
    public function getMotricidad()
    {
        return $this->motricidad;
    }

    /**
     * Set pedagogiaAcademica
     *
     * @param boolean $pedagogiaAcademica
     * @return ProtocoloEvaluacion
     */
    public function setPedagogiaAcademica($pedagogiaAcademica)
    {
        $this->pedagogiaAcademica = $pedagogiaAcademica;

        return $this;
    }

    /**
     * Get pedagogiaAcademica
     *
     * @return boolean
     */
    public function getPedagogiaAcademica()
    {
        return $this->pedagogiaAcademica;
    }

    /**
     * Set autonomia
     *
     * @param boolean $autonomia
     * @return ProtocoloEvaluacion
     */
    public function setAutonomia($autonomia)
    {
        $this->autonomia = $autonomia;

        return $this;
    }

    /**
     * Get autonomia
     *
     * @return boolean
     */
    public function getAutonomia()
    {
        return $this->autonomia;
    }

    /**
     * Set protocoloAprendizaje
     *
     * @param boolean $protocoloAprendizaje
     * @return ProtocoloEvaluacion
     */
    public function setProtocoloAprendizaje($protocoloAprendizaje)
    {
        $this->protocoloAprendizaje = $protocoloAprendizaje;

        return $this;
    }

    /**
     * Get protocoloAprendizaje
     *
     * @return boolean
     */
    public function getProtocoloAprendizaje()
    {
        return $this->protocoloAprendizaje;
    }

    /**
     * Set sensoriales
     *
     * @param boolean $sensoriales
     * @return ProtocoloEvaluacion
     */
    public function setSensoriales($sensoriales)
    {
        $this->sensoriales = $sensoriales;

        return $this;
    }

    /**
     * Get sensoriales
     *
     * @return boolean
     */
    public function getSensoriales()
    {
        return $this->sensoriales;
    }
}
