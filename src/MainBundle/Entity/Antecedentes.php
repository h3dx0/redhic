<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Antecedentes
 *
 * @ORM\Table(name="antecedentes")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\AntecedentesRepository")
 */
class Antecedentes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="deSalud", type="text", nullable=true)
     */
    private $deSalud;

    /**
     * @var string
     *
     * @ORM\Column(name="educativos", type="text", nullable=true)
     */
    private $educativos;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deSalud
     *
     * @param string $deSalud
     * @return Antecedentes
     */
    public function setDeSalud($deSalud)
    {
        $this->deSalud = $deSalud;

        return $this;
    }

    /**
     * Get deSalud
     *
     * @return string
     */
    public function getDeSalud()
    {
        return $this->deSalud;
    }

    /**
     * Set educativos
     *
     * @param string $educativos
     * @return Antecedentes
     */
    public function setEducativos($educativos)
    {
        $this->educativos = $educativos;

        return $this;
    }

    /**
     * Get educativos
     *
     * @return string
     */
    public function getEducativos()
    {
        return $this->educativos;
    }
}
