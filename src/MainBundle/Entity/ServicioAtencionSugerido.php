<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServicioAtencionSugerido
 *
 * @ORM\Table(name="servicio_atencion_sugerido")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ServicioAtencionSugeridoRepository")
 */
class ServicioAtencionSugerido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="atencionTemprana", type="boolean", nullable=true)
     */
    private $atencionTemprana;

    /**
     * @var bool
     *
     * @ORM\Column(name="intervNeuroPsicologica", type="boolean", nullable=true)
     */
    private $intervNeuroPsicologica;

    /**
     * @var bool
     *
     * @ORM\Column(name="desarrolloHabilSociales", type="boolean", nullable=true)
     */
    private $desarrolloHabilSociales;

    /**
     * @var bool
     *
     * @ORM\Column(name="transicionVidaAdulta", type="boolean", nullable=true)
     */
    private $transicionVidaAdulta;

    /**
     * @var bool
     *
     * @ORM\Column(name="apoyoFamiliar", type="boolean", nullable=true)
     */
    private $apoyoFamiliar;

    /**
     * @var bool
     *
     * @ORM\Column(name="otro", type="boolean", nullable=true)
     */
    private $otro;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set atencionTemprana
     *
     * @param boolean $atencionTemprana
     * @return ServicioAtencionSugerido
     */
    public function setAtencionTemprana($atencionTemprana)
    {
        $this->atencionTemprana = $atencionTemprana;

        return $this;
    }

    /**
     * Get atencionTemprana
     *
     * @return boolean 
     */
    public function getAtencionTemprana()
    {
        return $this->atencionTemprana;
    }

    /**
     * Set intervNeuroPsicologica
     *
     * @param boolean $intervNeuroPsicologica
     * @return ServicioAtencionSugerido
     */
    public function setIntervNeuroPsicologica($intervNeuroPsicologica)
    {
        $this->intervNeuroPsicologica = $intervNeuroPsicologica;

        return $this;
    }

    /**
     * Get intervNeuroPsicologica
     *
     * @return boolean 
     */
    public function getIntervNeuroPsicologica()
    {
        return $this->intervNeuroPsicologica;
    }

    /**
     * Set desarrolloHabilSociales
     *
     * @param boolean $desarrolloHabilSociales
     * @return ServicioAtencionSugerido
     */
    public function setDesarrolloHabilSociales($desarrolloHabilSociales)
    {
        $this->desarrolloHabilSociales = $desarrolloHabilSociales;

        return $this;
    }

    /**
     * Get desarrolloHabilSociales
     *
     * @return boolean 
     */
    public function getDesarrolloHabilSociales()
    {
        return $this->desarrolloHabilSociales;
    }

    /**
     * Set transicionVidaAdulta
     *
     * @param boolean $transicionVidaAdulta
     * @return ServicioAtencionSugerido
     */
    public function setTransicionVidaAdulta($transicionVidaAdulta)
    {
        $this->transicionVidaAdulta = $transicionVidaAdulta;

        return $this;
    }

    /**
     * Get transicionVidaAdulta
     *
     * @return boolean 
     */
    public function getTransicionVidaAdulta()
    {
        return $this->transicionVidaAdulta;
    }

    /**
     * Set apoyoFamiliar
     *
     * @param boolean $apoyoFamiliar
     * @return ServicioAtencionSugerido
     */
    public function setApoyoFamiliar($apoyoFamiliar)
    {
        $this->apoyoFamiliar = $apoyoFamiliar;

        return $this;
    }

    /**
     * Get apoyoFamiliar
     *
     * @return boolean 
     */
    public function getApoyoFamiliar()
    {
        return $this->apoyoFamiliar;
    }

    /**
     * Set otro
     *
     * @param boolean $otro
     * @return ServicioAtencionSugerido
     */
    public function setOtro($otro)
    {
        $this->otro = $otro;

        return $this;
    }

    /**
     * Get otro
     *
     * @return boolean 
     */
    public function getOtro()
    {
        return $this->otro;
    }
}
