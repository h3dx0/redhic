<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NGradoEscolar
 *
 * @ORM\Table(name="n_grado_escolar")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\NGradoEscolarRepository")
 */
class NGradoEscolar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="gradoEscolar", type="string", length=255)
     */
    private $gradoEscolar;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gradoEscolar
     *
     * @param string $gradoEscolar
     * @return NGradoEscolar
     */
    public function setGradoEscolar($gradoEscolar)
    {
        $this->gradoEscolar = $gradoEscolar;

        return $this;
    }

    /**
     * Get gradoEscolar
     *
     * @return string 
     */
    public function getGradoEscolar()
    {
        return $this->gradoEscolar;
    }
}
