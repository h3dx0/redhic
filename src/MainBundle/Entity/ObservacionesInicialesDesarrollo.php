<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObservacionesInicialesDesarrollo
 *
 * @ORM\Table(name="observaciones_iniciales_desarrollo")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ObservacionesInicialesDesarrolloRepository")
 */
class ObservacionesInicialesDesarrollo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usoMateriales", type="text", nullable=true)
     */
    private $usoMateriales;

    /**
     * @var string
     *
     * @ORM\Column(name="funcionesComunicacion", type="text", nullable=true)
     */
    private $funcionesComunicacion;

    /**
     * @var string
     *
     * @ORM\Column(name="lenguaje", type="text", nullable=true)
     */
    private $lenguaje;

    /**
     * @var string
     *
     * @ORM\Column(name="adecuacionComportamental", type="text", nullable=true)
     */
    private $adecuacionComportamental;

    /**
     * @var string
     *
     * @ORM\Column(name="relacionFamilia", type="text", nullable=true)
     */
    private $relacionFamilia;

    /**
     * @var string
     *
     * @ORM\Column(name="habilidadesCognitivas", type="text", nullable=true)
     */
    private $habilidadesCognitivas;

    /**
     * @var string
     *
     * @ORM\Column(name="conductaAutonomia", type="text", nullable=true)
     */
    private $conductaAutonomia;

    /**
     * @var string
     *
     * @ORM\Column(name="aspectosMotores", type="text", nullable=true)
     */
    private $aspectosMotores;

    /**
     * @var string
     *
     * @ORM\Column(name="otros", type="text", nullable=true)
     */
    private $otros;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usoMateriales
     *
     * @param string $usoMateriales
     * @return ObservacionesInicialesDesarrollo
     */
    public function setUsoMateriales($usoMateriales)
    {
        $this->usoMateriales = $usoMateriales;

        return $this;
    }

    /**
     * Get usoMateriales
     *
     * @return string 
     */
    public function getUsoMateriales()
    {
        return $this->usoMateriales;
    }

    /**
     * Set funcionesComunicacion
     *
     * @param string $funcionesComunicacion
     * @return ObservacionesInicialesDesarrollo
     */
    public function setFuncionesComunicacion($funcionesComunicacion)
    {
        $this->funcionesComunicacion = $funcionesComunicacion;

        return $this;
    }

    /**
     * Get funcionesComunicacion
     *
     * @return string 
     */
    public function getFuncionesComunicacion()
    {
        return $this->funcionesComunicacion;
    }

    /**
     * Set lenguaje
     *
     * @param string $lenguaje
     * @return ObservacionesInicialesDesarrollo
     */
    public function setLenguaje($lenguaje)
    {
        $this->lenguaje = $lenguaje;

        return $this;
    }

    /**
     * Get lenguaje
     *
     * @return string 
     */
    public function getLenguaje()
    {
        return $this->lenguaje;
    }

    /**
     * Set adecuacionComportamental
     *
     * @param string $adecuacionComportamental
     * @return ObservacionesInicialesDesarrollo
     */
    public function setAdecuacionComportamental($adecuacionComportamental)
    {
        $this->adecuacionComportamental = $adecuacionComportamental;

        return $this;
    }

    /**
     * Get adecuacionComportamental
     *
     * @return string 
     */
    public function getAdecuacionComportamental()
    {
        return $this->adecuacionComportamental;
    }

    /**
     * Set relacionFamilia
     *
     * @param string $relacionFamilia
     * @return ObservacionesInicialesDesarrollo
     */
    public function setRelacionFamilia($relacionFamilia)
    {
        $this->relacionFamilia = $relacionFamilia;

        return $this;
    }

    /**
     * Get relacionFamilia
     *
     * @return string 
     */
    public function getRelacionFamilia()
    {
        return $this->relacionFamilia;
    }

    /**
     * Set habilidadesCognitivas
     *
     * @param string $habilidadesCognitivas
     * @return ObservacionesInicialesDesarrollo
     */
    public function setHabilidadesCognitivas($habilidadesCognitivas)
    {
        $this->habilidadesCognitivas = $habilidadesCognitivas;

        return $this;
    }

    /**
     * Get habilidadesCognitivas
     *
     * @return string 
     */
    public function getHabilidadesCognitivas()
    {
        return $this->habilidadesCognitivas;
    }

    /**
     * Set conductaAutonomia
     *
     * @param string $conductaAutonomia
     * @return ObservacionesInicialesDesarrollo
     */
    public function setConductaAutonomia($conductaAutonomia)
    {
        $this->conductaAutonomia = $conductaAutonomia;

        return $this;
    }

    /**
     * Get conductaAutonomia
     *
     * @return string 
     */
    public function getConductaAutonomia()
    {
        return $this->conductaAutonomia;
    }

    /**
     * Set aspectosMotores
     *
     * @param string $aspectosMotores
     * @return ObservacionesInicialesDesarrollo
     */
    public function setAspectosMotores($aspectosMotores)
    {
        $this->aspectosMotores = $aspectosMotores;

        return $this;
    }

    /**
     * Get aspectosMotores
     *
     * @return string 
     */
    public function getAspectosMotores()
    {
        return $this->aspectosMotores;
    }

    /**
     * Set otros
     *
     * @param string $otros
     * @return ObservacionesInicialesDesarrollo
     */
    public function setOtros($otros)
    {
        $this->otros = $otros;

        return $this;
    }

    /**
     * Get otros
     *
     * @return string 
     */
    public function getOtros()
    {
        return $this->otros;
    }
}
