<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MainBundle\Entity\NTratamiento;
use MainBundle\Entity\HistoriaClinica;

/**
 * HistoriaInstitucional
 *
 * @ORM\Table(name="historia_institucional")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\HistoriaInstitucionalRepository")
 */
class HistoriaInstitucional
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hi_institucion", type="string", length=255)
     */
    private $hiInstitucion;

    /**
     * @var int
     *
     * @ORM\Column(name="hi_edad", type="integer")
     */
    private $hiEdad;

    /**
     * @var string
     *
     * @ORM\Column(name="hi_tiempo", type="string", length=255)
     */
    private $hiTiempo;

    /**
     * @var string
     *
     * @ORM\Column(name="hi_causa_no_permanencia", type="string", length=255, nullable=true)
     */
    private $hiCausaNoPermanencia;

    /**
     * @ORM\OneToOne(targetEntity="NTratamiento", mappedBy="tratamiento")
     */
    private $tratamiento;

    /**
     * @ORM\ManyToOne(targetEntity="HistoriaClinica", inversedBy="historiaInst")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $historiaClinica;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hiInstitucion
     *
     * @param string $hiInstitucion
     * @return HistoriaInstitucional
     */
    public function setHiInstitucion($hiInstitucion)
    {
        $this->hiInstitucion = $hiInstitucion;

        return $this;
    }

    /**
     * Get hiInstitucion
     *
     * @return string 
     */
    public function getHiInstitucion()
    {
        return $this->hiInstitucion;
    }

    /**
     * Set hiEdad
     *
     * @param integer $hiEdad
     * @return HistoriaInstitucional
     */
    public function setHiEdad($hiEdad)
    {
        $this->hiEdad = $hiEdad;

        return $this;
    }

    /**
     * Get hiEdad
     *
     * @return integer 
     */
    public function getHiEdad()
    {
        return $this->hiEdad;
    }

    /**
     * Set hiTiempo
     *
     * @param string $hiTiempo
     * @return HistoriaInstitucional
     */
    public function setHiTiempo($hiTiempo)
    {
        $this->hiTiempo = $hiTiempo;

        return $this;
    }

    /**
     * Get hiTiempo
     *
     * @return string 
     */
    public function getHiTiempo()
    {
        return $this->hiTiempo;
    }

    /**
     * Set hiCausaNoPermanencia
     *
     * @param string $hiCausaNoPermanencia
     * @return HistoriaInstitucional
     */
    public function setHiCausaNoPermanencia($hiCausaNoPermanencia)
    {
        $this->hiCausaNoPermanencia = $hiCausaNoPermanencia;

        return $this;
    }

    /**
     * Get hiCausaNoPermanencia
     *
     * @return string 
     */
    public function getHiCausaNoPermanencia()
    {
        return $this->hiCausaNoPermanencia;
    }

    /**
     * Set tratamiento
     *
     * @param \MainBundle\Entity\NTratamiento $tratamiento
     * @return HistoriaInstitucional
     */
    public function setTratamiento(\MainBundle\Entity\NTratamiento $tratamiento = null)
    {
        $this->tratamiento = $tratamiento;

        return $this;
    }

    /**
     * Get tratamiento
     *
     * @return \MainBundle\Entity\NTratamiento 
     */
    public function getTratamiento()
    {
        return $this->tratamiento;
    }

    /**
     * Set historiaClinica
     *
     * @param \MainBundle\Entity\HistoriaClinica $historiaClinica
     * @return HistoriaInstitucional
     */
    public function setHistoriaClinica(\MainBundle\Entity\HistoriaClinica $historiaClinica = null)
    {
        $this->historiaClinica = $historiaClinica;

        return $this;
    }

    /**
     * Get historiaClinica
     *
     * @return \MainBundle\Entity\HistoriaClinica 
     */
    public function getHistoriaClinica()
    {
        return $this->historiaClinica;
    }
}
