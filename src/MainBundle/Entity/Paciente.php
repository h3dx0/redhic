<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MainBundle\Entity\NCiudad;

/**
 * Paciente
 *
 * @ORM\Table(name="paciente")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\PacienteRepository")
 */
class Paciente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoPaterno", type="string", length=255)
     */
    private $apellidoPaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoMaterno", type="string", length=255)
     */
    private $apellidoMaterno;
 
    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=255)
     */
    private $ciudad;
    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="date")
     */
    private $fechaNacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="edad", type="string", length=255)
     */
    private $edad;
 /**
     * @var string
     *
     * @ORM\Column(name="nombre_escuela", type="string", length=255, nullable=true)
     */
    private $nombreEscuela;


    /**
     * @ORM\Column(name="telefono_escuela", type="string", nullable=true)
     */
     private $telefonoEscuela;

    /**
     * @ORM\Column(name="grado_escolar", type="string", length=255,nullable=true)
     */
    private $gradoEscolar;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Paciente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidoPaterno
     *
     * @param string $apellidoPaterno
     * @return Paciente
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get apellidoPaterno
     *
     * @return string 
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set apellidoMaterno
     *
     * @param string $apellidoMaterno
     * @return Paciente
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get apellidoMaterno
     *
     * @return string 
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set fechaNacimiento
     *
     * @param string $fechaNacimiento
     * @return Paciente
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return string 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set edad
     *
     * @param string $edad
     * @return Paciente
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return string 
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set telefonoEscuela
     *
     * @param integer $telefonoEscuela
     * @return Paciente
     */
    public function setTelefonoEscuela($telefonoEscuela)
    {
        $this->telefonoEscuela = $telefonoEscuela;

        return $this;
    }

    /**
     * Get telefonoEscuela
     *
     * @return integer 
     */
    public function getTelefonoEscuela()
    {
        return $this->telefonoEscuela;
    }

    
    /**
     * Set ciudad
     *
     * @param string $ciudad
     * @return Paciente
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string 
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set nombreEscuela
     *
     * @param string $nombreEscuela
     * @return Paciente
     */
    public function setNombreEscuela($nombreEscuela)
    {
        $this->nombreEscuela = $nombreEscuela;

        return $this;
    }

    /**
     * Get nombreEscuela
     *
     * @return string 
     */
    public function getNombreEscuela()
    {
        return $this->nombreEscuela;
    }

    /**
     * Set gradoEscolar
     *
     * @param string $gradoEscolar
     * @return Paciente
     */
    public function setGradoEscolar($gradoEscolar)
    {
        $this->gradoEscolar = $gradoEscolar;

        return $this;
    }

    /**
     * Get gradoEscolar
     *
     * @return string 
     */
    public function getGradoEscolar()
    {
        return $this->gradoEscolar;
    }
}
