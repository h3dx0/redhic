<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HipotesisDiagnostica
 *
 * @ORM\Table(name="hipotesis_diagnostica")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\HipotesisDiagnosticaRepository")
 */
class HipotesisDiagnostica
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="alteracionesSocioComunicativas", type="boolean", nullable=true)
     */
    private $alteracionesSocioComunicativas;

    /**
     * @var bool
     *
     * @ORM\Column(name="difAprendAcademico", type="boolean", nullable=true)
     */
    private $difAprendAcademico;

    /**
     * @var bool
     *
     * @ORM\Column(name="deficitAtencional", type="boolean", nullable=true)
     */
    private $deficitAtencional;

    /**
     * @var bool
     *
     * @ORM\Column(name="retardoGlobalDesarrollo", type="boolean", nullable=true)
     */
    private $retardoGlobalDesarrollo;

    /**
     * @var bool
     *
     * @ORM\Column(name="difDesarrolloLenguaje", type="boolean", nullable=true)
     */
    private $difDesarrolloLenguaje;

    /**
     * @var bool
     *
     * @ORM\Column(name="difMotoras", type="boolean", nullable=true)
     */
    private $difMotoras;

    /**
     * @var bool
     *
     * @ORM\Column(name="alteracionesComportamiento", type="boolean", nullable=true)
     */
    private $alteracionesComportamiento;

    /**
     * @var bool
     *
     * @ORM\Column(name="necesidadApoyoEmocional", type="boolean", nullable=true)
     */
    private $necesidadApoyoEmocional;

    /**
     * @var bool
     *
     * @ORM\Column(name="otra", type="boolean", nullable=true)
     */
    private $otra;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alteracionesSocioComunicativas
     *
     * @param boolean $alteracionesSocioComunicativas
     * @return HipotesisDiagnostica
     */
    public function setAlteracionesSocioComunicativas($alteracionesSocioComunicativas)
    {
        $this->alteracionesSocioComunicativas = $alteracionesSocioComunicativas;

        return $this;
    }

    /**
     * Get alteracionesSocioComunicativas
     *
     * @return boolean 
     */
    public function getAlteracionesSocioComunicativas()
    {
        return $this->alteracionesSocioComunicativas;
    }

    /**
     * Set difAprendAcademico
     *
     * @param boolean $difAprendAcademico
     * @return HipotesisDiagnostica
     */
    public function setDifAprendAcademico($difAprendAcademico)
    {
        $this->difAprendAcademico = $difAprendAcademico;

        return $this;
    }

    /**
     * Get difAprendAcademico
     *
     * @return boolean 
     */
    public function getDifAprendAcademico()
    {
        return $this->difAprendAcademico;
    }

    /**
     * Set deficitAtencional
     *
     * @param boolean $deficitAtencional
     * @return HipotesisDiagnostica
     */
    public function setDeficitAtencional($deficitAtencional)
    {
        $this->deficitAtencional = $deficitAtencional;

        return $this;
    }

    /**
     * Get deficitAtencional
     *
     * @return boolean 
     */
    public function getDeficitAtencional()
    {
        return $this->deficitAtencional;
    }

    /**
     * Set retardoGlobalDesarrollo
     *
     * @param boolean $retardoGlobalDesarrollo
     * @return HipotesisDiagnostica
     */
    public function setRetardoGlobalDesarrollo($retardoGlobalDesarrollo)
    {
        $this->retardoGlobalDesarrollo = $retardoGlobalDesarrollo;

        return $this;
    }

    /**
     * Get retardoGlobalDesarrollo
     *
     * @return boolean 
     */
    public function getRetardoGlobalDesarrollo()
    {
        return $this->retardoGlobalDesarrollo;
    }

    /**
     * Set difDesarrolloLenguaje
     *
     * @param boolean $difDesarrolloLenguaje
     * @return HipotesisDiagnostica
     */
    public function setDifDesarrolloLenguaje($difDesarrolloLenguaje)
    {
        $this->difDesarrolloLenguaje = $difDesarrolloLenguaje;

        return $this;
    }

    /**
     * Get difDesarrolloLenguaje
     *
     * @return boolean 
     */
    public function getDifDesarrolloLenguaje()
    {
        return $this->difDesarrolloLenguaje;
    }

    /**
     * Set difMotoras
     *
     * @param boolean $difMotoras
     * @return HipotesisDiagnostica
     */
    public function setDifMotoras($difMotoras)
    {
        $this->difMotoras = $difMotoras;

        return $this;
    }

    /**
     * Get difMotoras
     *
     * @return boolean 
     */
    public function getDifMotoras()
    {
        return $this->difMotoras;
    }

    /**
     * Set alteracionesComportamiento
     *
     * @param boolean $alteracionesComportamiento
     * @return HipotesisDiagnostica
     */
    public function setAlteracionesComportamiento($alteracionesComportamiento)
    {
        $this->alteracionesComportamiento = $alteracionesComportamiento;

        return $this;
    }

    /**
     * Get alteracionesComportamiento
     *
     * @return boolean 
     */
    public function getAlteracionesComportamiento()
    {
        return $this->alteracionesComportamiento;
    }

    /**
     * Set necesidadApoyoEmocional
     *
     * @param boolean $necesidadApoyoEmocional
     * @return HipotesisDiagnostica
     */
    public function setNecesidadApoyoEmocional($necesidadApoyoEmocional)
    {
        $this->necesidadApoyoEmocional = $necesidadApoyoEmocional;

        return $this;
    }

    /**
     * Get necesidadApoyoEmocional
     *
     * @return boolean 
     */
    public function getNecesidadApoyoEmocional()
    {
        return $this->necesidadApoyoEmocional;
    }

    /**
     * Set otra
     *
     * @param boolean $otra
     * @return HipotesisDiagnostica
     */
    public function setOtra($otra)
    {
        $this->otra = $otra;

        return $this;
    }

    /**
     * Get otra
     *
     * @return boolean 
     */
    public function getOtra()
    {
        return $this->otra;
    }
}
