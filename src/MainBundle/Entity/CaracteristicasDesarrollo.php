<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CaracteristicasDesarrollo
 *
 * @ORM\Table(name="caracteristicas_desarrollo")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\CaracteristicasDesarrolloRepository")
 */
class CaracteristicasDesarrollo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cdes_desc_tnpav", type="text", nullable=true)
     */
    private $cdesDescTnpav;

    /**
     * @var string
     *
     * @ORM\Column(name="cdes_descscdpav", type="text", nullable=true)
     */
    private $cdesDescscdpav;

    /**
     * @var string
     *
     * @ORM\Column(name="cdes_desc_edad_cca", type="text",  nullable=true)
     */
    private $cdesDescEdadCca;

    /**
     * @var string
     *
     * @ORM\Column(name="cdes_desc_pmah", type="text",nullable=true)
     */
    private $cdesDescPmah;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cdesDescTnpav
     *
     * @param string $cdesDescTnpav
     * @return CaracteristicasDesarrollo
     */
    public function setCdesDescTnpav($cdesDescTnpav)
    {
        $this->cdesDescTnpav = $cdesDescTnpav;

        return $this;
    }

    /**
     * Get cdesDescTnpav
     *
     * @return string 
     */
    public function getCdesDescTnpav()
    {
        return $this->cdesDescTnpav;
    }

    /**
     * Set cdesDescscdpav
     *
     * @param string $cdesDescscdpav
     * @return CaracteristicasDesarrollo
     */
    public function setCdesDescscdpav($cdesDescscdpav)
    {
        $this->cdesDescscdpav = $cdesDescscdpav;

        return $this;
    }

    /**
     * Get cdesDescscdpav
     *
     * @return string 
     */
    public function getCdesDescscdpav()
    {
        return $this->cdesDescscdpav;
    }

    /**
     * Set cdesDescEdadCca
     *
     * @param string $cdesDescEdadCca
     * @return CaracteristicasDesarrollo
     */
    public function setCdesDescEdadCca($cdesDescEdadCca)
    {
        $this->cdesDescEdadCca = $cdesDescEdadCca;

        return $this;
    }

    /**
     * Get cdesDescEdadCca
     *
     * @return string 
     */
    public function getCdesDescEdadCca()
    {
        return $this->cdesDescEdadCca;
    }

    /**
     * Set cdesDescPmah
     *
     * @param string $cdesDescPmah
     * @return CaracteristicasDesarrollo
     */
    public function setCdesDescPmah($cdesDescPmah)
    {
        $this->cdesDescPmah = $cdesDescPmah;

        return $this;
    }

    /**
     * Get cdesDescPmah
     *
     * @return string 
     */
    public function getCdesDescPmah()
    {
        return $this->cdesDescPmah;
    }
}
