<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MainBundle\Entity\AntecedentesFamiliaresOtro;
use MainBundle\Entity\CaracteristicasDesarrollo;
use MainBundle\Entity\DatosCentroTrabajo;
use MainBundle\Entity\DatosFamiliaresHermano;
use MainBundle\Entity\DatosFamiliaresMadre;
use MainBundle\Entity\DatosFamiliaresPadre;
use MainBundle\Entity\DescripcionAntecedentesAtencion;
use MainBundle\Entity\DescripcionEmbarazo;
use MainBundle\Entity\HabitosHijo;
use MainBundle\Entity\HistoriaEstadoSalud;
use MainBundle\Entity\HistoriaInstitucional;
use MainBundle\Entity\MotivoConsulta;
use MainBundle\Entity\NacimientoParto;
use MainBundle\Entity\Paciente;
use MainBundle\Entity\TratamientoMedico;

/**
 * HistoriaClinica
 *
 * @ORM\Table(name="historia_clinica")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\HistoriaClinicaRepository")
 */
class HistoriaClinica
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="datetime")
     */
    private $fechaCreacion;

    /**
    * @ORM\Column(name="hc_dgn_edad", type="integer")
   
    private $hcDgnEdad;
 */
    /**
     * @ORM\Column(name="hc_dgn_sexo", type="string")
     */
    private $hcDgnSexo;

    /**
     * @ORM\Column(name="hc_dgn_edad_madre", type="integer")
     
    private $hcDgnEdadMadre;

    /**
     * @ORM\Column(name="hc_dgn_edad_padre", type="integer")
  
    private $hcDgnEdadPadre;

    /**
     * @ORM\Column(name="hc_dgn_edad_tutor", type="integer")
    
    private $hcDgnEdadTutor;
 */
    /**
     * @ORM\Column(name="hc_rem_colegio", type="boolean", nullable=true)
     */
    private $hcRemColegio;

    /**
     * @ORM\Column(name="hc_rem_nenopediatra", type="boolean", nullable=true)
     */
    private $hcRemNenopediatra;

    /**
     * @ORM\Column(name="hc_rem_psiquiatra", type="boolean", nullable=true)
     */
    private $hcRemPsiquiatra;

    /**
     * @ORM\Column(name="hc_rem_pediatra", type="boolean", nullable=true)
     */
    private $hcRemPediatra;

   /**
     * @ORM\Column(name="hc_rem_familiar", type="boolean", nullable=true)
     */
   private $hcRemFamiliar;

    /**
     * @ORM\Column(name="hc_rem_busqueda", type="boolean", nullable=true)
     */
    private $hcRemBusqueda;

    /**
     * @ORM\Column(name="hc_rem_otro", type="boolean", nullable=true)
     */
    private $hcRemOtro;

    /**
     * @ORM\Column(name="hc_rem_terapia_lenguaje", type="boolean", nullable=true)
     */
    private $hcRemTerapiaLenguaje;

    /**
     * @ORM\Column(name="hc_rem_psicologo", type="boolean", nullable=true)
     */
    private $hcRemPsicologo;

    /*ANTECEDENTES ATENCION*/
    /**
     * @ORM\Column(name="hc_ada_farmacos_neurologicos", type="boolean", nullable=true)
     */
    private $hcAdaFarmacosNeurologicos;

    /**
     * @ORM\Column(name="hc_ada_terapia_lenguaje", type="boolean", nullable=true)
     */
    private $hcAdaTerapiaLenguaje;

    /**
     * @ORM\Column(name="hc_ada_abordaje_nuetricional", type="boolean", nullable=true)
     */
    private $hcAdaAbordajeNutricional;

    /**
     * @ORM\Column(name="hc_ada_apoyo_pedagogico", type="boolean", nullable=true)
     */
    private $hcAdaApoyoPedagogico;

    /**
     * @ORM\Column(name="hc_ada_terapia_ocupacional", type="boolean", nullable=true)
     */
    private $hcAdaTerapiaOcupacional;

    /**
     * @ORM\Column(name="hc_ada_terapia_asist_animales", type="boolean", nullable=true)
     */
    private $hcAdaTerapiaAsistAnimales;
    /**
     * @ORM\Column(name="hc_ada_terapias_sensoriales", type="boolean", nullable=true)
     */
    private $hcAdaTerapiasSensoriales;

    /**
     * @ORM\Column(name="hc_ada_estimulacion_temprana", type="boolean", nullable=true)
     */
    private $hcAdaEstimulacionTemprana; 
    /**
     * @ORM\Column(name="hc_ada_psicomotricidad", type="boolean", nullable=true)
     */
    private $hcAdaPsicomotricidad;
     /**
     * @ORM\Column(name="hc_ada_apoyo_psicologico", type="boolean", nullable=true)
     */
    private $hcAdaApoyoPsicologico;
    /**
     * @ORM\Column(name="hc_ada_otro", type="boolean", nullable=true)
     */
    private $hcAdaOtro;


    /**
     * @ORM\Column(name="hc_exa_electroencefalograma", type="boolean", nullable=true)
     */
    private $hcExaElectroencefalograma;

    /**
     * @ORM\Column(name="hc_exa_tac", type="boolean", nullable=true)
     */
    private $hcExaTac;

    /**
     * @ORM\Column(name="hc_exa_potenciales_emocados", type="boolean", nullable=true)
     */
    private $hcExaPotencialesEmocados;

    /**
     * @ORM\Column(name="hc_exa_valoracion_genetica", type="boolean", nullable=true)
     */
    private $hcExaValoracionGenetica;

    /**
     * @ORM\Column(name="hc_tamiz_metabolico", type="boolean", nullable=true)
     */
    private $hcTamizMetabolico;

    /**
     * @ORM\Column(name="hc_exa_resonancia_magnetica", type="boolean", nullable=true)
     */
    private $hcExaResonanciaMagnetica;

    /**
     * @ORM\Column(name="hs_eval_nenopsicologica", type="boolean", nullable=true)
     */
    private $hsEvalNenopsicologica;

    /**
     * @ORM\Column(name="hc_exa_eval_lenguaje", type="boolean", nullable=true)
     */
    private $hcExaEvalLenguaje;

    /**
     * @ORM\Column(name="hc_exa_otra", type="boolean", nullable=true)
     */
    private $hcExaOtra;

    /**
     * @ORM\Column(name="hc_emb_edad_mama", type="integer", nullable=true)
     */
    private $hcEmbEdadMama;

    /**
     * @ORM\Column(name="hc_emb_edad_papa", type="integer", nullable=true)
     */
    private $hcEmbEdadPapa;

    /**
     * @ORM\Column(name="hc_emb_abortos_mama", type="boolean", nullable=true)
     */
    private $hcEmbAbortosMama;

    /**
     * @ORM\Column(name="hc_emb_abortos_papa", type="boolean", nullable=true)
     */
    private $hcEmbAbortosPapa;

    /**
     * @ORM\Column(name="hc_emb_acg_mama", type="boolean", nullable=true)
     */
    private $hcEmbAcgMama;

     /**
      * @ORM\Column(name="hc_emb_acg_papa", type="boolean", nullable=true)
      */
     private $hcEmbAcgPapa;

    /**
     * @ORM\Column(name="hc_emb_edemb_mama", type="boolean", nullable=true)
     */
    private $hcEmbEdembMama;
    
    /**
     * @ORM\Column(name="hs_emb_edemb_papa", type="boolean", nullable=true)
     */
    private $hsEmbEdembPapa;

    /**
     * @ORM\Column(name="hc_emb_htox_mama", type="boolean", nullable=true)
     */
    private $hcEmbHtoxMama;

    /**
     * @ORM\Column(name="hc_emb_htox_papa", type="boolean", nullable=true)
     */
    private $hcEmbHtoxPapa;

    /**
     * @ORM\Column(name="hc_emb_ead_mama", type="boolean", nullable=true)
     */
    private $hcEmbEadMama;

    /**
     * @ORM\Column(name="hc_emb_ead_papa", type="boolean", nullable=true)
     */
    private $hcEmnEadPapa;

    /**
     * @ORM\Column(name="hc_emb_iaalim_mama", type="boolean", nullable=true)
     */
    private $hcEmbIaalimMama;

    /**
     * @ORM\Column(name="hc_emn_iaalim_papa", type="boolean", nullable=true)
     */
    private $hcEmnIaalimPapa;

    /**
     * @ORM\Column(name="hc_emb_medicamentos_mama", type="boolean", nullable=true)
     */
    private $hcEmbMedicamentosMama;

    /**
     * @ORM\Column(name="hc_emb_medicamentos_papa", type="boolean", nullable=true)
     */
    private $hcEmbMedicamentosPapa;

    /**
     * @ORM\Column(name="hc_emb_peso_ok_mama", type="boolean", nullable=true)
     */
    private $hcEmbPesoOkMama;

    /**
     * @ORM\Column(name="hc_emb_peso_ok_papa", type="boolean", nullable=true)
     */
    private $hcEmbPesoOkPapa;

    /**
     * @ORM\Column(name="hc_emb_experiod_mama", type="boolean", nullable=true)
     */
    private $hcEmbExperiodMama;

    /**
     * @ORM\Column(name="hc_emb_experiod_papa", type="boolean", nullable=true)
     */
    private $hcEmbExperiodPapa;

    /**
     * @ORM\Column(name="hc_nacp_parto_natural", type="boolean", nullable=true)
     */
    private $hcNacpPartoNatural;
    /**
     * @ORM\Column(name="hc_nacp_parto_cesarea", type="boolean", nullable=true)
     */
    private $hcNacpPartoCesarea;
 /**
     * @ORM\Column(name="hc_nacp_parto_termino", type="boolean", nullable=true)
     */
 private $hcNacpPartoTermino;

    /**
     * @ORM\Column(name="hc_nacp_parto_pretermino", type="boolean", nullable=true)
     */
    private $hcNacpPartoPretermino;

    /**
     * @ORM\Column(name="hc_nacp_parto_postermino", type="boolean", nullable=true)
     */
    private $hcNacpPartoPostermino;

    /**
     * @ORM\Column(name="hc_nacp_llanto_espontaneo", type="boolean", nullable=true)
     */
    private $hcNacpLlantoEspontaneo;
    /**
     * @ORM\Column(name="hc_nacp_llanto_provocado", type="boolean", nullable=true)
     */
    private $hcNacpLlantoProvocado;

    /**
     * @ORM\Column(name="hc_nacp_peso_normal", type="boolean", nullable=true)
     */
    private $hcNacpPesoNormal;

    /**
     * @ORM\Column(name="hc_nacp_peso_bajo", type="boolean", nullable=true)
     */
    private $hcNacpPesoBajo;

    /**
     * @ORM\Column(name="hc_nacp_peso_sobre", type="boolean", nullable=true)
     */
    private $hcNacpPesoSobre;

    /**
     * @ORM\Column(name="hc_nacp_sufrimiento_fetal", type="boolean", nullable=true)
     */
    private $hcNacpSufrimientoFetal;

    /**
     * @ORM\Column(name="hc_nacp_sufrimiento_fetal_desc", type="text", nullable=true)
     */
    private $hcNacpSufrimientoFetalDesc;

    /**
     * @ORM\Column(name="hc_cdes_tnpav", type="boolean", nullable=true)
     */
    private $hcCdesTnpav;

    /**
     * @ORM\Column(name="hc_cdes_edad_cca", type="integer", nullable=true)
     */
    private $hcCdesEdadCca;

    /**
     * @ORM\Column(name="hc_dsm_edad_srca", type="integer", nullable=true)
     */
    private $hcDsmEdadSrca;

    /**
     * @ORM\Column(name="hc_dsm_edad_spadres", type="integer", nullable=true)
     */
    private $hcDsmEdadSPadres;

    /**
     * @ORM\Column(name="hc_dsm_edad_tomar_obj", type="integer", nullable=true)
     */
    private $hcDsmEdadTomarObj;

    /**
     * @ORM\Column(name="hc_dsm_edad_mobjfunc", type="integer", nullable=true)
     */
    private $hcDsmEdadMobjfunc;

    /**
     * @ORM\Column(name="hc_dsm_edad_gatear", type="integer", nullable=true)
     */
    private $hcDsmEdadGatear;

    /**
     * @ORM\Column(name="hc_dsm_edad_caminar", type="integer", nullable=true)
     */
    private $hcDsmEdadCaminar;

    /**
     * @ORM\Column(name="hc_dsm_edad_primeras_palabras", type="integer", nullable=true)
     */
    private $hcDsmEdadPrimerasPalabras;

    /**
     * @ORM\Column(name="hc_dsm_edad_soaint", type="integer", nullable=true)
     */
    private $hcDsmEdadSoaint;

    /**
     * @ORM\Column(name="hc_dsm_edad_masen", type="integer", nullable=true)
     */
    private $hcDsmEdadMasen;

    /**
     * @ORM\Column(name="hc_dsm_edad_jugar", type="integer", nullable=true)
     */
    private $hcDsmEdadJugar;

    /**
     * @ORM\Column(name="hc_dsm_edad_responder", type="integer", nullable=true)
     */
    private $hcDsmEdadResponder;

    /*Enferemedades*/

    /**
     * @ORM\Column(name="hc_enfer_geneticas", type="boolean", nullable=true)
     */
    private $hcEnfGeneticas;

    /**
     * @ORM\Column(name="hc_enfer_probnutricionales", type="boolean", nullable=true)
     */
    private $hcEnfProbNutricionales;
    
    /**
     * @ORM\Column(name="hc_enfer_inmunologica", type="boolean", nullable=true)
     */
    private $hcEnfInmunnologica;
    
    /**
     * @ORM\Column(name="hc_enfer_trastorno_neurologico", type="boolean", nullable=true)
     */
    private $hcEnfTrastNeurologico;

    /**
     * @ORM\Column(name="hc_enfer_ret_psicomotor", type="boolean", nullable=true)
     */
    private $hcEnfRetPsicomotor;
    
    /**
     * @ORM\Column(name="hc_enfer_trast_sueño", type="boolean", nullable=true)
     */
    private $hcEnfTrastSueno;

    /**
     * @ORM\Column(name="hc_enfer_respiratorias", type="boolean", nullable=true)
     */
    private $hcEnfRespiratorias;
    /**
     * @ORM\Column(name="hc_enfer_otras", type="boolean", nullable=true)
     */
    private $hcEnfOtras;
    
    /**
     * @ORM\Column(name="hc_hes_medicamentos", type="text", nullable=true)
     */
    private $hcHesMedicamentos;

    /**
     * @ORM\Column(name="hc_af_tea_mama", type="boolean", nullable=true)
     */
    private $hcAfTeaMama;

     /**
      * @ORM\Column(name="hc_af_tea_papa", type="boolean", nullable=true)
      */
     private $hcAfTeaPapa;

    /**
     * @ORM\Column(name="hc_af_tea_hermano", type="boolean", nullable=true)
     */
    private $hcAfTeaHermano;

    /**
     * @ORM\Column(name="hc_af_tdl_mama", type="boolean", nullable=true)
     */
    private $hcAfTdlMama;

    /**
     * @ORM\Column(name="hc_af_tdl_papa", type="boolean", nullable=true)
     */
    private $hcAfTdlPapa;

    /**
     * @ORM\Column(name="hc_af_tdl_hermano", type="boolean", nullable=true)
     */
    private $hcAfTdlHermano;

    /**
     * @ORM\Column(name="hc_af_dintl_mama", type="boolean", nullable=true)
     */
    private $hcAfDintlMama;

    /**
     * @ORM\Column(name="hc_af_dintl_papa", type="boolean", nullable=true)
     */
    private $hcAfDintlPapa;

    /**
     * @ORM\Column(name="hc_af_dintl_hermano", type="boolean", nullable=true)
     */
    private $hcAfDintlHermano;

    /**
     * @ORM\Column(name="hc_af_enfpsq_mama", type="boolean", nullable=true)
     */
    private $hcAfEnfpsqMama;

    /**
     * @ORM\Column(name="hc_af_enfpsq_papa", type="boolean", nullable=true)
     */
    private $hcAfEnfpsqPapa;

    /**
     * @ORM\Column(name="hc_af_enfpsq_hermano", type="boolean", nullable=true)
     */
    private $hcAfEnfpsqHermano;

    /**
     * @ORM\Column(name="hc_af_taprend_mama", type="boolean", nullable=true)
     */
    private $hcAfTaprendMama;

    /**
     * @ORM\Column(name="hc_af_taprend_papa", type="boolean", nullable=true)
     */
    private $hcAfTaprendPapa;

    /**
     * @ORM\Column(name="hc_af_tapren_hermano", type="boolean", nullable=true)
     */
    private $hcAfTaprenHermano;

    /**
     * @ORM\Column(name="hc_af_tpdda_mama", type="boolean", nullable=true)
     */
    private $hcAfTpddaMama;

    /**
     * @ORM\Column(name="hc_af_tpdda_papa", type="boolean", nullable=true)
     */
    private $hcAfTpddaPapa;

    /**
     * @ORM\Column(name="hc_af_tpdda_hermano", type="boolean", nullable=true)
     */
    private $hcAfTpddaHermano;

    /**
     * @ORM\Column(name="hc_af_tcc_mama", type="boolean", nullable=true)
     */
    private $hcAfTccMama;

    /**
     * @ORM\Column(name="hc_af_tcc_papa", type="boolean", nullable=true)
     */
    private $hcAfTccPapa;

    /**
     * @ORM\Column(name="hc_af_tcc_hermano", type="boolean", nullable=true)
     */
    private $hcAfTccHermano;

    /**
     * @ORM\Column(name="hc_esfv_edad_control", type="integer", nullable=true)
     */
    private $hcEsfvEdadControl;

    /**
     * @ORM\Column(name="hc_esfv_dificultades", type="boolean", nullable=true)
     */
    private $hcEsfvDificultades;

    /**
     * @ORM\Column(name="hc_esfa_edad_control", type="integer", nullable=true)
     */
    private $hcEsfaEdadControl;

    /**
     * @ORM\Column(name="hc_esfa_dificultades", type="boolean", nullable=true)
     */
    private $hcEsfaDificultades;

    /**
     * @ORM\Column(name="hc_sueno_solo", type="boolean", nullable=true)
     */
    private $hcSuenoSolo;

    /**
     * @ORM\Column(name="hc_sueno_suficiente", type="boolean", nullable=true)
     */
    private $hcSuenoSuficiente;

    /**
     * @ORM\Column(name="hc_sueno_tranquilo", type="boolean", nullable=true)
     */
    private $hcSuenoTranquilo;
    
    /**
     * @ORM\Column(name="hc_sueno_alterado", type="boolean", nullable=true)
     */
    private $hcSuenoAlterado;

    /**
     * @ORM\Column(name="hc_sueno_pesadillas", type="boolean", nullable=true)
     */
    private $hcSuenoPesadillas;

    /**
     * @ORM\Column(name="hc_sueno_gritos", type="boolean", nullable=true)
     */
    private $hcSuenoGritos;

    /**
     * @ORM\Column(name="hc_sueno_somniloquios", type="boolean", nullable=true)
     */
    private $hcSuenoSomniloquios;

    
    /**
     * @ORM\Column(name="hc_alim_solo", type="boolean", nullable=true)
     */
    private $hcAlimSolo;

    /**
     * @ORM\Column(name="hc_alim_dieta_flexible", type="boolean", nullable=true)
     */
    private $hcAlimDietaFlexible;

    /**
     * @ORM\Column(name="hc_alim_cosas_raras", type="boolean", nullable=true)
     */
    private $hcAlimCosasRaras;

    /**
     * @ORM\Column(name="hc_alim_dosis_adecuada", type="boolean", nullable=true)
     */
    private $hcAlimDosisAdecuada;

    /**
     * @ORM\Column(name="hc_habitos_preocupacion", type="boolean", nullable=true)
     */
    private $hcHabitosPreocupacion;

    /**
     * @ORM\Column(name="hc_hi_instituciones_educativas", type="boolean", nullable=true)
     */
    private $hcHiInstitucionesEducativas;

    /**
     * @ORM\Column(name="hc_nacp_hospitalizacion", type="boolean", nullable=true)
     */
    private $hcNacpHospitalizacion;

    /**
     * @ORM\Column(name="hc_nacp_asistencia_respiratoria", type="boolean", nullable=true)
     */
    private $hcNacpAsistenciaRespiratoria;

    /**
     * @ORM\Column(name="hc_nacp_incubadora", type="boolean", nullable=true)
     */
    private $hcNacpIncubadora;

    /**
     * @ORM\Column(name="hc_nacp_reanimacion", type="boolean", nullable=true)
     */
    private $hcNacpReanimacion;

    /**
     * @ORM\Column(name="hc_nacp_trat_farmacologico", type="boolean", nullable=true)
     */
    private $hcNacpTratFarmacologico;

    /**
     * @ORM\Column(name="hc_estado_salud_actual", type="text", nullable=true)
     */
    private $hcEstadoSaludActual;

    /**
     * @ORM\Column(name="hc_hi_centros_educativos_desc", type="text", nullable=true)
     */
    private $hcHiCentrosEducativosDesc;

    /*Antecedentes educativos*/
    /**
     * @ORM\Column(name="hc_ae_asiste_escuela", type="boolean", nullable=true)
     */
    private $hcAEasisteEscuela;
    /**
     * @ORM\Column(name="hc_ae_apoyocurricular", type="boolean", nullable=true)
     */
    private $hcAEapoyoCurricular;
    /**
     * @ORM\Column(name="hc_ae_desfasado_edad", type="boolean", nullable=true)
     */
    private $hcAEdesfasadoEdad;
    /**
     * @ORM\Column(name="hc_ae_apoyo_psicopedagogico", type="boolean", nullable=true)
     */
    private $hcAEapoyoPsicopedagogico;
     /**
     * @ORM\Column(name="hc_ae_maestra_sombra", type="boolean", nullable=true)
     */
    private $hcAEmaestraSombra;
     /**
     * @ORM\Column(name="hc_ae_participa_manera_ordinaria_actividades", type="boolean", nullable=true)
     */
    private $hcAEparticipaManeraOrdinariaActividades;
     /**
     * @ORM\Column(name="hc_ae_integra_grupo", type="boolean", nullable=true)
     */
    private $hcAEingregraGrupo;
    /**
     * @ORM\Column(name="hc_ae_descripcion_experiencias", type="text", nullable=true)
     */
    private $hcAEDescripcionExperiencias;

    /*fin AE*/

    /**
     * @ORM\OneToOne(targetEntity="AntecedentesFamiliaresOtro", mappedBy="antecedentesFamiliaresOtro" ,cascade={"persist"})
     * @ORM\JoinColumn(name="idAntecedentesFamiliaresOtro", referencedColumnName="id")
     */
    private $antecedentesFamiliaresOtro;


    /**
     * @ORM\OneToOne(targetEntity="CaracteristicasDesarrollo", mappedBy="caracteristicasDesarrollo",cascade={"persist"})
     * @ORM\JoinColumn(name="idCaracteristicasDesarrollo", referencedColumnName="id")
     */
    private $caracteristicasDesarrollo;

     /**
     * @ORM\OneToOne(targetEntity="DatosFamiliaresHermano", mappedBy="datosFamiliaresHermano",cascade={"persist"})
     * @ORM\JoinColumn(name="idDatosFamiliaresHermano", referencedColumnName="id")
     */
    private $DatosFamiliaresHermano;

    /**
     * @ORM\OneToOne(targetEntity="DatosFamiliaresMadre", mappedBy="datosFamiliaresMadre",cascade={"persist"})
     * @ORM\JoinColumn(name="idDatosFamiliaresMadre", referencedColumnName="id")
     */
    private $DatosFamiliaresMadre;

    /**
     * @ORM\OneToOne(targetEntity="DatosFamiliaresPadre", mappedBy="datosFamiliaresPadre",cascade={"persist"})
     * @ORM\JoinColumn(name="idDatosFamiliaresPadre", referencedColumnName="id")
     */
    private $DatosFamiliaresPadre;
     /**
     * @ORM\OneToOne(targetEntity="EstadoSaludEnfermedades", mappedBy="estadoSaludEnfermedades",cascade={"persist"})
     * @ORM\JoinColumn(name="idEstadoSaludEnfermedades", referencedColumnName="id")
     */
    private $EstadoSaludEnfermedades;

    /**
     * @ORM\Column(name="hc_ae_descripcion", type="text", nullable=true)
     */
    private $descripcionAntecedentesAtencion;
    /**
     * @ORM\Column(name="hc_ae_examenes_descripcion", type="text", nullable=true)
     */
    private $descripcionAntecedentesAtencionExamenes;
    /**
     * @ORM\Column(name="hc_af_descripcion", type="text", nullable=true)
     */
    private $descripcionAntecedentesFamiliares;

    /**
     * @ORM\Column(name="hc_emb_descripcionEmb", type="text", nullable=true)
     */
    private $descripcionEmbarazo;

    /**
     * @ORM\OneToOne(targetEntity="HabitosHijo", mappedBy="habitosHijo",cascade={"persist"})
     * @ORM\JoinColumn(name="idHabitosHijo", referencedColumnName="id")
     
    private $habitosHijo;
    
    /**
     * @ORM\OneToMany(targetEntity="HistoriaEstadoSalud", mappedBy="historiaES",cascade={"persist"})
     * @ORM\JoinColumn(name="idHistoriaEstadoSalud", referencedColumnName="id")
    
    private $historiaEstadoSalud;
 */
    /**
     * @ORM\OneToMany(targetEntity="HistoriaInstitucional", mappedBy="historiaINST",cascade={"persist"})
     * @ORM\JoinColumn(name="idHistoriaInstitucional", referencedColumnName="id")
     */
    private $historiaInstitucional;

    /**
     * @ORM\OneToOne(targetEntity="MotivoConsulta", mappedBy="motivoConsulta",cascade={"persist"})
     * @ORM\JoinColumn(name="idMotivoConsulta", referencedColumnName="id")
     */
    private $MotivoConsulta;

    /**
     * @ORM\OneToOne(targetEntity="NacimientoParto", mappedBy="nacimientoParto",cascade={"persist"})
     * @ORM\JoinColumn(name="idNacimientoParto", referencedColumnName="id")
     */
    private $nacimientoParto;

    /**
     * @ORM\OneToOne(targetEntity="Paciente", mappedBy="paciente",cascade={"persist"})
     * @ORM\JoinColumn(name="idPaciente", referencedColumnName="id")
     */
    private $paciente;

    /**
     * @ORM\OneToOne(targetEntity="TratamientoMedico", mappedBy="tratamientoMedico",cascade={"persist"})
     * @ORM\JoinColumn(name="idTratamientoMedico", referencedColumnName="id")
     */
    private $tratamientoMedico;
    
    /*HABITO Y AUTONOMIA*/
    /**
     * @ORM\Column(name="hc_hab_auto_sueno", type="boolean", nullable=true)
     */
    private $hcHabAutSueno;
    /**
     * @ORM\Column(name="hc_hab_auto_alimenticio", type="boolean", nullable=true)
     */
    private $hcHabAutAlimenticio;
    /**
     * @ORM\Column(name="hc_hab_auto_alimenticiodesc", type="text", nullable=true)
     */
    private $hcHabAutAlimenticioDesc;
    /**
     * @ORM\Column(name="hc_hab_auto_higienicos", type="boolean", nullable=true)
     */
    private $hcHabAutHigienicos;
    /**
     * @ORM\Column(name="hc_hab_auto_higienicosdesc", type="text", nullable=true)
     */
    private $hcHabAutHigienicosDesc;
    /**
     * @ORM\Column(name="hc_hab_auto_control_esfinteres", type="boolean", nullable=true)
     */
    private $hcHabAutEsfinteres;
    /**
     * @ORM\Column(name="hc_hab_auto_control_esfinteresdesc", type="text", nullable=true)
     */
    private $hcHabAutEsfinteresDesc;

    /**
     * @ORM\Column(name="hc_hab_auto_autocuidado", type="boolean", nullable=true)
     */
    private $hcHabAutAutocuidado;
    /**
     * @ORM\Column(name="hc_hab_auto_autocuidadodesc", type="text", nullable=true)
     */
    private $hcHabAutAutocuidadoDesc;

    /**
     * @ORM\Column(name="hc_hab_auto_vestido", type="boolean", nullable=true)
     */
    private $hcHabAutVestido;

    /**
     * @ORM\Column(name="hc_hab_auto_vestidodesc", type="text", nullable=true)
     */
    private $hcHabAutVestidoDesc;

    /**
     * @ORM\Column(name="hc_hab_auto_juego_rutinas", type="boolean", nullable=true)
     */
    private $hcHabAutJuegoRutinas;
    /**
     * @ORM\Column(name="hc_hab_auto_juego_rutinasdesc", type="text", nullable=true)
     */
    private $hcHabAutJuegoRutinasDesc;
    /**
     * @ORM\Column(name="hc_hab_auto_otros", type="boolean", nullable=true)
     */
    private $hcHabAutOtros; 
     /**
     * @ORM\Column(name="hc_hab_auto_otrosdesc", type="text", nullable=true)
     */
    private $hcHabAutOtrosDesc;
    /**
     * @ORM\Column(name="hc_hab_auto_suenodesc", type="text", nullable=true)
     */
    private $hcHabAutSuenoDescripcion;
    /**
     * @ORM\Column(name="hc_descripcion_final", type="text", nullable=true)
     */
    private $hcDescripcionFinal;

    /**
     * Filtrable
     * @ORM\ManyToOne(targetEntity="Reica\UsuarioBundle\Entity\Usuario")
     */
    private $profesionalAtiende;

    /**
     * @ORM\Column(name="caso_activo", type="boolean", nullable=true)
     */
    private $estadoCaso;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return HistoriaClinica
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set hcDgnEdad
     *
     * @param integer $hcDgnEdad
     * @return HistoriaClinica
     */
    public function setHcDgnEdad($hcDgnEdad)
    {
        $this->hcDgnEdad = $hcDgnEdad;

        return $this;
    }

    /**
     * Get hcDgnEdad
     *
     * @return integer 
     */
    public function getHcDgnEdad()
    {
        return $this->hcDgnEdad;
    }

    /**
     * Set hcDgnSexo
     *
     * @param integer $hcDgnSexo
     * @return HistoriaClinica
     */
    public function setHcDgnSexo($hcDgnSexo)
    {
        $this->hcDgnSexo = $hcDgnSexo;

        return $this;
    }

    /**
     * Get hcDgnSexo
     *
     * @return integer 
     */
    public function getHcDgnSexo()
    {
        return $this->hcDgnSexo;
    }

    /**
     * Set hcDgnEdadMadre
     *
     * @param integer $hcDgnEdadMadre
     * @return HistoriaClinica
     */
    public function setHcDgnEdadMadre($hcDgnEdadMadre)
    {
        $this->hcDgnEdadMadre = $hcDgnEdadMadre;

        return $this;
    }

    /**
     * Get hcDgnEdadMadre
     *
     * @return integer 
     */
    public function getHcDgnEdadMadre()
    {
        return $this->hcDgnEdadMadre;
    }

    /**
     * Set hcDgnEdadPadre
     *
     * @param integer $hcDgnEdadPadre
     * @return HistoriaClinica
     */
    public function setHcDgnEdadPadre($hcDgnEdadPadre)
    {
        $this->hcDgnEdadPadre = $hcDgnEdadPadre;

        return $this;
    }

    /**
     * Get hcDgnEdadPadre
     *
     * @return integer 
     */
    public function getHcDgnEdadPadre()
    {
        return $this->hcDgnEdadPadre;
    }

    /**
     * Set hcDgnEdadTutor
     *
     * @param integer $hcDgnEdadTutor
     * @return HistoriaClinica
     */
    public function setHcDgnEdadTutor($hcDgnEdadTutor)
    {
        $this->hcDgnEdadTutor = $hcDgnEdadTutor;

        return $this;
    }

    /**
     * Get hcDgnEdadTutor
     *
     * @return integer 
     */
    public function getHcDgnEdadTutor()
    {
        return $this->hcDgnEdadTutor;
    }

    /**
     * Set hcRemColegio
     *
     * @param boolean $hcRemColegio
     * @return HistoriaClinica
     */
    public function setHcRemColegio($hcRemColegio)
    {
        $this->hcRemColegio = $hcRemColegio;

        return $this;
    }

    /**
     * Get hcRemColegio
     *
     * @return boolean 
     */
    public function getHcRemColegio()
    {
        return $this->hcRemColegio;
    }

    /**
     * Set hcRemNenopediatra
     *
     * @param boolean $hcRemNenopediatra
     * @return HistoriaClinica
     */
    public function setHcRemNenopediatra($hcRemNenopediatra)
    {
        $this->hcRemNenopediatra = $hcRemNenopediatra;

        return $this;
    }

    /**
     * Get hcRemNenopediatra
     *
     * @return boolean 
     */
    public function getHcRemNenopediatra()
    {
        return $this->hcRemNenopediatra;
    }

    /**
     * Set hcRemPsiquiatra
     *
     * @param boolean $hcRemPsiquiatra
     * @return HistoriaClinica
     */
    public function setHcRemPsiquiatra($hcRemPsiquiatra)
    {
        $this->hcRemPsiquiatra = $hcRemPsiquiatra;

        return $this;
    }

    /**
     * Get hcRemPsiquiatra
     *
     * @return boolean 
     */
    public function getHcRemPsiquiatra()
    {
        return $this->hcRemPsiquiatra;
    }

    /**
     * Set hcRemPediatra
     *
     * @param boolean $hcRemPediatra
     * @return HistoriaClinica
     */
    public function setHcRemPediatra($hcRemPediatra)
    {
        $this->hcRemPediatra = $hcRemPediatra;

        return $this;
    }

    /**
     * Get hcRemPediatra
     *
     * @return boolean 
     */
    public function getHcRemPediatra()
    {
        return $this->hcRemPediatra;
    }

    /**
     * Set hcRemFamiliar
     *
     * @param boolean $hcRemFamiliar
     * @return HistoriaClinica
     */
    public function setHcRemFamiliar($hcRemFamiliar)
    {
        $this->hcRemFamiliar = $hcRemFamiliar;

        return $this;
    }

    /**
     * Get hcRemFamiliar
     *
     * @return boolean 
     */
    public function getHcRemFamiliar()
    {
        return $this->hcRemFamiliar;
    }

    /**
     * Set hcRemBusqueda
     *
     * @param boolean $hcRemBusqueda
     * @return HistoriaClinica
     */
    public function setHcRemBusqueda($hcRemBusqueda)
    {
        $this->hcRemBusqueda = $hcRemBusqueda;

        return $this;
    }

    /**
     * Get hcRemBusqueda
     *
     * @return boolean 
     */
    public function getHcRemBusqueda()
    {
        return $this->hcRemBusqueda;
    }

    /**
     * Set hcRemOtro
     *
     * @param boolean $hcRemOtro
     * @return HistoriaClinica
     */
    public function setHcRemOtro($hcRemOtro)
    {
        $this->hcRemOtro = $hcRemOtro;

        return $this;
    }

    /**
     * Get hcRemOtro
     *
     * @return boolean 
     */
    public function getHcRemOtro()
    {
        return $this->hcRemOtro;
    }

    /**
     * Set hcRemTerapiaLenguaje
     *
     * @param boolean $hcRemTerapiaLenguaje
     * @return HistoriaClinica
     */
    public function setHcRemTerapiaLenguaje($hcRemTerapiaLenguaje)
    {
        $this->hcRemTerapiaLenguaje = $hcRemTerapiaLenguaje;

        return $this;
    }

    /**
     * Get hcRemTerapiaLenguaje
     *
     * @return boolean 
     */
    public function getHcRemTerapiaLenguaje()
    {
        return $this->hcRemTerapiaLenguaje;
    }

    /**
     * Set hcRemPsicologo
     *
     * @param boolean $hcRemPsicologo
     * @return HistoriaClinica
     */
    public function setHcRemPsicologo($hcRemPsicologo)
    {
        $this->hcRemPsicologo = $hcRemPsicologo;

        return $this;
    }

    /**
     * Get hcRemPsicologo
     *
     * @return boolean 
     */
    public function getHcRemPsicologo()
    {
        return $this->hcRemPsicologo;
    }

    /**
     * Set hcAdaNenopediatra
     *
     * @param boolean $hcAdaNenopediatra
     * @return HistoriaClinica
     */
    public function setHcAdaNenopediatra($hcAdaNenopediatra)
    {
        $this->hcAdaNenopediatra = $hcAdaNenopediatra;

        return $this;
    }

    /**
     * Get hcAdaNenopediatra
     *
     * @return boolean 
     */
    public function getHcAdaNenopediatra()
    {
        return $this->hcAdaNenopediatra;
    }

    /**
     * Set hcAdaTerapiaLenguaje
     *
     * @param boolean $hcAdaTerapiaLenguaje
     * @return HistoriaClinica
     */
    public function setHcAdaTerapiaLenguaje($hcAdaTerapiaLenguaje)
    {
        $this->hcAdaTerapiaLenguaje = $hcAdaTerapiaLenguaje;

        return $this;
    }

    /**
     * Get hcAdaTerapiaLenguaje
     *
     * @return boolean 
     */
    public function getHcAdaTerapiaLenguaje()
    {
        return $this->hcAdaTerapiaLenguaje;
    }

    /**
     * Set hcAdaPsquiatra
     *
     * @param boolean $hcAdaPsquiatra
     * @return HistoriaClinica
     */
    public function setHcAdaPsquiatra($hcAdaPsquiatra)
    {
        $this->hcAdaPsquiatra = $hcAdaPsquiatra;

        return $this;
    }

    /**
     * Get hcAdaPsquiatra
     *
     * @return boolean 
     */
    public function getHcAdaPsquiatra()
    {
        return $this->hcAdaPsquiatra;
    }

    /**
     * Set hcAdaPsicologo
     *
     * @param boolean $hcAdaPsicologo
     * @return HistoriaClinica
     */
    public function setHcAdaPsicologo($hcAdaPsicologo)
    {
        $this->hcAdaPsicologo = $hcAdaPsicologo;

        return $this;
    }

    /**
     * Get hcAdaPsicologo
     *
     * @return boolean 
     */
    public function getHcAdaPsicologo()
    {
        return $this->hcAdaPsicologo;
    }

    /**
     * Set hcAdaTerapiaOcupacional
     *
     * @param boolean $hcAdaTerapiaOcupacional
     * @return HistoriaClinica
     */
    public function setHcAdaTerapiaOcupacional($hcAdaTerapiaOcupacional)
    {
        $this->hcAdaTerapiaOcupacional = $hcAdaTerapiaOcupacional;

        return $this;
    }

    /**
     * Get hcAdaTerapiaOcupacional
     *
     * @return boolean 
     */
    public function getHcAdaTerapiaOcupacional()
    {
        return $this->hcAdaTerapiaOcupacional;
    }

    /**
     * Set hcAdaPsicopedagogo
     *
     * @param boolean $hcAdaPsicopedagogo
     * @return HistoriaClinica
     */
    public function setHcAdaPsicopedagogo($hcAdaPsicopedagogo)
    {
        $this->hcAdaPsicopedagogo = $hcAdaPsicopedagogo;

        return $this;
    }

    /**
     * Get hcAdaPsicopedagogo
     *
     * @return boolean 
     */
    public function getHcAdaPsicopedagogo()
    {
        return $this->hcAdaPsicopedagogo;
    }

    /**
     * Set hcAdaFisioterapeuta
     *
     * @param boolean $hcAdaFisioterapeuta
     * @return HistoriaClinica
     */
    public function setHcAdaFisioterapeuta($hcAdaFisioterapeuta)
    {
        $this->hcAdaFisioterapeuta = $hcAdaFisioterapeuta;

        return $this;
    }

    /**
     * Get hcAdaFisioterapeuta
     *
     * @return boolean 
     */
    public function getHcAdaFisioterapeuta()
    {
        return $this->hcAdaFisioterapeuta;
    }

    /**
     * Set hcAdaOtro
     *
     * @param boolean $hcAdaOtro
     * @return HistoriaClinica
     */
    public function setHcAdaOtro($hcAdaOtro)
    {
        $this->hcAdaOtro = $hcAdaOtro;

        return $this;
    }

    /**
     * Get hcAdaOtro
     *
     * @return boolean 
     */
    public function getHcAdaOtro()
    {
        return $this->hcAdaOtro;
    }

    /**
     * Set hcExaElectroencefalograma
     *
     * @param boolean $hcExaElectroencefalograma
     * @return HistoriaClinica
     */
    public function setHcExaElectroencefalograma($hcExaElectroencefalograma)
    {
        $this->hcExaElectroencefalograma = $hcExaElectroencefalograma;

        return $this;
    }

    /**
     * Get hcExaElectroencefalograma
     *
     * @return boolean 
     */
    public function getHcExaElectroencefalograma()
    {
        return $this->hcExaElectroencefalograma;
    }

    /**
     * Set hcExaTac
     *
     * @param boolean $hcExaTac
     * @return HistoriaClinica
     */
    public function setHcExaTac($hcExaTac)
    {
        $this->hcExaTac = $hcExaTac;

        return $this;
    }

    /**
     * Get hcExaTac
     *
     * @return boolean 
     */
    public function getHcExaTac()
    {
        return $this->hcExaTac;
    }

    /**
     * Set hcExaPotencialesEmocados
     *
     * @param boolean $hcExaPotencialesEmocados
     * @return HistoriaClinica
     */
    public function setHcExaPotencialesEmocados($hcExaPotencialesEmocados)
    {
        $this->hcExaPotencialesEmocados = $hcExaPotencialesEmocados;

        return $this;
    }

    /**
     * Get hcExaPotencialesEmocados
     *
     * @return boolean 
     */
    public function getHcExaPotencialesEmocados()
    {
        return $this->hcExaPotencialesEmocados;
    }

    /**
     * Set hcExaValoracionGenetica
     *
     * @param boolean $hcExaValoracionGenetica
     * @return HistoriaClinica
     */
    public function setHcExaValoracionGenetica($hcExaValoracionGenetica)
    {
        $this->hcExaValoracionGenetica = $hcExaValoracionGenetica;

        return $this;
    }

    /**
     * Get hcExaValoracionGenetica
     *
     * @return boolean 
     */
    public function getHcExaValoracionGenetica()
    {
        return $this->hcExaValoracionGenetica;
    }

    /**
     * Set hcTamizMetabolico
     *
     * @param boolean $hcTamizMetabolico
     * @return HistoriaClinica
     */
    public function setHcTamizMetabolico($hcTamizMetabolico)
    {
        $this->hcTamizMetabolico = $hcTamizMetabolico;

        return $this;
    }

    /**
     * Get hcTamizMetabolico
     *
     * @return boolean 
     */
    public function getHcTamizMetabolico()
    {
        return $this->hcTamizMetabolico;
    }

    /**
     * Set hcExaResonanciaMagnetica
     *
     * @param boolean $hcExaResonanciaMagnetica
     * @return HistoriaClinica
     */
    public function setHcExaResonanciaMagnetica($hcExaResonanciaMagnetica)
    {
        $this->hcExaResonanciaMagnetica = $hcExaResonanciaMagnetica;

        return $this;
    }

    /**
     * Get hcExaResonanciaMagnetica
     *
     * @return boolean 
     */
    public function getHcExaResonanciaMagnetica()
    {
        return $this->hcExaResonanciaMagnetica;
    }

    /**
     * Set hsEvalNenopsicologica
     *
     * @param boolean $hsEvalNenopsicologica
     * @return HistoriaClinica
     */
    public function setHsEvalNenopsicologica($hsEvalNenopsicologica)
    {
        $this->hsEvalNenopsicologica = $hsEvalNenopsicologica;

        return $this;
    }

    /**
     * Get hsEvalNenopsicologica
     *
     * @return boolean 
     */
    public function getHsEvalNenopsicologica()
    {
        return $this->hsEvalNenopsicologica;
    }

    /**
     * Set hcExaEvalLenguaje
     *
     * @param boolean $hcExaEvalLenguaje
     * @return HistoriaClinica
     */
    public function setHcExaEvalLenguaje($hcExaEvalLenguaje)
    {
        $this->hcExaEvalLenguaje = $hcExaEvalLenguaje;

        return $this;
    }

    /**
     * Get hcExaEvalLenguaje
     *
     * @return boolean 
     */
    public function getHcExaEvalLenguaje()
    {
        return $this->hcExaEvalLenguaje;
    }

    /**
     * Set hcExaOtra
     *
     * @param boolean $hcExaOtra
     * @return HistoriaClinica
     */
    public function setHcExaOtra($hcExaOtra)
    {
        $this->hcExaOtra = $hcExaOtra;

        return $this;
    }

    /**
     * Get hcExaOtra
     *
     * @return boolean 
     */
    public function getHcExaOtra()
    {
        return $this->hcExaOtra;
    }

    /**
     * Set hcEmbEdadMama
     *
     * @param integer $hcEmbEdadMama
     * @return HistoriaClinica
     */
    public function setHcEmbEdadMama($hcEmbEdadMama)
    {
        $this->hcEmbEdadMama = $hcEmbEdadMama;

        return $this;
    }

    /**
     * Get hcEmbEdadMama
     *
     * @return integer 
     */
    public function getHcEmbEdadMama()
    {
        return $this->hcEmbEdadMama;
    }

    /**
     * Set hcEmbEdadPapa
     *
     * @param integer $hcEmbEdadPapa
     * @return HistoriaClinica
     */
    public function setHcEmbEdadPapa($hcEmbEdadPapa)
    {
        $this->hcEmbEdadPapa = $hcEmbEdadPapa;

        return $this;
    }

    /**
     * Get hcEmbEdadPapa
     *
     * @return integer 
     */
    public function getHcEmbEdadPapa()
    {
        return $this->hcEmbEdadPapa;
    }

    /**
     * Set hcEmbAbortosMama
     *
     * @param boolean $hcEmbAbortosMama
     * @return HistoriaClinica
     */
    public function setHcEmbAbortosMama($hcEmbAbortosMama)
    {
        $this->hcEmbAbortosMama = $hcEmbAbortosMama;

        return $this;
    }

    /**
     * Get hcEmbAbortosMama
     *
     * @return boolean 
     */
    public function getHcEmbAbortosMama()
    {
        return $this->hcEmbAbortosMama;
    }

    /**
     * Set hcEmbAbortosPapa
     *
     * @param boolean $hcEmbAbortosPapa
     * @return HistoriaClinica
     */
    public function setHcEmbAbortosPapa($hcEmbAbortosPapa)
    {
        $this->hcEmbAbortosPapa = $hcEmbAbortosPapa;

        return $this;
    }

    /**
     * Get hcEmbAbortosPapa
     *
     * @return boolean 
     */
    public function getHcEmbAbortosPapa()
    {
        return $this->hcEmbAbortosPapa;
    }

    /**
     * Set hcEmbAcgMama
     *
     * @param boolean $hcEmbAcgMama
     * @return HistoriaClinica
     */
    public function setHcEmbAcgMama($hcEmbAcgMama)
    {
        $this->hcEmbAcgMama = $hcEmbAcgMama;

        return $this;
    }

    /**
     * Get hcEmbAcgMama
     *
     * @return boolean 
     */
    public function getHcEmbAcgMama()
    {
        return $this->hcEmbAcgMama;
    }

    /**
     * Set hcEmbAcgPapa
     *
     * @param boolean $hcEmbAcgPapa
     * @return HistoriaClinica
     */
    public function setHcEmbAcgPapa($hcEmbAcgPapa)
    {
        $this->hcEmbAcgPapa = $hcEmbAcgPapa;

        return $this;
    }

    /**
     * Get hcEmbAcgPapa
     *
     * @return boolean 
     */
    public function getHcEmbAcgPapa()
    {
        return $this->hcEmbAcgPapa;
    }

    /**
     * Set hcEmbEdembMama
     *
     * @param boolean $hcEmbEdembMama
     * @return HistoriaClinica
     */
    public function setHcEmbEdembMama($hcEmbEdembMama)
    {
        $this->hcEmbEdembMama = $hcEmbEdembMama;

        return $this;
    }

    /**
     * Get hcEmbEdembMama
     *
     * @return boolean 
     */
    public function getHcEmbEdembMama()
    {
        return $this->hcEmbEdembMama;
    }

    /**
     * Set hsEmbEdembPapa
     *
     * @param boolean $hsEmbEdembPapa
     * @return HistoriaClinica
     */
    public function setHsEmbEdembPapa($hsEmbEdembPapa)
    {
        $this->hsEmbEdembPapa = $hsEmbEdembPapa;

        return $this;
    }

    /**
     * Get hsEmbEdembPapa
     *
     * @return boolean 
     */
    public function getHsEmbEdembPapa()
    {
        return $this->hsEmbEdembPapa;
    }

    /**
     * Set hcEmbHtoxMama
     *
     * @param boolean $hcEmbHtoxMama
     * @return HistoriaClinica
     */
    public function setHcEmbHtoxMama($hcEmbHtoxMama)
    {
        $this->hcEmbHtoxMama = $hcEmbHtoxMama;

        return $this;
    }

    /**
     * Get hcEmbHtoxMama
     *
     * @return boolean 
     */
    public function getHcEmbHtoxMama()
    {
        return $this->hcEmbHtoxMama;
    }

    /**
     * Set hcEmbHtoxPapa
     *
     * @param boolean $hcEmbHtoxPapa
     * @return HistoriaClinica
     */
    public function setHcEmbHtoxPapa($hcEmbHtoxPapa)
    {
        $this->hcEmbHtoxPapa = $hcEmbHtoxPapa;

        return $this;
    }

    /**
     * Get hcEmbHtoxPapa
     *
     * @return boolean 
     */
    public function getHcEmbHtoxPapa()
    {
        return $this->hcEmbHtoxPapa;
    }

    /**
     * Set hcEmbEadMama
     *
     * @param boolean $hcEmbEadMama
     * @return HistoriaClinica
     */
    public function setHcEmbEadMama($hcEmbEadMama)
    {
        $this->hcEmbEadMama = $hcEmbEadMama;

        return $this;
    }

    /**
     * Get hcEmbEadMama
     *
     * @return boolean 
     */
    public function getHcEmbEadMama()
    {
        return $this->hcEmbEadMama;
    }

    /**
     * Set hcEmnEadPapa
     *
     * @param boolean $hcEmnEadPapa
     * @return HistoriaClinica
     */
    public function setHcEmnEadPapa($hcEmnEadPapa)
    {
        $this->hcEmnEadPapa = $hcEmnEadPapa;

        return $this;
    }

    /**
     * Get hcEmnEadPapa
     *
     * @return boolean 
     */
    public function getHcEmnEadPapa()
    {
        return $this->hcEmnEadPapa;
    }

    /**
     * Set hcEmbIaalimMama
     *
     * @param boolean $hcEmbIaalimMama
     * @return HistoriaClinica
     */
    public function setHcEmbIaalimMama($hcEmbIaalimMama)
    {
        $this->hcEmbIaalimMama = $hcEmbIaalimMama;

        return $this;
    }

    /**
     * Get hcEmbIaalimMama
     *
     * @return boolean 
     */
    public function getHcEmbIaalimMama()
    {
        return $this->hcEmbIaalimMama;
    }

    /**
     * Set hcEmnIaalimPapa
     *
     * @param boolean $hcEmnIaalimPapa
     * @return HistoriaClinica
     */
    public function setHcEmnIaalimPapa($hcEmnIaalimPapa)
    {
        $this->hcEmnIaalimPapa = $hcEmnIaalimPapa;

        return $this;
    }

    /**
     * Get hcEmnIaalimPapa
     *
     * @return boolean 
     */
    public function getHcEmnIaalimPapa()
    {
        return $this->hcEmnIaalimPapa;
    }

    /**
     * Set hcEmbMedicamentosMama
     *
     * @param boolean $hcEmbMedicamentosMama
     * @return HistoriaClinica
     */
    public function setHcEmbMedicamentosMama($hcEmbMedicamentosMama)
    {
        $this->hcEmbMedicamentosMama = $hcEmbMedicamentosMama;

        return $this;
    }

    /**
     * Get hcEmbMedicamentosMama
     *
     * @return boolean 
     */
    public function getHcEmbMedicamentosMama()
    {
        return $this->hcEmbMedicamentosMama;
    }

    /**
     * Set hcEmbMedicamentosPapa
     *
     * @param boolean $hcEmbMedicamentosPapa
     * @return HistoriaClinica
     */
    public function setHcEmbMedicamentosPapa($hcEmbMedicamentosPapa)
    {
        $this->hcEmbMedicamentosPapa = $hcEmbMedicamentosPapa;

        return $this;
    }

    /**
     * Get hcEmbMedicamentosPapa
     *
     * @return boolean 
     */
    public function getHcEmbMedicamentosPapa()
    {
        return $this->hcEmbMedicamentosPapa;
    }

    /**
     * Set hcEmbPesoOkMama
     *
     * @param boolean $hcEmbPesoOkMama
     * @return HistoriaClinica
     */
    public function setHcEmbPesoOkMama($hcEmbPesoOkMama)
    {
        $this->hcEmbPesoOkMama = $hcEmbPesoOkMama;

        return $this;
    }

    /**
     * Get hcEmbPesoOkMama
     *
     * @return boolean 
     */
    public function getHcEmbPesoOkMama()
    {
        return $this->hcEmbPesoOkMama;
    }

    /**
     * Set hcEmbPesoOkPapa
     *
     * @param boolean $hcEmbPesoOkPapa
     * @return HistoriaClinica
     */
    public function setHcEmbPesoOkPapa($hcEmbPesoOkPapa)
    {
        $this->hcEmbPesoOkPapa = $hcEmbPesoOkPapa;

        return $this;
    }

    /**
     * Get hcEmbPesoOkPapa
     *
     * @return boolean 
     */
    public function getHcEmbPesoOkPapa()
    {
        return $this->hcEmbPesoOkPapa;
    }

    /**
     * Set hcEmbExperiodMama
     *
     * @param boolean $hcEmbExperiodMama
     * @return HistoriaClinica
     */
    public function setHcEmbExperiodMama($hcEmbExperiodMama)
    {
        $this->hcEmbExperiodMama = $hcEmbExperiodMama;

        return $this;
    }

    /**
     * Get hcEmbExperiodMama
     *
     * @return boolean 
     */
    public function getHcEmbExperiodMama()
    {
        return $this->hcEmbExperiodMama;
    }

    /**
     * Set hcEmbExperiodPapa
     *
     * @param boolean $hcEmbExperiodPapa
     * @return HistoriaClinica
     */
    public function setHcEmbExperiodPapa($hcEmbExperiodPapa)
    {
        $this->hcEmbExperiodPapa = $hcEmbExperiodPapa;

        return $this;
    }

    /**
     * Get hcEmbExperiodPapa
     *
     * @return boolean 
     */
    public function getHcEmbExperiodPapa()
    {
        return $this->hcEmbExperiodPapa;
    }

    /**
     * Set hcNacpPartoNatural
     *
     * @param boolean $hcNacpPartoNatural
     * @return HistoriaClinica
     */
    public function setHcNacpPartoNatural($hcNacpPartoNatural)
    {
        $this->hcNacpPartoNatural = $hcNacpPartoNatural;

        return $this;
    }

    /**
     * Get hcNacpPartoNatural
     *
     * @return boolean 
     */
    public function getHcNacpPartoNatural()
    {
        return $this->hcNacpPartoNatural;
    }

    /**
     * Set hcNacpPartoTermino
     *
     * @param boolean $hcNacpPartoTermino
     * @return HistoriaClinica
     */
    public function setHcNacpPartoTermino($hcNacpPartoTermino)
    {
        $this->hcNacpPartoTermino = $hcNacpPartoTermino;

        return $this;
    }

    /**
     * Get hcNacpPartoTermino
     *
     * @return boolean 
     */
    public function getHcNacpPartoTermino()
    {
        return $this->hcNacpPartoTermino;
    }

    /**
     * Set hcNacpPartoPretermino
     *
     * @param boolean $hcNacpPartoPretermino
     * @return HistoriaClinica
     */
    public function setHcNacpPartoPretermino($hcNacpPartoPretermino)
    {
        $this->hcNacpPartoPretermino = $hcNacpPartoPretermino;

        return $this;
    }

    /**
     * Get hcNacpPartoPretermino
     *
     * @return boolean 
     */
    public function getHcNacpPartoPretermino()
    {
        return $this->hcNacpPartoPretermino;
    }

    /**
     * Set hcNacpPartoPostermino
     *
     * @param boolean $hcNacpPartoPostermino
     * @return HistoriaClinica
     */
    public function setHcNacpPartoPostermino($hcNacpPartoPostermino)
    {
        $this->hcNacpPartoPostermino = $hcNacpPartoPostermino;

        return $this;
    }

    /**
     * Get hcNacpPartoPostermino
     *
     * @return boolean 
     */
    public function getHcNacpPartoPostermino()
    {
        return $this->hcNacpPartoPostermino;
    }

    /**
     * Set hcNacpLlantoEspontaneo
     *
     * @param boolean $hcNacpLlantoEspontaneo
     * @return HistoriaClinica
     */
    public function setHcNacpLlantoEspontaneo($hcNacpLlantoEspontaneo)
    {
        $this->hcNacpLlantoEspontaneo = $hcNacpLlantoEspontaneo;

        return $this;
    }

    /**
     * Get hcNacpLlantoEspontaneo
     *
     * @return boolean 
     */
    public function getHcNacpLlantoEspontaneo()
    {
        return $this->hcNacpLlantoEspontaneo;
    }

    /**
     * Set hcNacpPesoNormal
     *
     * @param boolean $hcNacpPesoNormal
     * @return HistoriaClinica
     */
    public function setHcNacpPesoNormal($hcNacpPesoNormal)
    {
        $this->hcNacpPesoNormal = $hcNacpPesoNormal;

        return $this;
    }

    /**
     * Get hcNacpPesoNormal
     *
     * @return boolean 
     */
    public function getHcNacpPesoNormal()
    {
        return $this->hcNacpPesoNormal;
    }

    /**
     * Set hcNacpPesoBajo
     *
     * @param boolean $hcNacpPesoBajo
     * @return HistoriaClinica
     */
    public function setHcNacpPesoBajo($hcNacpPesoBajo)
    {
        $this->hcNacpPesoBajo = $hcNacpPesoBajo;

        return $this;
    }

    /**
     * Get hcNacpPesoBajo
     *
     * @return boolean 
     */
    public function getHcNacpPesoBajo()
    {
        return $this->hcNacpPesoBajo;
    }

    /**
     * Set hcNacpPesoSobre
     *
     * @param boolean $hcNacpPesoSobre
     * @return HistoriaClinica
     */
    public function setHcNacpPesoSobre($hcNacpPesoSobre)
    {
        $this->hcNacpPesoSobre = $hcNacpPesoSobre;

        return $this;
    }

    /**
     * Get hcNacpPesoSobre
     *
     * @return boolean 
     */
    public function getHcNacpPesoSobre()
    {
        return $this->hcNacpPesoSobre;
    }

    /**
     * Set hcNacpSufrimientoFetal
     *
     * @param boolean $hcNacpSufrimientoFetal
     * @return HistoriaClinica
     */
    public function setHcNacpSufrimientoFetal($hcNacpSufrimientoFetal)
    {
        $this->hcNacpSufrimientoFetal = $hcNacpSufrimientoFetal;

        return $this;
    }

    /**
     * Get hcNacpSufrimientoFetal
     *
     * @return boolean 
     */
    public function getHcNacpSufrimientoFetal()
    {
        return $this->hcNacpSufrimientoFetal;
    }

    /**
     * Set hcNacpSufrimientoFetalDesc
     *
     * @param string $hcNacpSufrimientoFetalDesc
     * @return HistoriaClinica
     */
    public function setHcNacpSufrimientoFetalDesc($hcNacpSufrimientoFetalDesc)
    {
        $this->hcNacpSufrimientoFetalDesc = $hcNacpSufrimientoFetalDesc;

        return $this;
    }

    /**
     * Get hcNacpSufrimientoFetalDesc
     *
     * @return string 
     */
    public function getHcNacpSufrimientoFetalDesc()
    {
        return $this->hcNacpSufrimientoFetalDesc;
    }

    /**
     * Set hcCdesTnpav
     *
     * @param boolean $hcCdesTnpav
     * @return HistoriaClinica
     */
    public function setHcCdesTnpav($hcCdesTnpav)
    {
        $this->hcCdesTnpav = $hcCdesTnpav;

        return $this;
    }

    /**
     * Get hcCdesTnpav
     *
     * @return boolean 
     */
    public function getHcCdesTnpav()
    {
        return $this->hcCdesTnpav;
    }

    /**
     * Set hcCdesEdadCca
     *
     * @param integer $hcCdesEdadCca
     * @return HistoriaClinica
     */
    public function setHcCdesEdadCca($hcCdesEdadCca)
    {
        $this->hcCdesEdadCca = $hcCdesEdadCca;

        return $this;
    }

    /**
     * Get hcCdesEdadCca
     *
     * @return integer 
     */
    public function getHcCdesEdadCca()
    {
        return $this->hcCdesEdadCca;
    }

    /**
     * Set hcDsmEdadSrca
     *
     * @param integer $hcDsmEdadSrca
     * @return HistoriaClinica
     */
    public function setHcDsmEdadSrca($hcDsmEdadSrca)
    {
        $this->hcDsmEdadSrca = $hcDsmEdadSrca;

        return $this;
    }

    /**
     * Get hcDsmEdadSrca
     *
     * @return integer 
     */
    public function getHcDsmEdadSrca()
    {
        return $this->hcDsmEdadSrca;
    }

    /**
     * Set hcDsmEdadSPadres
     *
     * @param integer $hcDsmEdadSPadres
     * @return HistoriaClinica
     */
    public function setHcDsmEdadSPadres($hcDsmEdadSPadres)
    {
        $this->hcDsmEdadSPadres = $hcDsmEdadSPadres;

        return $this;
    }

    /**
     * Get hcDsmEdadSPadres
     *
     * @return integer 
     */
    public function getHcDsmEdadSPadres()
    {
        return $this->hcDsmEdadSPadres;
    }

    /**
     * Set hcDsmEdadTomarObj
     *
     * @param integer $hcDsmEdadTomarObj
     * @return HistoriaClinica
     */
    public function setHcDsmEdadTomarObj($hcDsmEdadTomarObj)
    {
        $this->hcDsmEdadTomarObj = $hcDsmEdadTomarObj;

        return $this;
    }

    /**
     * Get hcDsmEdadTomarObj
     *
     * @return integer 
     */
    public function getHcDsmEdadTomarObj()
    {
        return $this->hcDsmEdadTomarObj;
    }

    /**
     * Set hcDsmEdadMobjfunc
     *
     * @param integer $hcDsmEdadMobjfunc
     * @return HistoriaClinica
     */
    public function setHcDsmEdadMobjfunc($hcDsmEdadMobjfunc)
    {
        $this->hcDsmEdadMobjfunc = $hcDsmEdadMobjfunc;

        return $this;
    }

    /**
     * Get hcDsmEdadMobjfunc
     *
     * @return integer 
     */
    public function getHcDsmEdadMobjfunc()
    {
        return $this->hcDsmEdadMobjfunc;
    }

    /**
     * Set hcDsmEdadGatear
     *
     * @param integer $hcDsmEdadGatear
     * @return HistoriaClinica
     */
    public function setHcDsmEdadGatear($hcDsmEdadGatear)
    {
        $this->hcDsmEdadGatear = $hcDsmEdadGatear;

        return $this;
    }

    /**
     * Get hcDsmEdadGatear
     *
     * @return integer 
     */
    public function getHcDsmEdadGatear()
    {
        return $this->hcDsmEdadGatear;
    }

    /**
     * Set hcDsmEdadCaminar
     *
     * @param integer $hcDsmEdadCaminar
     * @return HistoriaClinica
     */
    public function setHcDsmEdadCaminar($hcDsmEdadCaminar)
    {
        $this->hcDsmEdadCaminar = $hcDsmEdadCaminar;

        return $this;
    }

    /**
     * Get hcDsmEdadCaminar
     *
     * @return integer 
     */
    public function getHcDsmEdadCaminar()
    {
        return $this->hcDsmEdadCaminar;
    }

    /**
     * Set hcDsmEdadPrimerasPalabras
     *
     * @param integer $hcDsmEdadPrimerasPalabras
     * @return HistoriaClinica
     */
    public function setHcDsmEdadPrimerasPalabras($hcDsmEdadPrimerasPalabras)
    {
        $this->hcDsmEdadPrimerasPalabras = $hcDsmEdadPrimerasPalabras;

        return $this;
    }

    /**
     * Get hcDsmEdadPrimerasPalabras
     *
     * @return integer 
     */
    public function getHcDsmEdadPrimerasPalabras()
    {
        return $this->hcDsmEdadPrimerasPalabras;
    }

    /**
     * Set hcDsmEdadSoaint
     *
     * @param integer $hcDsmEdadSoaint
     * @return HistoriaClinica
     */
    public function setHcDsmEdadSoaint($hcDsmEdadSoaint)
    {
        $this->hcDsmEdadSoaint = $hcDsmEdadSoaint;

        return $this;
    }

    /**
     * Get hcDsmEdadSoaint
     *
     * @return integer 
     */
    public function getHcDsmEdadSoaint()
    {
        return $this->hcDsmEdadSoaint;
    }

    /**
     * Set hcDsmEdadMasen
     *
     * @param integer $hcDsmEdadMasen
     * @return HistoriaClinica
     */
    public function setHcDsmEdadMasen($hcDsmEdadMasen)
    {
        $this->hcDsmEdadMasen = $hcDsmEdadMasen;

        return $this;
    }

    /**
     * Get hcDsmEdadMasen
     *
     * @return integer 
     */
    public function getHcDsmEdadMasen()
    {
        return $this->hcDsmEdadMasen;
    }

    /**
     * Set hcDsmEdadJugar
     *
     * @param integer $hcDsmEdadJugar
     * @return HistoriaClinica
     */
    public function setHcDsmEdadJugar($hcDsmEdadJugar)
    {
        $this->hcDsmEdadJugar = $hcDsmEdadJugar;

        return $this;
    }

    /**
     * Get hcDsmEdadJugar
     *
     * @return integer 
     */
    public function getHcDsmEdadJugar()
    {
        return $this->hcDsmEdadJugar;
    }

    /**
     * Set hcDsmEdadResponder
     *
     * @param integer $hcDsmEdadResponder
     * @return HistoriaClinica
     */
    public function setHcDsmEdadResponder($hcDsmEdadResponder)
    {
        $this->hcDsmEdadResponder = $hcDsmEdadResponder;

        return $this;
    }

    /**
     * Get hcDsmEdadResponder
     *
     * @return integer 
     */
    public function getHcDsmEdadResponder()
    {
        return $this->hcDsmEdadResponder;
    }

    /**
     * Set hcHesMedicamentos
     *
     * @param string $hcHesMedicamentos
     * @return HistoriaClinica
     */
    public function setHcHesMedicamentos($hcHesMedicamentos)
    {
        $this->hcHesMedicamentos = $hcHesMedicamentos;

        return $this;
    }

    /**
     * Get hcHesMedicamentos
     *
     * @return string 
     */
    public function getHcHesMedicamentos()
    {
        return $this->hcHesMedicamentos;
    }

    /**
     * Set hcAfTeaMama
     *
     * @param boolean $hcAfTeaMama
     * @return HistoriaClinica
     */
    public function setHcAfTeaMama($hcAfTeaMama)
    {
        $this->hcAfTeaMama = $hcAfTeaMama;

        return $this;
    }

    /**
     * Get hcAfTeaMama
     *
     * @return boolean 
     */
    public function getHcAfTeaMama()
    {
        return $this->hcAfTeaMama;
    }

    /**
     * Set hcAfTeaPapa
     *
     * @param boolean $hcAfTeaPapa
     * @return HistoriaClinica
     */
    public function setHcAfTeaPapa($hcAfTeaPapa)
    {
        $this->hcAfTeaPapa = $hcAfTeaPapa;

        return $this;
    }

    /**
     * Get hcAfTeaPapa
     *
     * @return boolean 
     */
    public function getHcAfTeaPapa()
    {
        return $this->hcAfTeaPapa;
    }

    /**
     * Set hcAfTeaHermano
     *
     * @param boolean $hcAfTeaHermano
     * @return HistoriaClinica
     */
    public function setHcAfTeaHermano($hcAfTeaHermano)
    {
        $this->hcAfTeaHermano = $hcAfTeaHermano;

        return $this;
    }

    /**
     * Get hcAfTeaHermano
     *
     * @return boolean 
     */
    public function getHcAfTeaHermano()
    {
        return $this->hcAfTeaHermano;
    }

    /**
     * Set hcAfTdlMama
     *
     * @param boolean $hcAfTdlMama
     * @return HistoriaClinica
     */
    public function setHcAfTdlMama($hcAfTdlMama)
    {
        $this->hcAfTdlMama = $hcAfTdlMama;

        return $this;
    }

    /**
     * Get hcAfTdlMama
     *
     * @return boolean 
     */
    public function getHcAfTdlMama()
    {
        return $this->hcAfTdlMama;
    }

    /**
     * Set hcAfTdlPapa
     *
     * @param boolean $hcAfTdlPapa
     * @return HistoriaClinica
     */
    public function setHcAfTdlPapa($hcAfTdlPapa)
    {
        $this->hcAfTdlPapa = $hcAfTdlPapa;

        return $this;
    }

    /**
     * Get hcAfTdlPapa
     *
     * @return boolean 
     */
    public function getHcAfTdlPapa()
    {
        return $this->hcAfTdlPapa;
    }

    /**
     * Set hcAfTdlHermano
     *
     * @param boolean $hcAfTdlHermano
     * @return HistoriaClinica
     */
    public function setHcAfTdlHermano($hcAfTdlHermano)
    {
        $this->hcAfTdlHermano = $hcAfTdlHermano;

        return $this;
    }

    /**
     * Get hcAfTdlHermano
     *
     * @return boolean 
     */
    public function getHcAfTdlHermano()
    {
        return $this->hcAfTdlHermano;
    }

    /**
     * Set hcAfDintlMama
     *
     * @param boolean $hcAfDintlMama
     * @return HistoriaClinica
     */
    public function setHcAfDintlMama($hcAfDintlMama)
    {
        $this->hcAfDintlMama = $hcAfDintlMama;

        return $this;
    }

    /**
     * Get hcAfDintlMama
     *
     * @return boolean 
     */
    public function getHcAfDintlMama()
    {
        return $this->hcAfDintlMama;
    }

    /**
     * Set hcAfDintlPapa
     *
     * @param boolean $hcAfDintlPapa
     * @return HistoriaClinica
     */
    public function setHcAfDintlPapa($hcAfDintlPapa)
    {
        $this->hcAfDintlPapa = $hcAfDintlPapa;

        return $this;
    }

    /**
     * Get hcAfDintlPapa
     *
     * @return boolean 
     */
    public function getHcAfDintlPapa()
    {
        return $this->hcAfDintlPapa;
    }

    /**
     * Set hcAfDintlHermano
     *
     * @param boolean $hcAfDintlHermano
     * @return HistoriaClinica
     */
    public function setHcAfDintlHermano($hcAfDintlHermano)
    {
        $this->hcAfDintlHermano = $hcAfDintlHermano;

        return $this;
    }

    /**
     * Get hcAfDintlHermano
     *
     * @return boolean 
     */
    public function getHcAfDintlHermano()
    {
        return $this->hcAfDintlHermano;
    }

    /**
     * Set hcAfEnfpsqMama
     *
     * @param boolean $hcAfEnfpsqMama
     * @return HistoriaClinica
     */
    public function setHcAfEnfpsqMama($hcAfEnfpsqMama)
    {
        $this->hcAfEnfpsqMama = $hcAfEnfpsqMama;

        return $this;
    }

    /**
     * Get hcAfEnfpsqMama
     *
     * @return boolean 
     */
    public function getHcAfEnfpsqMama()
    {
        return $this->hcAfEnfpsqMama;
    }

    /**
     * Set hcAfEnfpsqPapa
     *
     * @param boolean $hcAfEnfpsqPapa
     * @return HistoriaClinica
     */
    public function setHcAfEnfpsqPapa($hcAfEnfpsqPapa)
    {
        $this->hcAfEnfpsqPapa = $hcAfEnfpsqPapa;

        return $this;
    }

    /**
     * Get hcAfEnfpsqPapa
     *
     * @return boolean 
     */
    public function getHcAfEnfpsqPapa()
    {
        return $this->hcAfEnfpsqPapa;
    }

    /**
     * Set hcAfEnfpsqHermano
     *
     * @param boolean $hcAfEnfpsqHermano
     * @return HistoriaClinica
     */
    public function setHcAfEnfpsqHermano($hcAfEnfpsqHermano)
    {
        $this->hcAfEnfpsqHermano = $hcAfEnfpsqHermano;

        return $this;
    }

    /**
     * Get hcAfEnfpsqHermano
     *
     * @return boolean 
     */
    public function getHcAfEnfpsqHermano()
    {
        return $this->hcAfEnfpsqHermano;
    }

    /**
     * Set hcAfTaprendMama
     *
     * @param boolean $hcAfTaprendMama
     * @return HistoriaClinica
     */
    public function setHcAfTaprendMama($hcAfTaprendMama)
    {
        $this->hcAfTaprendMama = $hcAfTaprendMama;

        return $this;
    }

    /**
     * Get hcAfTaprendMama
     *
     * @return boolean 
     */
    public function getHcAfTaprendMama()
    {
        return $this->hcAfTaprendMama;
    }

    /**
     * Set hcAfTaprendPapa
     *
     * @param boolean $hcAfTaprendPapa
     * @return HistoriaClinica
     */
    public function setHcAfTaprendPapa($hcAfTaprendPapa)
    {
        $this->hcAfTaprendPapa = $hcAfTaprendPapa;

        return $this;
    }

    /**
     * Get hcAfTaprendPapa
     *
     * @return boolean 
     */
    public function getHcAfTaprendPapa()
    {
        return $this->hcAfTaprendPapa;
    }

    /**
     * Set hcAfTaprenHermano
     *
     * @param boolean $hcAfTaprenHermano
     * @return HistoriaClinica
     */
    public function setHcAfTaprenHermano($hcAfTaprenHermano)
    {
        $this->hcAfTaprenHermano = $hcAfTaprenHermano;

        return $this;
    }

    /**
     * Get hcAfTaprenHermano
     *
     * @return boolean 
     */
    public function getHcAfTaprenHermano()
    {
        return $this->hcAfTaprenHermano;
    }

    /**
     * Set hcAfTpddaMama
     *
     * @param boolean $hcAfTpddaMama
     * @return HistoriaClinica
     */
    public function setHcAfTpddaMama($hcAfTpddaMama)
    {
        $this->hcAfTpddaMama = $hcAfTpddaMama;

        return $this;
    }

    /**
     * Get hcAfTpddaMama
     *
     * @return boolean 
     */
    public function getHcAfTpddaMama()
    {
        return $this->hcAfTpddaMama;
    }

    /**
     * Set hcAfTpddaPapa
     *
     * @param boolean $hcAfTpddaPapa
     * @return HistoriaClinica
     */
    public function setHcAfTpddaPapa($hcAfTpddaPapa)
    {
        $this->hcAfTpddaPapa = $hcAfTpddaPapa;

        return $this;
    }

    /**
     * Get hcAfTpddaPapa
     *
     * @return boolean 
     */
    public function getHcAfTpddaPapa()
    {
        return $this->hcAfTpddaPapa;
    }

    /**
     * Set hcAfTpddaHermano
     *
     * @param boolean $hcAfTpddaHermano
     * @return HistoriaClinica
     */
    public function setHcAfTpddaHermano($hcAfTpddaHermano)
    {
        $this->hcAfTpddaHermano = $hcAfTpddaHermano;

        return $this;
    }

    /**
     * Get hcAfTpddaHermano
     *
     * @return boolean 
     */
    public function getHcAfTpddaHermano()
    {
        return $this->hcAfTpddaHermano;
    }

    /**
     * Set hcAfTccMama
     *
     * @param boolean $hcAfTccMama
     * @return HistoriaClinica
     */
    public function setHcAfTccMama($hcAfTccMama)
    {
        $this->hcAfTccMama = $hcAfTccMama;

        return $this;
    }

    /**
     * Get hcAfTccMama
     *
     * @return boolean 
     */
    public function getHcAfTccMama()
    {
        return $this->hcAfTccMama;
    }

    /**
     * Set hcAfTccPapa
     *
     * @param boolean $hcAfTccPapa
     * @return HistoriaClinica
     */
    public function setHcAfTccPapa($hcAfTccPapa)
    {
        $this->hcAfTccPapa = $hcAfTccPapa;

        return $this;
    }

    /**
     * Get hcAfTccPapa
     *
     * @return boolean 
     */
    public function getHcAfTccPapa()
    {
        return $this->hcAfTccPapa;
    }

    /**
     * Set hcAfTccHermano
     *
     * @param boolean $hcAfTccHermano
     * @return HistoriaClinica
     */
    public function setHcAfTccHermano($hcAfTccHermano)
    {
        $this->hcAfTccHermano = $hcAfTccHermano;

        return $this;
    }

    /**
     * Get hcAfTccHermano
     *
     * @return boolean 
     */
    public function getHcAfTccHermano()
    {
        return $this->hcAfTccHermano;
    }

    /**
     * Set hcEsfvEdadControl
     *
     * @param integer $hcEsfvEdadControl
     * @return HistoriaClinica
     */
    public function setHcEsfvEdadControl($hcEsfvEdadControl)
    {
        $this->hcEsfvEdadControl = $hcEsfvEdadControl;

        return $this;
    }

    /**
     * Get hcEsfvEdadControl
     *
     * @return integer 
     */
    public function getHcEsfvEdadControl()
    {
        return $this->hcEsfvEdadControl;
    }

    /**
     * Set hcEsfvDificultades
     *
     * @param boolean $hcEsfvDificultades
     * @return HistoriaClinica
     */
    public function setHcEsfvDificultades($hcEsfvDificultades)
    {
        $this->hcEsfvDificultades = $hcEsfvDificultades;

        return $this;
    }

    /**
     * Get hcEsfvDificultades
     *
     * @return boolean 
     */
    public function getHcEsfvDificultades()
    {
        return $this->hcEsfvDificultades;
    }

    /**
     * Set hcEsfaEdadControl
     *
     * @param integer $hcEsfaEdadControl
     * @return HistoriaClinica
     */
    public function setHcEsfaEdadControl($hcEsfaEdadControl)
    {
        $this->hcEsfaEdadControl = $hcEsfaEdadControl;

        return $this;
    }

    /**
     * Get hcEsfaEdadControl
     *
     * @return integer 
     */
    public function getHcEsfaEdadControl()
    {
        return $this->hcEsfaEdadControl;
    }

    /**
     * Set hcEsfaDificultades
     *
     * @param boolean $hcEsfaDificultades
     * @return HistoriaClinica
     */
    public function setHcEsfaDificultades($hcEsfaDificultades)
    {
        $this->hcEsfaDificultades = $hcEsfaDificultades;

        return $this;
    }

    /**
     * Get hcEsfaDificultades
     *
     * @return boolean 
     */
    public function getHcEsfaDificultades()
    {
        return $this->hcEsfaDificultades;
    }

    /**
     * Set hcSuenoSolo
     *
     * @param boolean $hcSuenoSolo
     * @return HistoriaClinica
     */
    public function setHcSuenoSolo($hcSuenoSolo)
    {
        $this->hcSuenoSolo = $hcSuenoSolo;

        return $this;
    }

    /**
     * Get hcSuenoSolo
     *
     * @return boolean 
     */
    public function getHcSuenoSolo()
    {
        return $this->hcSuenoSolo;
    }

    /**
     * Set hcSuenoSuficiente
     *
     * @param boolean $hcSuenoSuficiente
     * @return HistoriaClinica
     */
    public function setHcSuenoSuficiente($hcSuenoSuficiente)
    {
        $this->hcSuenoSuficiente = $hcSuenoSuficiente;

        return $this;
    }

    /**
     * Get hcSuenoSuficiente
     *
     * @return boolean 
     */
    public function getHcSuenoSuficiente()
    {
        return $this->hcSuenoSuficiente;
    }

    /**
     * Set hcSuenoTranquilo
     *
     * @param boolean $hcSuenoTranquilo
     * @return HistoriaClinica
     */
    public function setHcSuenoTranquilo($hcSuenoTranquilo)
    {
        $this->hcSuenoTranquilo = $hcSuenoTranquilo;

        return $this;
    }

    /**
     * Get hcSuenoTranquilo
     *
     * @return boolean 
     */
    public function getHcSuenoTranquilo()
    {
        return $this->hcSuenoTranquilo;
    }

    /**
     * Set hcSuenoAlterado
     *
     * @param boolean $hcSuenoAlterado
     * @return HistoriaClinica
     */
    public function setHcSuenoAlterado($hcSuenoAlterado)
    {
        $this->hcSuenoAlterado = $hcSuenoAlterado;

        return $this;
    }

    /**
     * Get hcSuenoAlterado
     *
     * @return boolean 
     */
    public function getHcSuenoAlterado()
    {
        return $this->hcSuenoAlterado;
    }

    /**
     * Set hcSuenoPesadillas
     *
     * @param boolean $hcSuenoPesadillas
     * @return HistoriaClinica
     */
    public function setHcSuenoPesadillas($hcSuenoPesadillas)
    {
        $this->hcSuenoPesadillas = $hcSuenoPesadillas;

        return $this;
    }

    /**
     * Get hcSuenoPesadillas
     *
     * @return boolean 
     */
    public function getHcSuenoPesadillas()
    {
        return $this->hcSuenoPesadillas;
    }

    /**
     * Set hcSuenoGritos
     *
     * @param boolean $hcSuenoGritos
     * @return HistoriaClinica
     */
    public function setHcSuenoGritos($hcSuenoGritos)
    {
        $this->hcSuenoGritos = $hcSuenoGritos;

        return $this;
    }

    /**
     * Get hcSuenoGritos
     *
     * @return boolean 
     */
    public function getHcSuenoGritos()
    {
        return $this->hcSuenoGritos;
    }

    /**
     * Set hcSuenoSomniloquios
     *
     * @param boolean $hcSuenoSomniloquios
     * @return HistoriaClinica
     */
    public function setHcSuenoSomniloquios($hcSuenoSomniloquios)
    {
        $this->hcSuenoSomniloquios = $hcSuenoSomniloquios;

        return $this;
    }

    /**
     * Get hcSuenoSomniloquios
     *
     * @return boolean 
     */
    public function getHcSuenoSomniloquios()
    {
        return $this->hcSuenoSomniloquios;
    }

    /**
     * Set hcAlimSolo
     *
     * @param boolean $hcAlimSolo
     * @return HistoriaClinica
     */
    public function setHcAlimSolo($hcAlimSolo)
    {
        $this->hcAlimSolo = $hcAlimSolo;

        return $this;
    }

    /**
     * Get hcAlimSolo
     *
     * @return boolean 
     */
    public function getHcAlimSolo()
    {
        return $this->hcAlimSolo;
    }

    /**
     * Set hcAlimDietaFlexible
     *
     * @param boolean $hcAlimDietaFlexible
     * @return HistoriaClinica
     */
    public function setHcAlimDietaFlexible($hcAlimDietaFlexible)
    {
        $this->hcAlimDietaFlexible = $hcAlimDietaFlexible;

        return $this;
    }

    /**
     * Get hcAlimDietaFlexible
     *
     * @return boolean 
     */
    public function getHcAlimDietaFlexible()
    {
        return $this->hcAlimDietaFlexible;
    }

    /**
     * Set hcAlimCosasRaras
     *
     * @param boolean $hcAlimCosasRaras
     * @return HistoriaClinica
     */
    public function setHcAlimCosasRaras($hcAlimCosasRaras)
    {
        $this->hcAlimCosasRaras = $hcAlimCosasRaras;

        return $this;
    }

    /**
     * Get hcAlimCosasRaras
     *
     * @return boolean 
     */
    public function getHcAlimCosasRaras()
    {
        return $this->hcAlimCosasRaras;
    }

    /**
     * Set hcAlimDosisAdecuada
     *
     * @param boolean $hcAlimDosisAdecuada
     * @return HistoriaClinica
     */
    public function setHcAlimDosisAdecuada($hcAlimDosisAdecuada)
    {
        $this->hcAlimDosisAdecuada = $hcAlimDosisAdecuada;

        return $this;
    }

    /**
     * Get hcAlimDosisAdecuada
     *
     * @return boolean 
     */
    public function getHcAlimDosisAdecuada()
    {
        return $this->hcAlimDosisAdecuada;
    }

    /**
     * Set hcHabitosPreocupacion
     *
     * @param boolean $hcHabitosPreocupacion
     * @return HistoriaClinica
     */
    public function setHcHabitosPreocupacion($hcHabitosPreocupacion)
    {
        $this->hcHabitosPreocupacion = $hcHabitosPreocupacion;

        return $this;
    }

    /**
     * Get hcHabitosPreocupacion
     *
     * @return boolean 
     */
    public function getHcHabitosPreocupacion()
    {
        return $this->hcHabitosPreocupacion;
    }

    /**
     * Set hcHiInstitucionesEducativas
     *
     * @param boolean $hcHiInstitucionesEducativas
     * @return HistoriaClinica
     */
    public function setHcHiInstitucionesEducativas($hcHiInstitucionesEducativas)
    {
        $this->hcHiInstitucionesEducativas = $hcHiInstitucionesEducativas;

        return $this;
    }

    /**
     * Get hcHiInstitucionesEducativas
     *
     * @return boolean 
     */
    public function getHcHiInstitucionesEducativas()
    {
        return $this->hcHiInstitucionesEducativas;
    }

    /**
     * Set hcNacpHospitalizacion
     *
     * @param boolean $hcNacpHospitalizacion
     * @return HistoriaClinica
     */
    public function setHcNacpHospitalizacion($hcNacpHospitalizacion)
    {
        $this->hcNacpHospitalizacion = $hcNacpHospitalizacion;

        return $this;
    }

    /**
     * Get hcNacpHospitalizacion
     *
     * @return boolean 
     */
    public function getHcNacpHospitalizacion()
    {
        return $this->hcNacpHospitalizacion;
    }

    /**
     * Set hcNacpAsistenciaRespiratoria
     *
     * @param boolean $hcNacpAsistenciaRespiratoria
     * @return HistoriaClinica
     */
    public function setHcNacpAsistenciaRespiratoria($hcNacpAsistenciaRespiratoria)
    {
        $this->hcNacpAsistenciaRespiratoria = $hcNacpAsistenciaRespiratoria;

        return $this;
    }

    /**
     * Get hcNacpAsistenciaRespiratoria
     *
     * @return boolean 
     */
    public function getHcNacpAsistenciaRespiratoria()
    {
        return $this->hcNacpAsistenciaRespiratoria;
    }

    /**
     * Set hcNacpIncubadora
     *
     * @param boolean $hcNacpIncubadora
     * @return HistoriaClinica
     */
    public function setHcNacpIncubadora($hcNacpIncubadora)
    {
        $this->hcNacpIncubadora = $hcNacpIncubadora;

        return $this;
    }

    /**
     * Get hcNacpIncubadora
     *
     * @return boolean 
     */
    public function getHcNacpIncubadora()
    {
        return $this->hcNacpIncubadora;
    }

    /**
     * Set hcNacpReanimacion
     *
     * @param boolean $hcNacpReanimacion
     * @return HistoriaClinica
     */
    public function setHcNacpReanimacion($hcNacpReanimacion)
    {
        $this->hcNacpReanimacion = $hcNacpReanimacion;

        return $this;
    }

    /**
     * Get hcNacpReanimacion
     *
     * @return boolean 
     */
    public function getHcNacpReanimacion()
    {
        return $this->hcNacpReanimacion;
    }

    /**
     * Set hcNacpTratFarmacologico
     *
     * @param boolean $hcNacpTratFarmacologico
     * @return HistoriaClinica
     */
    public function setHcNacpTratFarmacologico($hcNacpTratFarmacologico)
    {
        $this->hcNacpTratFarmacologico = $hcNacpTratFarmacologico;

        return $this;
    }

    /**
     * Get hcNacpTratFarmacologico
     *
     * @return boolean 
     */
    public function getHcNacpTratFarmacologico()
    {
        return $this->hcNacpTratFarmacologico;
    }

    /**
     * Set hcEstadoSaludActual
     *
     * @param string $hcEstadoSaludActual
     * @return HistoriaClinica
     */
    public function setHcEstadoSaludActual($hcEstadoSaludActual)
    {
        $this->hcEstadoSaludActual = $hcEstadoSaludActual;

        return $this;
    }

    /**
     * Get hcEstadoSaludActual
     *
     * @return string 
     */
    public function getHcEstadoSaludActual()
    {
        return $this->hcEstadoSaludActual;
    }

    /**
     * Set hcHiCentrosEducativosDesc
     *
     * @param string $hcHiCentrosEducativosDesc
     * @return HistoriaClinica
     */
    public function setHcHiCentrosEducativosDesc($hcHiCentrosEducativosDesc)
    {
        $this->hcHiCentrosEducativosDesc = $hcHiCentrosEducativosDesc;

        return $this;
    }

    /**
     * Get hcHiCentrosEducativosDesc
     *
     * @return string 
     */
    public function getHcHiCentrosEducativosDesc()
    {
        return $this->hcHiCentrosEducativosDesc;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->historiaEstadoSalud = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fechaCreacion = new \DateTime('now');    
        $this->historiaInstitucional = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set antecedentesFamiliaresOtro
     *
     * @param \MainBundle\Entity\AntecedentesFamiliaresOtro $antecedentesFamiliaresOtro
     * @return HistoriaClinica
     */
    public function setAntecedentesFamiliaresOtro(\MainBundle\Entity\AntecedentesFamiliaresOtro $antecedentesFamiliaresOtro = null)
    {
        $this->antecedentesFamiliaresOtro = $antecedentesFamiliaresOtro;

        return $this;
    }

    /**
     * Get antecedentesFamiliaresOtro
     *
     * @return \MainBundle\Entity\AntecedentesFamiliaresOtro 
     */
    public function getAntecedentesFamiliaresOtro()
    {
        return $this->antecedentesFamiliaresOtro;
    }

    /**
     * Set caracteristicasDesarrollo
     *
     * @param \MainBundle\Entity\CaracteristicasDesarrollo $caracteristicasDesarrollo
     * @return HistoriaClinica
     */
    public function setCaracteristicasDesarrollo(\MainBundle\Entity\CaracteristicasDesarrollo $caracteristicasDesarrollo = null)
    {
        $this->caracteristicasDesarrollo = $caracteristicasDesarrollo;

        return $this;
    }

    /**
     * Get caracteristicasDesarrollo
     *
     * @return \MainBundle\Entity\CaracteristicasDesarrollo 
     */
    public function getCaracteristicasDesarrollo()
    {
        return $this->caracteristicasDesarrollo;
    }

    /**
     * Set datosCentroTrabajo
     *
     * @param \MainBundle\Entity\DatosCentroTrabajo $datosCentroTrabajo
     * @return HistoriaClinica
     */
    public function setDatosCentroTrabajo(\MainBundle\Entity\DatosCentroTrabajo $datosCentroTrabajo = null)
    {
        $this->datosCentroTrabajo = $datosCentroTrabajo;

        return $this;
    }

    /**
     * Get datosCentroTrabajo
     *
     * @return \MainBundle\Entity\DatosCentroTrabajo 
     */
    public function getDatosCentroTrabajo()
    {
        return $this->datosCentroTrabajo;
    }

    /**
     * Set DatosFamiliaresHermano
     *
     * @param \MainBundle\Entity\DatosFamiliaresHermano $datosFamiliaresHermano
     * @return HistoriaClinica
     */
    public function setDatosFamiliaresHermano(\MainBundle\Entity\DatosFamiliaresHermano $datosFamiliaresHermano = null)
    {
        $this->DatosFamiliaresHermano = $datosFamiliaresHermano;

        return $this;
    }

    /**
     * Get DatosFamiliaresHermano
     *
     * @return \MainBundle\Entity\DatosFamiliaresHermano 
     */
    public function getDatosFamiliaresHermano()
    {
        return $this->DatosFamiliaresHermano;
    }

    /**
     * Set DatosFamiliaresMadre
     *
     * @param \MainBundle\Entity\DatosFamiliaresMadre $datosFamiliaresMadre
     * @return HistoriaClinica
     */
    public function setDatosFamiliaresMadre(\MainBundle\Entity\DatosFamiliaresMadre $datosFamiliaresMadre = null)
    {
        $this->DatosFamiliaresMadre = $datosFamiliaresMadre;

        return $this;
    }

    /**
     * Get DatosFamiliaresMadre
     *
     * @return \MainBundle\Entity\DatosFamiliaresMadre 
     */
    public function getDatosFamiliaresMadre()
    {
        return $this->DatosFamiliaresMadre;
    }

    /**
     * Set DatosFamiliaresPadre
     *
     * @param \MainBundle\Entity\DatosFamiliaresPadre $datosFamiliaresPadre
     * @return HistoriaClinica
     */
    public function setDatosFamiliaresPadre(\MainBundle\Entity\DatosFamiliaresPadre $datosFamiliaresPadre = null)
    {
        $this->DatosFamiliaresPadre = $datosFamiliaresPadre;

        return $this;
    }

    /**
     * Get DatosFamiliaresPadre
     *
     * @return \MainBundle\Entity\DatosFamiliaresPadre 
     */
    public function getDatosFamiliaresPadre()
    {
        return $this->DatosFamiliaresPadre;
    }

    /**
     * Set descripcionAntecedentesAtencion
     *
     * @param \MainBundle\Entity\DescripcionAntecedentesAtencion $descripcionAntecedentesAtencion
     * @return HistoriaClinica
     */
    public function setDescripcionAntecedentesAtencion($descripcionAntecedentesAtencion = null)
    {
        $this->descripcionAntecedentesAtencion = $descripcionAntecedentesAtencion;

        return $this;
    }

    /**
     * Get descripcionAntecedentesAtencion
     *
     * @return \MainBundle\Entity\DescripcionAntecedentesAtencion 
     */
    public function getDescripcionAntecedentesAtencion()
    {
        return $this->descripcionAntecedentesAtencion;
    }

    /**
     * Set descripcionEmbarazo
     *
     * @param \MainBundle\Entity\DescripcionEmbarazo $descripcionEmbarazo
     * @return HistoriaClinica
     */
    public function setDescripcionEmbarazo($descripcionEmbarazo = null)
    {
        $this->descripcionEmbarazo = $descripcionEmbarazo;

        return $this;
    }

    /**
     * Get descripcionEmbarazo
     *
     * @return \MainBundle\Entity\DescripcionEmbarazo 
     */
    public function getDescripcionEmbarazo()
    {
        return $this->descripcionEmbarazo;
    }

    /**
     * Set habitosHijo
     *
     * @param \MainBundle\Entity\HabitosHijo $habitosHijo
     * @return HistoriaClinica
     */
    public function setHabitosHijo(\MainBundle\Entity\HabitosHijo $habitosHijo = null)
    {
        $this->habitosHijo = $habitosHijo;

        return $this;
    }

    /**
     * Get habitosHijo
     *
     * @return \MainBundle\Entity\HabitosHijo 
     */
    public function getHabitosHijo()
    {
        return $this->habitosHijo;
    }

    /**
     * Add historiaEstadoSalud
     *
     * @param \MainBundle\Entity\HistoriaEstadoSalud $historiaEstadoSalud
     * @return HistoriaClinica
     */
    public function addHistoriaEstadoSalud(\MainBundle\Entity\HistoriaEstadoSalud $historiaEstadoSalud)
    {
        $this->historiaEstadoSalud[] = $historiaEstadoSalud;

        return $this;
    }

    /**
     * Remove historiaEstadoSalud
     *
     * @param \MainBundle\Entity\HistoriaEstadoSalud $historiaEstadoSalud
     */
    public function removeHistoriaEstadoSalud(\MainBundle\Entity\HistoriaEstadoSalud $historiaEstadoSalud)
    {
        $this->historiaEstadoSalud->removeElement($historiaEstadoSalud);
    }

    /**
     * Get historiaEstadoSalud
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoriaEstadoSalud()
    {
        return $this->historiaEstadoSalud;
    }

    /**
     * Add historiaInstitucional
     *
     * @param \MainBundle\Entity\HistoriaInstitucional $historiaInstitucional
     * @return HistoriaClinica
     */
    public function addHistoriaInstitucional(\MainBundle\Entity\HistoriaInstitucional $historiaInstitucional)
    {
        $this->historiaInstitucional[] = $historiaInstitucional;

        return $this;
    }

    /**
     * Remove historiaInstitucional
     *
     * @param \MainBundle\Entity\HistoriaInstitucional $historiaInstitucional
     */
    public function removeHistoriaInstitucional(\MainBundle\Entity\HistoriaInstitucional $historiaInstitucional)
    {
        $this->historiaInstitucional->removeElement($historiaInstitucional);
    }

    /**
     * Get historiaInstitucional
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoriaInstitucional()
    {
        return $this->historiaInstitucional;
    }

    /**
     * Set MotivoConsulta
     *
     * @param \MainBundle\Entity\MotivoConsulta $motivoConsulta
     * @return HistoriaClinica
     */
    public function setMotivoConsulta(\MainBundle\Entity\MotivoConsulta $motivoConsulta = null)
    {
        $this->MotivoConsulta = $motivoConsulta;

        return $this;
    }

    /**
     * Get MotivoConsulta
     *
     * @return \MainBundle\Entity\MotivoConsulta 
     */
    public function getMotivoConsulta()
    {
        return $this->MotivoConsulta;
    }

    /**
     * Set nacimientoParto
     *
     * @param \MainBundle\Entity\NacimientoParto $nacimientoParto
     * @return HistoriaClinica
     */
    public function setNacimientoParto(\MainBundle\Entity\NacimientoParto $nacimientoParto = null)
    {
        $this->nacimientoParto = $nacimientoParto;

        return $this;
    }

    /**
     * Get nacimientoParto
     *
     * @return \MainBundle\Entity\NacimientoParto 
     */
    public function getNacimientoParto()
    {
        return $this->nacimientoParto;
    }

    /**
     * Set paciente
     *
     * @param \MainBundle\Entity\Paciente $paciente
     * @return HistoriaClinica
     */
    public function setPaciente(\MainBundle\Entity\Paciente $paciente = null)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return \MainBundle\Entity\Paciente 
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set tratamientoMedico
     *
     * @param \MainBundle\Entity\TratamientoMedico $tratamientoMedico
     * @return HistoriaClinica
     */
    public function setTratamientoMedico(\MainBundle\Entity\TratamientoMedico $tratamientoMedico = null)
    {
        $this->tratamientoMedico = $tratamientoMedico;

        return $this;
    }

    /**
     * Get tratamientoMedico
     *
     * @return \MainBundle\Entity\TratamientoMedico 
     */
    public function getTratamientoMedico()
    {
        return $this->tratamientoMedico;
    }

    /**
     * Set hcNacpPartoCesarea
     *
     * @param boolean $hcNacpPartoCesarea
     * @return HistoriaClinica
     */
    public function setHcNacpPartoCesarea($hcNacpPartoCesarea)
    {
        $this->hcNacpPartoCesarea = $hcNacpPartoCesarea;

        return $this;
    }

    /**
     * Get hcNacpPartoCesarea
     *
     * @return boolean 
     */
    public function getHcNacpPartoCesarea()
    {
        return $this->hcNacpPartoCesarea;
    }

    /**
     * Set hcNacpLlantoProvocado
     *
     * @param boolean $hcNacpLlantoProvocado
     * @return HistoriaClinica
     */
    public function setHcNacpLlantoProvocado($hcNacpLlantoProvocado)
    {
        $this->hcNacpLlantoProvocado = $hcNacpLlantoProvocado;

        return $this;
    }

    /**
     * Get hcNacpLlantoProvocado
     *
     * @return boolean 
     */
    public function getHcNacpLlantoProvocado()
    {
        return $this->hcNacpLlantoProvocado;
    }

    /**
     * Set hcEnfGeneticas
     *
     * @param boolean $hcEnfGeneticas
     * @return HistoriaClinica
     */
    public function setHcEnfGeneticas($hcEnfGeneticas)
    {
        $this->hcEnfGeneticas = $hcEnfGeneticas;

        return $this;
    }

    /**
     * Get hcEnfGeneticas
     *
     * @return boolean 
     */
    public function getHcEnfGeneticas()
    {
        return $this->hcEnfGeneticas;
    }

    /**
     * Set hcEnfProbNutricionales
     *
     * @param boolean $hcEnfProbNutricionales
     * @return HistoriaClinica
     */
    public function setHcEnfProbNutricionales($hcEnfProbNutricionales)
    {
        $this->hcEnfProbNutricionales = $hcEnfProbNutricionales;

        return $this;
    }

    /**
     * Get hcEnfProbNutricionales
     *
     * @return boolean 
     */
    public function getHcEnfProbNutricionales()
    {
        return $this->hcEnfProbNutricionales;
    }

    /**
     * Set hcEnfInmunnologica
     *
     * @param boolean $hcEnfInmunnologica
     * @return HistoriaClinica
     */
    public function setHcEnfInmunnologica($hcEnfInmunnologica)
    {
        $this->hcEnfInmunnologica = $hcEnfInmunnologica;

        return $this;
    }

    /**
     * Get hcEnfInmunnologica
     *
     * @return boolean 
     */
    public function getHcEnfInmunnologica()
    {
        return $this->hcEnfInmunnologica;
    }

    /**
     * Set hcEnfTrastNeurologico
     *
     * @param boolean $hcEnfTrastNeurologico
     * @return HistoriaClinica
     */
    public function setHcEnfTrastNeurologico($hcEnfTrastNeurologico)
    {
        $this->hcEnfTrastNeurologico = $hcEnfTrastNeurologico;

        return $this;
    }

    /**
     * Get hcEnfTrastNeurologico
     *
     * @return boolean 
     */
    public function getHcEnfTrastNeurologico()
    {
        return $this->hcEnfTrastNeurologico;
    }

    /**
     * Set hcEnfRetPsicomotor
     *
     * @param boolean $hcEnfRetPsicomotor
     * @return HistoriaClinica
     */
    public function setHcEnfRetPsicomotor($hcEnfRetPsicomotor)
    {
        $this->hcEnfRetPsicomotor = $hcEnfRetPsicomotor;

        return $this;
    }

    /**
     * Get hcEnfRetPsicomotor
     *
     * @return boolean 
     */
    public function getHcEnfRetPsicomotor()
    {
        return $this->hcEnfRetPsicomotor;
    }

    /**
     * Set hcEnfTrastSueno
     *
     * @param boolean $hcEnfTrastSueno
     * @return HistoriaClinica
     */
    public function setHcEnfTrastSueno($hcEnfTrastSueno)
    {
        $this->hcEnfTrastSueno = $hcEnfTrastSueno;

        return $this;
    }

    /**
     * Get hcEnfTrastSueno
     *
     * @return boolean 
     */
    public function getHcEnfTrastSueno()
    {
        return $this->hcEnfTrastSueno;
    }

    /**
     * Set hcEnfRespiratorias
     *
     * @param boolean $hcEnfRespiratorias
     * @return HistoriaClinica
     */
    public function setHcEnfRespiratorias($hcEnfRespiratorias)
    {
        $this->hcEnfRespiratorias = $hcEnfRespiratorias;

        return $this;
    }

    /**
     * Get hcEnfRespiratorias
     *
     * @return boolean 
     */
    public function getHcEnfRespiratorias()
    {
        return $this->hcEnfRespiratorias;
    }

    /**
     * Set hcEnfOtras
     *
     * @param boolean $hcEnfOtras
     * @return HistoriaClinica
     */
    public function setHcEnfOtras($hcEnfOtras)
    {
        $this->hcEnfOtras = $hcEnfOtras;

        return $this;
    }

    /**
     * Get hcEnfOtras
     *
     * @return boolean 
     */
    public function getHcEnfOtras()
    {
        return $this->hcEnfOtras;
    }

    /**
     * Set hcAEasisteEscuela
     *
     * @param boolean $hcAEasisteEscuela
     * @return HistoriaClinica
     */
    public function setHcAEasisteEscuela($hcAEasisteEscuela)
    {
        $this->hcAEasisteEscuela = $hcAEasisteEscuela;

        return $this;
    }

    /**
     * Get hcAEasisteEscuela
     *
     * @return boolean 
     */
    public function getHcAEasisteEscuela()
    {
        return $this->hcAEasisteEscuela;
    }

    /**
     * Set hcAEapoyoCurricular
     *
     * @param boolean $hcAEapoyoCurricular
     * @return HistoriaClinica
     */
    public function setHcAEapoyoCurricular($hcAEapoyoCurricular)
    {
        $this->hcAEapoyoCurricular = $hcAEapoyoCurricular;

        return $this;
    }

    /**
     * Get hcAEapoyoCurricular
     *
     * @return boolean 
     */
    public function getHcAEapoyoCurricular()
    {
        return $this->hcAEapoyoCurricular;
    }

    /**
     * Set hcAEdesfasadoEdad
     *
     * @param boolean $hcAEdesfasadoEdad
     * @return HistoriaClinica
     */
    public function setHcAEdesfasadoEdad($hcAEdesfasadoEdad)
    {
        $this->hcAEdesfasadoEdad = $hcAEdesfasadoEdad;

        return $this;
    }

    /**
     * Get hcAEdesfasadoEdad
     *
     * @return boolean 
     */
    public function getHcAEdesfasadoEdad()
    {
        return $this->hcAEdesfasadoEdad;
    }

    /**
     * Set hcAEapoyoPsicopedagogico
     *
     * @param boolean $hcAEapoyoPsicopedagogico
     * @return HistoriaClinica
     */
    public function setHcAEapoyoPsicopedagogico($hcAEapoyoPsicopedagogico)
    {
        $this->hcAEapoyoPsicopedagogico = $hcAEapoyoPsicopedagogico;

        return $this;
    }

    /**
     * Get hcAEapoyoPsicopedagogico
     *
     * @return boolean 
     */
    public function getHcAEapoyoPsicopedagogico()
    {
        return $this->hcAEapoyoPsicopedagogico;
    }

    /**
     * Set hcAEmaestraSombra
     *
     * @param boolean $hcAEmaestraSombra
     * @return HistoriaClinica
     */
    public function setHcAEmaestraSombra($hcAEmaestraSombra)
    {
        $this->hcAEmaestraSombra = $hcAEmaestraSombra;

        return $this;
    }

    /**
     * Get hcAEmaestraSombra
     *
     * @return boolean 
     */
    public function getHcAEmaestraSombra()
    {
        return $this->hcAEmaestraSombra;
    }

    /**
     * Set hcAEparticipaManeraOrdinariaActividades
     *
     * @param boolean $hcAEparticipaManeraOrdinariaActividades
     * @return HistoriaClinica
     */
    public function setHcAEparticipaManeraOrdinariaActividades($hcAEparticipaManeraOrdinariaActividades)
    {
        $this->hcAEparticipaManeraOrdinariaActividades = $hcAEparticipaManeraOrdinariaActividades;

        return $this;
    }

    /**
     * Get hcAEparticipaManeraOrdinariaActividades
     *
     * @return boolean 
     */
    public function getHcAEparticipaManeraOrdinariaActividades()
    {
        return $this->hcAEparticipaManeraOrdinariaActividades;
    }

    /**
     * Set hcAEingregraGrupo
     *
     * @param boolean $hcAEingregraGrupo
     * @return HistoriaClinica
     */
    public function setHcAEingregraGrupo($hcAEingregraGrupo)
    {
        $this->hcAEingregraGrupo = $hcAEingregraGrupo;

        return $this;
    }

    /**
     * Get hcAEingregraGrupo
     *
     * @return boolean 
     */
    public function getHcAEingregraGrupo()
    {
        return $this->hcAEingregraGrupo;
    }

    /**
     * Set hcAEDescripcionExperiencias
     *
     * @param string $hcAEDescripcionExperiencias
     * @return HistoriaClinica
     */
    public function setHcAEDescripcionExperiencias($hcAEDescripcionExperiencias)
    {
        $this->hcAEDescripcionExperiencias = $hcAEDescripcionExperiencias;

        return $this;
    }

    /**
     * Get hcAEDescripcionExperiencias
     *
     * @return string 
     */
    public function getHcAEDescripcionExperiencias()
    {
        return $this->hcAEDescripcionExperiencias;
    }

    /**
     * Set hcHabAutSueno
     *
     * @param boolean $hcHabAutSueno
     * @return HistoriaClinica
     */
    public function setHcHabAutSueno($hcHabAutSueno)
    {
        $this->hcHabAutSueno = $hcHabAutSueno;

        return $this;
    }

    /**
     * Get hcHabAutSueno
     *
     * @return boolean 
     */
    public function getHcHabAutSueno()
    {
        return $this->hcHabAutSueno;
    }

    /**
     * Set hcHabAutAlimenticio
     *
     * @param boolean $hcHabAutAlimenticio
     * @return HistoriaClinica
     */
    public function setHcHabAutAlimenticio($hcHabAutAlimenticio)
    {
        $this->hcHabAutAlimenticio = $hcHabAutAlimenticio;

        return $this;
    }

    /**
     * Get hcHabAutAlimenticio
     *
     * @return boolean 
     */
    public function getHcHabAutAlimenticio()
    {
        return $this->hcHabAutAlimenticio;
    }

    /**
     * Set hcHabAutHigienicos
     *
     * @param boolean $hcHabAutHigienicos
     * @return HistoriaClinica
     */
    public function setHcHabAutHigienicos($hcHabAutHigienicos)
    {
        $this->hcHabAutHigienicos = $hcHabAutHigienicos;

        return $this;
    }

    /**
     * Get hcHabAutHigienicos
     *
     * @return boolean 
     */
    public function getHcHabAutHigienicos()
    {
        return $this->hcHabAutHigienicos;
    }

    /**
     * Set hcHabAutEsfinteres
     *
     * @param boolean $hcHabAutEsfinteres
     * @return HistoriaClinica
     */
    public function setHcHabAutEsfinteres($hcHabAutEsfinteres)
    {
        $this->hcHabAutEsfinteres = $hcHabAutEsfinteres;

        return $this;
    }

    /**
     * Get hcHabAutEsfinteres
     *
     * @return boolean 
     */
    public function getHcHabAutEsfinteres()
    {
        return $this->hcHabAutEsfinteres;
    }

    /**
     * Set hcHabAutAutocuidado
     *
     * @param boolean $hcHabAutAutocuidado
     * @return HistoriaClinica
     */
    public function setHcHabAutAutocuidado($hcHabAutAutocuidado)
    {
        $this->hcHabAutAutocuidado = $hcHabAutAutocuidado;

        return $this;
    }

    /**
     * Get hcHabAutAutocuidado
     *
     * @return boolean 
     */
    public function getHcHabAutAutocuidado()
    {
        return $this->hcHabAutAutocuidado;
    }

    /**
     * Set hcHabAutVestido
     *
     * @param boolean $hcHabAutVestido
     * @return HistoriaClinica
     */
    public function setHcHabAutVestido($hcHabAutVestido)
    {
        $this->hcHabAutVestido = $hcHabAutVestido;

        return $this;
    }

    /**
     * Get hcHabAutVestido
     *
     * @return boolean 
     */
    public function getHcHabAutVestido()
    {
        return $this->hcHabAutVestido;
    }

    /**
     * Set hcHabAutJuegoRutinas
     *
     * @param boolean $hcHabAutJuegoRutinas
     * @return HistoriaClinica
     */
    public function setHcHabAutJuegoRutinas($hcHabAutJuegoRutinas)
    {
        $this->hcHabAutJuegoRutinas = $hcHabAutJuegoRutinas;

        return $this;
    }

    /**
     * Get hcHabAutJuegoRutinas
     *
     * @return boolean 
     */
    public function getHcHabAutJuegoRutinas()
    {
        return $this->hcHabAutJuegoRutinas;
    }

    /**
     * Set hcHabAutOtros
     *
     * @param boolean $hcHabAutOtros
     * @return HistoriaClinica
     */
    public function setHcHabAutOtros($hcHabAutOtros)
    {
        $this->hcHabAutOtros = $hcHabAutOtros;

        return $this;
    }

    /**
     * Get hcHabAutOtros
     *
     * @return boolean 
     */
    public function getHcHabAutOtros()
    {
        return $this->hcHabAutOtros;
    }

    /**
     * Set hcHabAutDescripcion
     *
     * @param string $hcHabAutDescripcion
     * @return HistoriaClinica
     */
    public function setHcHabAutDescripcion($hcHabAutDescripcion)
    {
        $this->hcHabAutDescripcion = $hcHabAutDescripcion;

        return $this;
    }

    /**
     * Get hcHabAutDescripcion
     *
     * @return string 
     */
    public function getHcHabAutDescripcion()
    {
        return $this->hcHabAutDescripcion;
    }

    /**
     * Set historiaEstadoSalud
     *
     * @param \MainBundle\Entity\HabitosHijo $historiaEstadoSalud
     * @return HistoriaClinica
     */
    public function setHistoriaEstadoSalud(\MainBundle\Entity\HabitosHijo $historiaEstadoSalud = null)
    {
        $this->historiaEstadoSalud = $historiaEstadoSalud;

        return $this;
    }

    /**
     * Set hcHabAutAlimenticioDesc
     *
     * @param boolean $hcHabAutAlimenticioDesc
     * @return HistoriaClinica
     */
    public function setHcHabAutAlimenticioDesc($hcHabAutAlimenticioDesc)
    {
        $this->hcHabAutAlimenticioDesc = $hcHabAutAlimenticioDesc;

        return $this;
    }

    /**
     * Get hcHabAutAlimenticioDesc
     *
     * @return boolean 
     */
    public function getHcHabAutAlimenticioDesc()
    {
        return $this->hcHabAutAlimenticioDesc;
    }

    /**
     * Set hcHabAutHigienicosDesc
     *
     * @param boolean $hcHabAutHigienicosDesc
     * @return HistoriaClinica
     */
    public function setHcHabAutHigienicosDesc($hcHabAutHigienicosDesc)
    {
        $this->hcHabAutHigienicosDesc = $hcHabAutHigienicosDesc;

        return $this;
    }

    /**
     * Get hcHabAutHigienicosDesc
     *
     * @return boolean 
     */
    public function getHcHabAutHigienicosDesc()
    {
        return $this->hcHabAutHigienicosDesc;
    }

    /**
     * Set hcHabAutEsfinteresDesc
     *
     * @param boolean $hcHabAutEsfinteresDesc
     * @return HistoriaClinica
     */
    public function setHcHabAutEsfinteresDesc($hcHabAutEsfinteresDesc)
    {
        $this->hcHabAutEsfinteresDesc = $hcHabAutEsfinteresDesc;

        return $this;
    }

    /**
     * Get hcHabAutEsfinteresDesc
     *
     * @return boolean 
     */
    public function getHcHabAutEsfinteresDesc()
    {
        return $this->hcHabAutEsfinteresDesc;
    }

    /**
     * Set hcHabAutAutocuidadoDesc
     *
     * @param boolean $hcHabAutAutocuidadoDesc
     * @return HistoriaClinica
     */
    public function setHcHabAutAutocuidadoDesc($hcHabAutAutocuidadoDesc)
    {
        $this->hcHabAutAutocuidadoDesc = $hcHabAutAutocuidadoDesc;

        return $this;
    }

    /**
     * Get hcHabAutAutocuidadoDesc
     *
     * @return boolean 
     */
    public function getHcHabAutAutocuidadoDesc()
    {
        return $this->hcHabAutAutocuidadoDesc;
    }

    /**
     * Set hcHabAutVestidoDesc
     *
     * @param boolean $hcHabAutVestidoDesc
     * @return HistoriaClinica
     */
    public function setHcHabAutVestidoDesc($hcHabAutVestidoDesc)
    {
        $this->hcHabAutVestidoDesc = $hcHabAutVestidoDesc;

        return $this;
    }

    /**
     * Get hcHabAutVestidoDesc
     *
     * @return boolean 
     */
    public function getHcHabAutVestidoDesc()
    {
        return $this->hcHabAutVestidoDesc;
    }

    /**
     * Set hcHabAutJuegoRutinasDesc
     *
     * @param boolean $hcHabAutJuegoRutinasDesc
     * @return HistoriaClinica
     */
    public function setHcHabAutJuegoRutinasDesc($hcHabAutJuegoRutinasDesc)
    {
        $this->hcHabAutJuegoRutinasDesc = $hcHabAutJuegoRutinasDesc;

        return $this;
    }

    /**
     * Get hcHabAutJuegoRutinasDesc
     *
     * @return boolean 
     */
    public function getHcHabAutJuegoRutinasDesc()
    {
        return $this->hcHabAutJuegoRutinasDesc;
    }

    /**
     * Set hcHabAutSuenoDescripcion
     *
     * @param string $hcHabAutSuenoDescripcion
     * @return HistoriaClinica
     */
    public function setHcHabAutSuenoDescripcion($hcHabAutSuenoDescripcion)
    {
        $this->hcHabAutSuenoDescripcion = $hcHabAutSuenoDescripcion;

        return $this;
    }

    /**
     * Get hcHabAutSuenoDescripcion
     *
     * @return string 
     */
    public function getHcHabAutSuenoDescripcion()
    {
        return $this->hcHabAutSuenoDescripcion;
    }

    /**
     * Set hcHabAutOtrosDesc
     *
     * @param string $hcHabAutOtrosDesc
     * @return HistoriaClinica
     */
    public function setHcHabAutOtrosDesc($hcHabAutOtrosDesc)
    {
        $this->hcHabAutOtrosDesc = $hcHabAutOtrosDesc;

        return $this;
    }

    /**
     * Get hcHabAutOtrosDesc
     *
     * @return string 
     */
    public function getHcHabAutOtrosDesc()
    {
        return $this->hcHabAutOtrosDesc;
    }

    /**
     * Set hcAdaFarmacosNeurologicos
     *
     * @param boolean $hcAdaFarmacosNeurologicos
     * @return HistoriaClinica
     */
    public function setHcAdaFarmacosNeurologicos($hcAdaFarmacosNeurologicos)
    {
        $this->hcAdaFarmacosNeurologicos = $hcAdaFarmacosNeurologicos;

        return $this;
    }

    /**
     * Get hcAdaFarmacosNeurologicos
     *
     * @return boolean 
     */
    public function getHcAdaFarmacosNeurologicos()
    {
        return $this->hcAdaFarmacosNeurologicos;
    }

    /**
     * Set hcAdaAbordajeNutricional
     *
     * @param boolean $hcAdaAbordajeNutricional
     * @return HistoriaClinica
     */
    public function setHcAdaAbordajeNutricional($hcAdaAbordajeNutricional)
    {
        $this->hcAdaAbordajeNutricional = $hcAdaAbordajeNutricional;

        return $this;
    }

    /**
     * Get hcAdaAbordajeNutricional
     *
     * @return boolean 
     */
    public function getHcAdaAbordajeNutricional()
    {
        return $this->hcAdaAbordajeNutricional;
    }

    /**
     * Set hcAdaApoyoPedagógico
     *
     * @param boolean $hcAdaApoyoPedagógico
     * @return HistoriaClinica
     */
    public function setHcAdaApoyoPedagógico($hcAdaApoyoPedagógico)
    {
        $this->hcAdaApoyoPedagógico = $hcAdaApoyoPedagógico;

        return $this;
    }

    /**
     * Get hcAdaApoyoPedagógico
     *
     * @return boolean 
     */
    public function getHcAdaApoyoPedagógico()
    {
        return $this->hcAdaApoyoPedagógico;
    }

    /**
     * Set hcAdaTerapiaAsistAnimales
     *
     * @param boolean $hcAdaTerapiaAsistAnimales
     * @return HistoriaClinica
     */
    public function setHcAdaTerapiaAsistAnimales($hcAdaTerapiaAsistAnimales)
    {
        $this->hcAdaTerapiaAsistAnimales = $hcAdaTerapiaAsistAnimales;

        return $this;
    }

    /**
     * Get hcAdaTerapiaAsistAnimales
     *
     * @return boolean 
     */
    public function getHcAdaTerapiaAsistAnimales()
    {
        return $this->hcAdaTerapiaAsistAnimales;
    }

    /**
     * Set hcAdaTerapiasSensoriales
     *
     * @param boolean $hcAdaTerapiasSensoriales
     * @return HistoriaClinica
     */
    public function setHcAdaTerapiasSensoriales($hcAdaTerapiasSensoriales)
    {
        $this->hcAdaTerapiasSensoriales = $hcAdaTerapiasSensoriales;

        return $this;
    }

    /**
     * Get hcAdaTerapiasSensoriales
     *
     * @return boolean 
     */
    public function getHcAdaTerapiasSensoriales()
    {
        return $this->hcAdaTerapiasSensoriales;
    }

    /**
     * Set hcAdaEstimulacionTemprana
     *
     * @param boolean $hcAdaEstimulacionTemprana
     * @return HistoriaClinica
     */
    public function setHcAdaEstimulacionTemprana($hcAdaEstimulacionTemprana)
    {
        $this->hcAdaEstimulacionTemprana = $hcAdaEstimulacionTemprana;

        return $this;
    }

    /**
     * Get hcAdaEstimulacionTemprana
     *
     * @return boolean 
     */
    public function getHcAdaEstimulacionTemprana()
    {
        return $this->hcAdaEstimulacionTemprana;
    }

    /**
     * Set hcAdaPsicomotricidad
     *
     * @param boolean $hcAdaPsicomotricidad
     * @return HistoriaClinica
     */
    public function setHcAdaPsicomotricidad($hcAdaPsicomotricidad)
    {
        $this->hcAdaPsicomotricidad = $hcAdaPsicomotricidad;

        return $this;
    }

    /**
     * Get hcAdaPsicomotricidad
     *
     * @return boolean 
     */
    public function getHcAdaPsicomotricidad()
    {
        return $this->hcAdaPsicomotricidad;
    }

    /**
     * Set hcAdaApoyoPsicologico
     *
     * @param boolean $hcAdaApoyoPsicologico
     * @return HistoriaClinica
     */
    public function setHcAdaApoyoPsicologico($hcAdaApoyoPsicologico)
    {
        $this->hcAdaApoyoPsicologico = $hcAdaApoyoPsicologico;

        return $this;
    }

    /**
     * Get hcAdaApoyoPsicologico
     *
     * @return boolean 
     */
    public function getHcAdaApoyoPsicologico()
    {
        return $this->hcAdaApoyoPsicologico;
    }

    /**
     * Set hcAdaApoyoPedagogico
     *
     * @param boolean $hcAdaApoyoPedagogico
     * @return HistoriaClinica
     */
    public function setHcAdaApoyoPedagogico($hcAdaApoyoPedagogico)
    {
        $this->hcAdaApoyoPedagogico = $hcAdaApoyoPedagogico;

        return $this;
    }

    /**
     * Get hcAdaApoyoPedagogico
     *
     * @return boolean 
     */
    public function getHcAdaApoyoPedagogico()
    {
        return $this->hcAdaApoyoPedagogico;
    }

    /**
     * Set EstadoSaludEnfermedades
     *
     * @param \MainBundle\Entity\EstadoSaludEnfermedades $estadoSaludEnfermedades
     * @return HistoriaClinica
     */
    public function setEstadoSaludEnfermedades(\MainBundle\Entity\EstadoSaludEnfermedades $estadoSaludEnfermedades = null)
    {
        $this->EstadoSaludEnfermedades = $estadoSaludEnfermedades;

        return $this;
    }

    /**
     * Get EstadoSaludEnfermedades
     *
     * @return \MainBundle\Entity\EstadoSaludEnfermedades 
     */
    public function getEstadoSaludEnfermedades()
    {
        return $this->EstadoSaludEnfermedades;
    }

    /**
     * Set hcDescripcionFinal
     *
     * @param string $hcDescripcionFinal
     * @return HistoriaClinica
     */
    public function setHcDescripcionFinal($hcDescripcionFinal)
    {
        $this->hcDescripcionFinal = $hcDescripcionFinal;

        return $this;
    }

    /**
     * Get hcDescripcionFinal
     *
     * @return string 
     */
    public function getHcDescripcionFinal()
    {
        return $this->hcDescripcionFinal;
    }

    /**
     * Set descripcionAntecedentesAtencionExamenes
     *
     * @param string $descripcionAntecedentesAtencionExamenes
     * @return HistoriaClinica
     */
    public function setDescripcionAntecedentesAtencionExamenes($descripcionAntecedentesAtencionExamenes)
    {
        $this->descripcionAntecedentesAtencionExamenes = $descripcionAntecedentesAtencionExamenes;

        return $this;
    }

    /**
     * Get descripcionAntecedentesAtencionExamenes
     *
     * @return string 
     */
    public function getDescripcionAntecedentesAtencionExamenes()
    {
        return $this->descripcionAntecedentesAtencionExamenes;
    }

    /**
     * Set descripcionAntecedentesFamiliares
     *
     * @param string $descripcionAntecedentesFamiliares
     * @return HistoriaClinica
     */
    public function setDescripcionAntecedentesFamiliares($descripcionAntecedentesFamiliares)
    {
        $this->descripcionAntecedentesFamiliares = $descripcionAntecedentesFamiliares;

        return $this;
    }

    /**
     * Get descripcionAntecedentesFamiliares
     *
     * @return string 
     */
    public function getDescripcionAntecedentesFamiliares()
    {
        return $this->descripcionAntecedentesFamiliares;
    }

    /**
     * Set profesionalAtiende
     *
     * @param \Reica\UsuarioBundle\Entity\Usuario $profesionalAtiende
     * @return HistoriaClinica
     */
    public function setProfesionalAtiende(\Reica\UsuarioBundle\Entity\Usuario $profesionalAtiende = null)
    {
        $this->profesionalAtiende = $profesionalAtiende;

        return $this;
    }

    /**
     * Get profesionalAtiende
     *
     * @return \Reica\UsuarioBundle\Entity\Usuario 
     */
    public function getProfesionalAtiende()
    {
        return $this->profesionalAtiende;
    }

    /**
     * Set estadoCaso
     *
     * @param boolean $estadoCaso
     * @return HistoriaClinica
     */
    public function setEstadoCaso($estadoCaso)
    {
        $this->estadoCaso = $estadoCaso;

        return $this;
    }

    /**
     * Get estadoCaso
     *
     * @return boolean 
     */
    public function getEstadoCaso()
    {
        return $this->estadoCaso;
    }
}
