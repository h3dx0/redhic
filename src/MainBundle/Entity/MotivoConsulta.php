<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MotivoConsulta
 *
 * @ORM\Table(name="motivo_consulta")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\MotivoConsultaRepository")
 */
class MotivoConsulta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo_asistencia", type="text", nullable=true)
     */
    private $motivoAsistencia;

    /**
     * @var string
     *
     * @ORM\Column(name="expectativa_solicitud", type="text", nullable=true)
     */
    private $expectativaSolicitud;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set motivoAsistencia
     *
     * @param string $motivoAsistencia
     * @return MotivoConsulta
     */
    public function setMotivoAsistencia($motivoAsistencia)
    {
        $this->motivoAsistencia = $motivoAsistencia;

        return $this;
    }

    /**
     * Get motivoAsistencia
     *
     * @return string 
     */
    public function getMotivoAsistencia()
    {
        return $this->motivoAsistencia;
    }

    /**
     * Set expectativaSolicitud
     *
     * @param string $expectativaSolicitud
     * @return MotivoConsulta
     */
    public function setExpectativaSolicitud($expectativaSolicitud)
    {
        $this->expectativaSolicitud = $expectativaSolicitud;

        return $this;
    }

    /**
     * Get expectativaSolicitud
     *
     * @return string 
     */
    public function getExpectativaSolicitud()
    {
        return $this->expectativaSolicitud;
    }
}
