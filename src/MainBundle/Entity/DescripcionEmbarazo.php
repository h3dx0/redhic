<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescripcionEmbarazo
 *
 * @ORM\Table(name="descripcion_embarazo")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\DescripcionEmbarazoRepository")
 */
class DescripcionEmbarazo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="emb_desc_aborto_mama", type="text",  nullable=true)
     */
    private $embDescAbortoMama;

    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set embDescAbortoMama
     *
     * @param string $embDescAbortoMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescAbortoMama($embDescAbortoMama)
    {
        $this->embDescAbortoMama = $embDescAbortoMama;

        return $this;
    }

    /**
     * Get embDescAbortoMama
     *
     * @return string 
     */
    public function getEmbDescAbortoMama()
    {
        return $this->embDescAbortoMama;
    }

    /**
     * Set embDescAbortoPapa
     *
     * @param string $embDescAbortoPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescAbortoPapa($embDescAbortoPapa)
    {
        $this->embDescAbortoPapa = $embDescAbortoPapa;

        return $this;
    }

    /**
     * Get embDescAbortoPapa
     *
     * @return string 
     */
    public function getEmbDescAbortoPapa()
    {
        return $this->embDescAbortoPapa;
    }

    /**
     * Set embDescAcgMama
     *
     * @param string $embDescAcgMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescAcgMama($embDescAcgMama)
    {
        $this->embDescAcgMama = $embDescAcgMama;

        return $this;
    }

    /**
     * Get embDescAcgMama
     *
     * @return string 
     */
    public function getEmbDescAcgMama()
    {
        return $this->embDescAcgMama;
    }

    /**
     * Set embDescAcgPapa
     *
     * @param string $embDescAcgPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescAcgPapa($embDescAcgPapa)
    {
        $this->embDescAcgPapa = $embDescAcgPapa;

        return $this;
    }

    /**
     * Get embDescAcgPapa
     *
     * @return string 
     */
    public function getEmbDescAcgPapa()
    {
        return $this->embDescAcgPapa;
    }

    /**
     * Set embDescEdembMama
     *
     * @param string $embDescEdembMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescEdembMama($embDescEdembMama)
    {
        $this->embDescEdembMama = $embDescEdembMama;

        return $this;
    }

    /**
     * Get embDescEdembMama
     *
     * @return string 
     */
    public function getEmbDescEdembMama()
    {
        return $this->embDescEdembMama;
    }

    /**
     * Set embDescEdembPapa
     *
     * @param string $embDescEdembPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescEdembPapa($embDescEdembPapa)
    {
        $this->embDescEdembPapa = $embDescEdembPapa;

        return $this;
    }

    /**
     * Get embDescEdembPapa
     *
     * @return string 
     */
    public function getEmbDescEdembPapa()
    {
        return $this->embDescEdembPapa;
    }

    /**
     * Set embDescHtoxMama
     *
     * @param string $embDescHtoxMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescHtoxMama($embDescHtoxMama)
    {
        $this->embDescHtoxMama = $embDescHtoxMama;

        return $this;
    }

    /**
     * Get embDescHtoxMama
     *
     * @return string 
     */
    public function getEmbDescHtoxMama()
    {
        return $this->embDescHtoxMama;
    }

    /**
     * Set embDescHtoxPapa
     *
     * @param string $embDescHtoxPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescHtoxPapa($embDescHtoxPapa)
    {
        $this->embDescHtoxPapa = $embDescHtoxPapa;

        return $this;
    }

    /**
     * Get embDescHtoxPapa
     *
     * @return string 
     */
    public function getEmbDescHtoxPapa()
    {
        return $this->embDescHtoxPapa;
    }

    /**
     * Set embDescEadMama
     *
     * @param string $embDescEadMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescEadMama($embDescEadMama)
    {
        $this->embDescEadMama = $embDescEadMama;

        return $this;
    }

    /**
     * Get embDescEadMama
     *
     * @return string 
     */
    public function getEmbDescEadMama()
    {
        return $this->embDescEadMama;
    }

    /**
     * Set embDescEadPapa
     *
     * @param string $embDescEadPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescEadPapa($embDescEadPapa)
    {
        $this->embDescEadPapa = $embDescEadPapa;

        return $this;
    }

    /**
     * Get embDescEadPapa
     *
     * @return string 
     */
    public function getEmbDescEadPapa()
    {
        return $this->embDescEadPapa;
    }

    /**
     * Set embDescIaalimMama
     *
     * @param string $embDescIaalimMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescIaalimMama($embDescIaalimMama)
    {
        $this->embDescIaalimMama = $embDescIaalimMama;

        return $this;
    }

    /**
     * Get embDescIaalimMama
     *
     * @return string 
     */
    public function getEmbDescIaalimMama()
    {
        return $this->embDescIaalimMama;
    }

    /**
     * Set embDescIaalimPapa
     *
     * @param string $embDescIaalimPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescIaalimPapa($embDescIaalimPapa)
    {
        $this->embDescIaalimPapa = $embDescIaalimPapa;

        return $this;
    }

    /**
     * Get embDescIaalimPapa
     *
     * @return string 
     */
    public function getEmbDescIaalimPapa()
    {
        return $this->embDescIaalimPapa;
    }

    /**
     * Set embDescMedicamentosMama
     *
     * @param string $embDescMedicamentosMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescMedicamentosMama($embDescMedicamentosMama)
    {
        $this->embDescMedicamentosMama = $embDescMedicamentosMama;

        return $this;
    }

    /**
     * Get embDescMedicamentosMama
     *
     * @return string 
     */
    public function getEmbDescMedicamentosMama()
    {
        return $this->embDescMedicamentosMama;
    }

    /**
     * Set embDescMedicamentosPapa
     *
     * @param string $embDescMedicamentosPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescMedicamentosPapa($embDescMedicamentosPapa)
    {
        $this->embDescMedicamentosPapa = $embDescMedicamentosPapa;

        return $this;
    }

    /**
     * Get embDescMedicamentosPapa
     *
     * @return string 
     */
    public function getEmbDescMedicamentosPapa()
    {
        return $this->embDescMedicamentosPapa;
    }

    /**
     * Set embDescPesoOkMama
     *
     * @param string $embDescPesoOkMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescPesoOkMama($embDescPesoOkMama)
    {
        $this->embDescPesoOkMama = $embDescPesoOkMama;

        return $this;
    }

    /**
     * Get embDescPesoOkMama
     *
     * @return string 
     */
    public function getEmbDescPesoOkMama()
    {
        return $this->embDescPesoOkMama;
    }

    /**
     * Set embDescPesoOkPapa
     *
     * @param string $embDescPesoOkPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescPesoOkPapa($embDescPesoOkPapa)
    {
        $this->embDescPesoOkPapa = $embDescPesoOkPapa;

        return $this;
    }

    /**
     * Get embDescPesoOkPapa
     *
     * @return string 
     */
    public function getEmbDescPesoOkPapa()
    {
        return $this->embDescPesoOkPapa;
    }

    /**
     * Set embDescExperiodMama
     *
     * @param string $embDescExperiodMama
     * @return DescripcionEmbarazo
     */
    public function setEmbDescExperiodMama($embDescExperiodMama)
    {
        $this->embDescExperiodMama = $embDescExperiodMama;

        return $this;
    }

    /**
     * Get embDescExperiodMama
     *
     * @return string 
     */
    public function getEmbDescExperiodMama()
    {
        return $this->embDescExperiodMama;
    }

    /**
     * Set embDescExperiodPapa
     *
     * @param string $embDescExperiodPapa
     * @return DescripcionEmbarazo
     */
    public function setEmbDescExperiodPapa($embDescExperiodPapa)
    {
        $this->embDescExperiodPapa = $embDescExperiodPapa;

        return $this;
    }

    /**
     * Get embDescExperiodPapa
     *
     * @return string 
     */
    public function getEmbDescExperiodPapa()
    {
        return $this->embDescExperiodPapa;
    }
}
