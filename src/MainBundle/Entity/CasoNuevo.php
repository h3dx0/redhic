<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CasoNuevo
 *
 * @ORM\Table(name="caso_nuevo")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\CasoNuevoRepository")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\CasoNuevoRepository")
 */
class CasoNuevo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoPaterno", type="string", length=255)
     */
    private $apellidoPaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoMaterno", type="string", length=255)
     */
    private $apellidoMaterno;

    /**
     * @var int
     * Filtrable
     * @ORM\Column(name="edad", type="integer")
     */
    private $edad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaNacimiento", type="date")
     */
    private $fechaNacimiento;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaRegistroCaso", type="date")
     */
    private $fechaRegistroCaso;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreMama", type="string", length=255, nullable=true)
     */
    private $nombreMama;
    /**
     * @var string
     *
     * @ORM\Column(name="telefonoMama", type="string", length=255, nullable=true)
     */
    private $telefonoMama;
    /**
     * @var string
     *
     * @ORM\Column(name="emailMama", type="string", length=255, nullable=true)
     */
    private $emailMama;
    /**
     * @var string
     *
     * @ORM\Column(name="nombrePapa", type="string", length=255, nullable=true)
     */
    private $nombrePapa;
    /**
     * @var string
     *
     * @ORM\Column(name="telefonoPapa", type="string", length=255, nullable=true)
     */
    private $telefonoPapa;
    /**
     * @var string
     *
     * @ORM\Column(name="emailPapa", type="string", length=255, nullable=true)
     */
    private $emailPapa;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreEspecialistaRemision", type="string", length=255, nullable=true)
     */
    private $nombreEspecialistaRemision;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoEspecialistaRemision", type="string", length=255, nullable=true)
     */
    private $tipoEspecialistaRemision;

    /**
     * @var string
     * filtrable
     * @ORM\Column(name="procedencia", type="string", length=255, nullable=true)
     */
    private $procedencia;

    /**
     * @var string
     * filtrable
     * @ORM\Column(name="sexo", type="string", length=255)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="motivoConsulta", type="text", nullable=true)
     */
    private $motivoConsulta;

    /**
     * @ORM\OneToOne(targetEntity="AntecedentesEvaluativos", mappedBy="antecedentesEvaluativos",cascade={"persist"})
     * @ORM\JoinColumn(name="idAntecedentesEvaluativos", referencedColumnName="id")
     */
    private $antecedentesEvaluativos;

    /**
     * @ORM\OneToOne(targetEntity="Antecedentes", mappedBy="antecedentes",cascade={"persist"})
     * @ORM\JoinColumn(name="idAntecedentes", referencedColumnName="id")
     */
    private $antecedentes;

    /**
     * @ORM\OneToOne(targetEntity="ObservacionesInicialesDesarrollo", mappedBy="observacionesInicialesDesarrollo",cascade={"persist"})
     * @ORM\JoinColumn(name="idObservacionesInicialesDesarrollo", referencedColumnName="id")
     */
    private $observacionesInicialesDesarrollo;

    /**
    * @var string
    *
    * @ORM\Column(name="propuestaAtencion", type="text", nullable=true)
    */
    private $propuestaAtencion;
    /**
     * @ORM\OneToOne(targetEntity="ProtocoloEvaluacion", mappedBy="protocoloEvaluacion",cascade={"persist"})
     * @ORM\JoinColumn(name="idProtocoloEvaluacion", referencedColumnName="id")
     */
    private $protocoloEvaluacion;

    /**
     * @var string
     *
     * @ORM\Column(name="derivacionesInterconsultas", type="text", nullable=true)
     */
    private $derivacionesInterconsultas;
    /**
     * @var string
     *
     * @ORM\Column(name="especificacionesEvaluativas", type="text", nullable=true)
     */
    private $especificacionesEvaluativas;

    /**
     * @var string
     *
     * @ORM\Column(name="comentariosFinales", type="text", nullable=true)
     */
    private $comentariosFinales;

    /**
     * Filtrable
     * @ORM\ManyToOne(targetEntity="Reica\UsuarioBundle\Entity\Usuario")
     */
    private $especialista;

    /**
     * @ORM\OneToOne(targetEntity="HipotesisDiagnostica", mappedBy="hipotesisDiagnostica",cascade={"persist"})
     * @ORM\JoinColumn(name="idHipotesisDiagnostica", referencedColumnName="id")
     */
    private $hipotesisDiagnostica;

    /**
     * @ORM\OneToOne(targetEntity="EscenarioAntecionSugeridos",cascade={"persist"})
     * @ORM\JoinColumn(name="idEscenarioAtencionSugeridos", referencedColumnName="id")
     */

    private $escenarioAtencionSugeridos;
    /**
     * @ORM\OneToOne(targetEntity="ServicioAtencionSugerido", mappedBy="servicioAtencionSugerido",cascade={"persist"})
     * @ORM\JoinColumn(name="idServicioAtencionSugerido", referencedColumnName="id")
     */
    private $servicioAtencionSugerido;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CasoNuevo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidoPaterno
     *
     * @param string $apellidoPaterno
     * @return CasoNuevo
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get apellidoPaterno
     *
     * @return string
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set apellidoMaterno
     *
     * @param string $apellidoMaterno
     * @return CasoNuevo
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get apellidoMaterno
     *
     * @return string
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     * @return CasoNuevo
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return integer
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return CasoNuevo
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set nombreEspecialistaRemision
     *
     * @param string $nombreEspecialistaRemision
     * @return CasoNuevo
     */
    public function setNombreEspecialistaRemision($nombreEspecialistaRemision)
    {
        $this->nombreEspecialistaRemision = $nombreEspecialistaRemision;

        return $this;
    }

    /**
     * Get nombreEspecialistaRemision
     *
     * @return string
     */
    public function getNombreEspecialistaRemision()
    {
        return $this->nombreEspecialistaRemision;
    }

    /**
     * Set tipoEspecialistaRemision
     *
     * @param string $tipoEspecialistaRemision
     * @return CasoNuevo
     */
    public function setTipoEspecialistaRemision($tipoEspecialistaRemision)
    {
        $this->tipoEspecialistaRemision = $tipoEspecialistaRemision;

        return $this;
    }

    /**
     * Get tipoEspecialistaRemision
     *
     * @return string
     */
    public function getTipoEspecialistaRemision()
    {
        return $this->tipoEspecialistaRemision;
    }

    /**
     * Set procedencia
     *
     * @param string $procedencia
     * @return CasoNuevo
     */
    public function setProcedencia($procedencia)
    {
        $this->procedencia = $procedencia;

        return $this;
    }

    /**
     * Get procedencia
     *
     * @return string
     */
    public function getProcedencia()
    {
        return $this->procedencia;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     * @return CasoNuevo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set motivoConsulta
     *
     * @param string $motivoConsulta
     * @return CasoNuevo
     */
    public function setMotivoConsulta($motivoConsulta)
    {
        $this->motivoConsulta = $motivoConsulta;

        return $this;
    }

    /**
     * Get motivoConsulta
     *
     * @return string
     */
    public function getMotivoConsulta()
    {
        return $this->motivoConsulta;
    }

    /**
     * Set comentariosFinales
     *
     * @param string $comentariosFinales
     * @return CasoNuevo
     */
    public function setComentariosFinales($comentariosFinales)
    {
        $this->comentariosFinales = $comentariosFinales;

        return $this;
    }

    /**
     * Get comentariosFinales
     *
     * @return string
     */
    public function getComentariosFinales()
    {
        return $this->comentariosFinales;
    }

    /**
     * Set especialista
     *
     * @param string $especialista
     * @return CasoNuevo
     */
    public function setEspecialista($especialista)
    {
        $this->especialista = $especialista;

        return $this;
    }

    /**
     * Get especialista
     *
     * @return string
     */
    public function getEspecialista()
    {
        return $this->especialista;
    }

    /**
     * Set derivacionesInterconsultas
     *
     * @param string $derivacionesInterconsultas
     * @return CasoNuevo
     */
    public function setDerivacionesInterconsultas($derivacionesInterconsultas)
    {
        $this->derivacionesInterconsultas = $derivacionesInterconsultas;

        return $this;
    }

    /**
     * Get derivacionesInterconsultas
     *
     * @return string
     */
    public function getDerivacionesInterconsultas()
    {
        return $this->derivacionesInterconsultas;
    }

    /**
     * Set especificacionesEvaluativas
     *
     * @param string $especificacionesEvaluativas
     * @return CasoNuevo
     */
    public function setEspecificacionesEvaluativas($especificacionesEvaluativas)
    {
        $this->especificacionesEvaluativas = $especificacionesEvaluativas;

        return $this;
    }

    /**
     * Get especificacionesEvaluativas
     *
     * @return string
     */
    public function getEspecificacionesEvaluativas()
    {
        return $this->especificacionesEvaluativas;
    }

    /**
     * Set antecedentesEvaluativos
     *
     * @param \MainBundle\Entity\AntecedentesEvaluativos $antecedentesEvaluativos
     * @return CasoNuevo
     */
    public function setAntecedentesEvaluativos(\MainBundle\Entity\AntecedentesEvaluativos $antecedentesEvaluativos = null)
    {
        $this->antecedentesEvaluativos = $antecedentesEvaluativos;

        return $this;
    }

    /**
     * Get antecedentesEvaluativos
     *
     * @return \MainBundle\Entity\AntecedentesEvaluativos
     */
    public function getAntecedentesEvaluativos()
    {
        return $this->antecedentesEvaluativos;
    }

    /**
     * Set antecedentes
     *
     * @param \MainBundle\Entity\AntecedentesAntecedentes $antecedentes
     * @return CasoNuevo
     */
    public function setAntecedentes(\MainBundle\Entity\Antecedentes $antecedentes = null)
    {
        $this->antecedentes = $antecedentes;

        return $this;
    }

    /**
     * Get antecedentes
     *
     * @return \MainBundle\Entity\AntecedentesAntecedentes
     */
    public function getAntecedentes()
    {
        return $this->antecedentes;
    }

    /**
     * Set observacionesInicialesDesarrollo
     *
     * @param \MainBundle\Entity\ObservacionesInicialesDesarrollo $observacionesInicialesDesarrollo
     * @return CasoNuevo
     */
    public function setObservacionesInicialesDesarrollo(\MainBundle\Entity\ObservacionesInicialesDesarrollo $observacionesInicialesDesarrollo = null)
    {
        $this->observacionesInicialesDesarrollo = $observacionesInicialesDesarrollo;

        return $this;
    }

    /**
     * Get observacionesInicialesDesarrollo
     *
     * @return \MainBundle\Entity\ObservacionesInicialesDesarrollo
     */
    public function getObservacionesInicialesDesarrollo()
    {
        return $this->observacionesInicialesDesarrollo;
    }

    /**
     * Set protocoloEvaluacion
     *
     * @param \MainBundle\Entity\protocoloEvaluacion $protocoloEvaluacion
     * @return CasoNuevo
     */
    public function setProtocoloEvaluacion(\MainBundle\Entity\protocoloEvaluacion $protocoloEvaluacion = null)
    {
        $this->protocoloEvaluacion = $protocoloEvaluacion;

        return $this;
    }

    /**
     * Get protocoloEvaluacion
     *
     * @return \MainBundle\Entity\protocoloEvaluacion
     */
    public function getProtocoloEvaluacion()
    {
        return $this->protocoloEvaluacion;
    }

    /**
     * Set nombreMama
     *
     * @param string $nombreMama
     * @return CasoNuevo
     */
    public function setNombreMama($nombreMama)
    {
        $this->nombreMama = $nombreMama;

        return $this;
    }

    /**
     * Get nombreMama
     *
     * @return string
     */
    public function getNombreMama()
    {
        return $this->nombreMama;
    }

    /**
     * Set telefonoMama
     *
     * @param string $telefonoMama
     * @return CasoNuevo
     */
    public function setTelefonoMama($telefonoMama)
    {
        $this->telefonoMama = $telefonoMama;

        return $this;
    }

    /**
     * Get telefonoMama
     *
     * @return string
     */
    public function getTelefonoMama()
    {
        return $this->telefonoMama;
    }

    /**
     * Set emailMama
     *
     * @param string $emailMama
     * @return CasoNuevo
     */
    public function setEmailMama($emailMama)
    {
        $this->emailMama = $emailMama;

        return $this;
    }

    /**
     * Get emailMama
     *
     * @return string
     */
    public function getEmailMama()
    {
        return $this->emailMama;
    }

    /**
     * Set nombrePapa
     *
     * @param string $nombrePapa
     * @return CasoNuevo
     */
    public function setNombrePapa($nombrePapa)
    {
        $this->nombrePapa = $nombrePapa;

        return $this;
    }

    /**
     * Get nombrePapa
     *
     * @return string
     */
    public function getNombrePapa()
    {
        return $this->nombrePapa;
    }

    /**
     * Set telefonoPapa
     *
     * @param string $telefonoPapa
     * @return CasoNuevo
     */
    public function setTelefonoPapa($telefonoPapa)
    {
        $this->telefonoPapa = $telefonoPapa;

        return $this;
    }

    /**
     * Get telefonoPapa
     *
     * @return string
     */
    public function getTelefonoPapa()
    {
        return $this->telefonoPapa;
    }

    /**
     * Set emailPapa
     *
     * @param string $emailPapa
     * @return CasoNuevo
     */
    public function setEmailPapa($emailPapa)
    {
        $this->emailPapa = $emailPapa;

        return $this;
    }

    /**
     * Get emailPapa
     *
     * @return string
     */
    public function getEmailPapa()
    {
        return $this->emailPapa;
    }

    /**
     * Set propuestaAtencion
     *
     * @param string $propuestaAtencion
     * @return CasoNuevo
     */
    public function setPropuestaAtencion($propuestaAtencion)
    {
        $this->propuestaAtencion = $propuestaAtencion;

        return $this;
    }

    /**
     * Get propuestaAtencion
     *
     * @return string
     */
    public function getPropuestaAtencion()
    {
        return $this->propuestaAtencion;
    }

    /**
     * Set fechaRegistroCaso
     *
     * @param \DateTime $fechaRegistroCaso
     * @return CasoNuevo
     */
    public function setFechaRegistroCaso($fechaRegistroCaso)
    {
        $this->fechaRegistroCaso = $fechaRegistroCaso;

        return $this;
    }

    /**
     * Get fechaRegistroCaso
     *
     * @return \DateTime
     */
    public function getFechaRegistroCaso()
    {
        return $this->fechaRegistroCaso;
    }

    /**
     * Set hipotesisDiagnostica
     *
     * @param \MainBundle\Entity\HipotesisDiagnostica $hipotesisDiagnostica
     * @return CasoNuevo
     */
    public function setHipotesisDiagnostica(\MainBundle\Entity\HipotesisDiagnostica $hipotesisDiagnostica = null)
    {
        $this->hipotesisDiagnostica = $hipotesisDiagnostica;

        return $this;
    }

    /**
     * Get hipotesisDiagnostica
     *
     * @return \MainBundle\Entity\HipotesisDiagnostica 
     */
    public function getHipotesisDiagnostica()
    {
        return $this->hipotesisDiagnostica;
    }


    /**
     * Set servicioAtencionSugerido
     *
     * @param \MainBundle\Entity\ServicioAtencionSugerido $servicioAtencionSugerido
     * @return CasoNuevo
     */
    public function setServicioAtencionSugerido(\MainBundle\Entity\ServicioAtencionSugerido $servicioAtencionSugerido = null)
    {
        $this->servicioAtencionSugerido = $servicioAtencionSugerido;

        return $this;
    }

    /**
     * Get servicioAtencionSugerido
     *
     * @return \MainBundle\Entity\ServicioAtencionSugerido 
     */
    public function getServicioAtencionSugerido()
    {
        return $this->servicioAtencionSugerido;
    }

    /**
     * Set escenarioAtencionSugeridos
     *
     * @param \MainBundle\Entity\EscenarioAntecionSugeridos $escenarioAtencionSugeridos
     * @return CasoNuevo
     */
    public function setEscenarioAtencionSugeridos(\MainBundle\Entity\EscenarioAntecionSugeridos $escenarioAtencionSugeridos = null)
    {
        $this->escenarioAtencionSugeridos = $escenarioAtencionSugeridos;

        return $this;
    }

    /**
     * Get escenarioAtencionSugeridos
     *
     * @return \MainBundle\Entity\EscenarioAntecionSugeridos 
     */
    public function getEscenarioAtencionSugeridos()
    {
        return $this->escenarioAtencionSugeridos;
    }
}
