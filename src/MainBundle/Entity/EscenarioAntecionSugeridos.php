<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EscenarioAntecionSugeridos
 *
 * @ORM\Table(name="escenario_antecion_sugeridos")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\EscenarioAntecionSugeridosRepository")
 */
class EscenarioAntecionSugeridos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="consultaTerap", type="boolean", nullable=true)
     */
    private $consultaTerap;

    /**
     * @var bool
     *
     * @ORM\Column(name="sistemaAmbulatorio", type="boolean", nullable=true)
     */
    private $sistemaAmbulatorio;

    /**
     * @var bool
     *
     * @ORM\Column(name="ambienteEducativo", type="boolean", nullable=true)
     */
    private $ambienteEducativo;

    /**
     * @var bool
     *
     * @ORM\Column(name="ambitoSocial", type="boolean", nullable=true)
     */
    private $ambitoSocial;

    /**
     * @var bool
     *
     * @ORM\Column(name="otro", type="boolean", nullable=true)
     */
    private $otro;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consultaTerap
     *
     * @param boolean $consultaTerap
     * @return EscenarioAntecionSugeridos
     */
    public function setConsultaTerap($consultaTerap)
    {
        $this->consultaTerap = $consultaTerap;

        return $this;
    }

    /**
     * Get consultaTerap
     *
     * @return boolean 
     */
    public function getConsultaTerap()
    {
        return $this->consultaTerap;
    }

    /**
     * Set sistemaAmbulatorio
     *
     * @param boolean $sistemaAmbulatorio
     * @return EscenarioAntecionSugeridos
     */
    public function setSistemaAmbulatorio($sistemaAmbulatorio)
    {
        $this->sistemaAmbulatorio = $sistemaAmbulatorio;

        return $this;
    }

    /**
     * Get sistemaAmbulatorio
     *
     * @return boolean 
     */
    public function getSistemaAmbulatorio()
    {
        return $this->sistemaAmbulatorio;
    }

    /**
     * Set ambienteEducativo
     *
     * @param boolean $ambienteEducativo
     * @return EscenarioAntecionSugeridos
     */
    public function setAmbienteEducativo($ambienteEducativo)
    {
        $this->ambienteEducativo = $ambienteEducativo;

        return $this;
    }

    /**
     * Get ambienteEducativo
     *
     * @return boolean 
     */
    public function getAmbienteEducativo()
    {
        return $this->ambienteEducativo;
    }

    /**
     * Set ambitoSocial
     *
     * @param boolean $ambitoSocial
     * @return EscenarioAntecionSugeridos
     */
    public function setAmbitoSocial($ambitoSocial)
    {
        $this->ambitoSocial = $ambitoSocial;

        return $this;
    }

    /**
     * Get ambitoSocial
     *
     * @return boolean 
     */
    public function getAmbitoSocial()
    {
        return $this->ambitoSocial;
    }

    /**
     * Set otro
     *
     * @param boolean $otro
     * @return EscenarioAntecionSugeridos
     */
    public function setOtro($otro)
    {
        $this->otro = $otro;

        return $this;
    }

    /**
     * Get otro
     *
     * @return boolean 
     */
    public function getOtro()
    {
        return $this->otro;
    }
}
