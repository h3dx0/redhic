<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatosFamiliaresMadre
 *
 * @ORM\Table(name="datos_familiares_madre")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\DatosFamiliaresMadreRepository")
 */
class DatosFamiliaresMadre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255,nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apep", type="string", length=255, nullable=true)
     */
    private $apep;

    /**
    * @ORM\Column(name="edad", type="integer",nullable=true)
    */
    private $edad;
    /**
     * @var string
     *
     * @ORM\Column(name="apem", type="string", length=255, nullable=true)
     */
    private $apem;

    /**
     * @var int
     *
     * @ORM\Column(name="telefono_cel",type="string", length=255, nullable=true)
     */
    private $telefonoCel;

    /**
     * @var int
     *
     * @ORM\Column(name="telefono_dom", type="string", length=255, nullable=true)
     */
    private $telefonoDom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return DatosFamiliaresMadre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apep
     *
     * @param string $apep
     * @return DatosFamiliaresMadre
     */
    public function setApep($apep)
    {
        $this->apep = $apep;

        return $this;
    }

    /**
     * Get apep
     *
     * @return string 
     */
    public function getApep()
    {
        return $this->apep;
    }

    /**
     * Set apem
     *
     * @param string $apem
     * @return DatosFamiliaresMadre
     */
    public function setApem($apem)
    {
        $this->apem = $apem;

        return $this;
    }

    /**
     * Get apem
     *
     * @return string 
     */
    public function getApem()
    {
        return $this->apem;
    }

    /**
     * Set telefonoCel
     *
     * @param integer $telefonoCel
     * @return DatosFamiliaresMadre
     */
    public function setTelefonoCel($telefonoCel)
    {
        $this->telefonoCel = $telefonoCel;

        return $this;
    }

    /**
     * Get telefonoCel
     *
     * @return integer 
     */
    public function getTelefonoCel()
    {
        return $this->telefonoCel;
    }

    /**
     * Set telefonoDom
     *
     * @param integer $telefonoDom
     * @return DatosFamiliaresMadre
     */
    public function setTelefonoDom($telefonoDom)
    {
        $this->telefonoDom = $telefonoDom;

        return $this;
    }

    /**
     * Get telefonoDom
     *
     * @return integer 
     */
    public function getTelefonoDom()
    {
        return $this->telefonoDom;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return DatosFamiliaresMadre
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     * @return DatosFamiliaresMadre
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return integer 
     */
    public function getEdad()
    {
        return $this->edad;
    }
}
