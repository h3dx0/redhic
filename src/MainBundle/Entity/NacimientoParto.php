<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NacimientoParto
 *
 * @ORM\Table(name="nacimiento_parto")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\NacimientoPartoRepository")
 */
class NacimientoParto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nac_desc_sufrimiento_fetal", type="text",  nullable=true)
     */
    private $nacDescSufrimientoFetal;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nacDescSufrimientoFetal
     *
     * @param string $nacDescSufrimientoFetal
     * @return NacimientoParto
     */
    public function setNacDescSufrimientoFetal($nacDescSufrimientoFetal)
    {
        $this->nacDescSufrimientoFetal = $nacDescSufrimientoFetal;

        return $this;
    }

    /**
     * Get nacDescSufrimientoFetal
     *
     * @return string 
     */
    public function getNacDescSufrimientoFetal()
    {
        return $this->nacDescSufrimientoFetal;
    }

    
}
