<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MainBundle\Entity\NParentesco;

/**
 * AntecedentesFamiliaresOtro
 *
 * @ORM\Table(name="antecedentes_familiares_otro")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\AntecedentesFamiliaresOtroRepository")
 */
class AntecedentesFamiliaresOtro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="af_tea", type="boolean", nullable=true)
     */
    private $afTea;

    /**
     * @var bool
     *
     * @ORM\Column(name="af_tdl", type="boolean", nullable=true)
     */
    private $afTdl;

    /**
     * @var bool
     *
     * @ORM\Column(name="af_dintl", type="boolean", nullable=true)
     */
    private $afDintl;

    /**
     * @var bool
     *
     * @ORM\Column(name="af_enfpsq", type="boolean", nullable=true)
     */
    private $afEnfpsq;

    /**
     * @var bool
     *
     * @ORM\Column(name="af_taprend", type="boolean", nullable=true)
     */
    private $afTaprend;

    /**
     * @var bool
     *
     * @ORM\Column(name="af_tpdda", type="boolean", nullable=true)
     */
    private $afTpdda;

    /**
     * @var bool
     *
     * @ORM\Column(name="af_tcc", type="boolean", nullable=true)
     */
    private $afTcc;


    /**
     * @ORM\OneToOne(targetEntity="NParentesco", mappedBy="parentesco")
     */
    private $parentesco;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set afTea
     *
     * @param boolean $afTea
     * @return AntecedentesFamiliaresOtro
     */
    public function setAfTea($afTea)
    {
        $this->afTea = $afTea;

        return $this;
    }

    /**
     * Get afTea
     *
     * @return boolean 
     */
    public function getAfTea()
    {
        return $this->afTea;
    }

    /**
     * Set afTdl
     *
     * @param boolean $afTdl
     * @return AntecedentesFamiliaresOtro
     */
    public function setAfTdl($afTdl)
    {
        $this->afTdl = $afTdl;

        return $this;
    }

    /**
     * Get afTdl
     *
     * @return boolean 
     */
    public function getAfTdl()
    {
        return $this->afTdl;
    }

    /**
     * Set afDintl
     *
     * @param boolean $afDintl
     * @return AntecedentesFamiliaresOtro
     */
    public function setAfDintl($afDintl)
    {
        $this->afDintl = $afDintl;

        return $this;
    }

    /**
     * Get afDintl
     *
     * @return boolean 
     */
    public function getAfDintl()
    {
        return $this->afDintl;
    }

    /**
     * Set afEnfpsq
     *
     * @param boolean $afEnfpsq
     * @return AntecedentesFamiliaresOtro
     */
    public function setAfEnfpsq($afEnfpsq)
    {
        $this->afEnfpsq = $afEnfpsq;

        return $this;
    }

    /**
     * Get afEnfpsq
     *
     * @return boolean 
     */
    public function getAfEnfpsq()
    {
        return $this->afEnfpsq;
    }

    /**
     * Set afTaprend
     *
     * @param boolean $afTaprend
     * @return AntecedentesFamiliaresOtro
     */
    public function setAfTaprend($afTaprend)
    {
        $this->afTaprend = $afTaprend;

        return $this;
    }

    /**
     * Get afTaprend
     *
     * @return boolean 
     */
    public function getAfTaprend()
    {
        return $this->afTaprend;
    }

    /**
     * Set afTpdda
     *
     * @param boolean $afTpdda
     * @return AntecedentesFamiliaresOtro
     */
    public function setAfTpdda($afTpdda)
    {
        $this->afTpdda = $afTpdda;

        return $this;
    }

    /**
     * Get afTpdda
     *
     * @return boolean 
     */
    public function getAfTpdda()
    {
        return $this->afTpdda;
    }

    /**
     * Set afTcc
     *
     * @param boolean $afTcc
     * @return AntecedentesFamiliaresOtro
     */
    public function setAfTcc($afTcc)
    {
        $this->afTcc = $afTcc;

        return $this;
    }

    /**
     * Get afTcc
     *
     * @return boolean 
     */
    public function getAfTcc()
    {
        return $this->afTcc;
    }

    /**
     * Set parentesco
     *
     * @param \MainBundle\Entity\NParentesco $parentesco
     * @return AntecedentesFamiliaresOtro
     */
    public function setParentesco(\MainBundle\Entity\NParentesco $parentesco = null)
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    /**
     * Get parentesco
     *
     * @return \MainBundle\Entity\NParentesco 
     */
    public function getParentesco()
    {
        return $this->parentesco;
    }
}
