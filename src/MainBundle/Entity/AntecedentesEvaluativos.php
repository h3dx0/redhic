<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AntecedentesEvaluativos
 *
 * @ORM\Table(name="antecedentes_evaluativos")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\AntecedentesEvaluativosRepository")
 */
class AntecedentesEvaluativos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="neurologicos", type="text", nullable=true)
     */
    private $neurologicos;

    /**
     * @var string
     *
     * @ORM\Column(name="psicologicos", type="text", nullable=true)
     */
    private $psicologicos;

    /**
     * @var string
     *
     * @ORM\Column(name="otros", type="text", nullable=true)
     */
    private $otros;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set neurologicos
     *
     * @param string $neurologicos
     * @return AntecedentesEvaluativos
     */
    public function setNeurologicos($neurologicos)
    {
        $this->neurologicos = $neurologicos;

        return $this;
    }

    /**
     * Get neurologicos
     *
     * @return string 
     */
    public function getNeurologicos()
    {
        return $this->neurologicos;
    }

    /**
     * Set psicologicos
     *
     * @param string $psicologicos
     * @return AntecedentesEvaluativos
     */
    public function setPsicologicos($psicologicos)
    {
        $this->psicologicos = $psicologicos;

        return $this;
    }

    /**
     * Get psicologicos
     *
     * @return string 
     */
    public function getPsicologicos()
    {
        return $this->psicologicos;
    }

    /**
     * Set otros
     *
     * @param string $otros
     * @return AntecedentesEvaluativos
     */
    public function setOtros($otros)
    {
        $this->otros = $otros;

        return $this;
    }

    /**
     * Get otros
     *
     * @return string 
     */
    public function getOtros()
    {
        return $this->otros;
    }
}
