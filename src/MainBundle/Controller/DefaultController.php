<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MainBundle:Default:index.html.twig', array(
            'result' => 'INDEX',
        ));
        
    }

    public function dumpDataAction()
    {
        $HOST_LOCAL = $this->container->getParameter('host_local');
        $USER_LOCAL = $this->container->getParameter('user_local');
        $PASS_LOCAL = $this->container->getParameter('pass_local');
        $DB_LOCAL = $this->container->getParameter('db_local');

        $HOST_ONLINE = $this->container->getParameter('host_online');
        $USER_ONLINE = $this->container->getParameter('user_online');
        $PASS_ONLINE = $this->container->getParameter('pass_online');
        $DB_ONLINE = $this->container->getParameter('db_online');

        $PATH = $this->container->getParameter('kernel.root_dir') . '/../web';

        $TABLES = array('antecedentes_familiares_otro', 'caracteristicas_desarrollo', 'datos_centro_trabajo', 'datos_familiares_hermano', 'datos_familiares_madre', 'datos_familiares_padre', 'descripcion_antecedentes_atencion', 'descripcion_embarazo', 'habitos_hijo', 'historia_clinica', 'historia_estado_salud', 'historia_institucional', 'motivo_consulta', 'n_frecuencia', 'n_grado_escolar', 'n_parentesco', 'n_periodo', 'n_tratamiento', 'nacimiento_parto', 'paciente', 'tratamiento_medico');
        
        self::dumpSql($HOST_ONLINE,$USER_ONLINE,$PASS_ONLINE,$DB_ONLINE, $DB_LOCAL, $PATH, $TABLES);
        self::deleteData($HOST_LOCAL,$USER_LOCAL,$PASS_LOCAL,$DB_LOCAL, $TABLES);
        self::importSql($HOST_LOCAL, $USER_LOCAL, $PASS_LOCAL, $DB_LOCAL, $PATH);
        return $this->render('MainBundle:Default:index.html.twig', array(
            'result' => 'OK',
        ));
    }

    private static function dumpSql($HOST_ONLINE,$USER_ONLINE,$PASS_ONLINE,$DB_ONLINE, $DB_LOCAL, $PATH, $TABLES){

        $mysqli = new \mysqli($HOST_ONLINE,$USER_ONLINE,$PASS_ONLINE,$DB_ONLINE); 
        $mysqli->select_db($DB_ONLINE); 
        $mysqli->query("SET NAMES 'utf8'");

        $queryTables    = $mysqli->query('SHOW TABLES'); 
        while($row = $queryTables->fetch_row()) 
        { 
            if(in_array($row[0], $TABLES)){
                $target_tables[] = $row[0]; 
            }
        }
        foreach($target_tables as $table)
        {
            $result         =   $mysqli->query('SELECT * FROM '.$table);  
            $fields_amount  =   $result->field_count;  
            $rows_num=$mysqli->affected_rows;     
            $res            =   $mysqli->query('SHOW CREATE TABLE '.$table); 
            $TableMLine     =   $res->fetch_row();
            $content        = (!isset($content) ?  '' : $content) . "\n";

            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) 
            {
                while($row = $result->fetch_row())  
                { //when started (and every after 100 command cycle):
                    if ($st_counter%100 == 0 || $st_counter == 0 )  
                    {
                            $content .= "\nINSERT INTO ".$table." VALUES";
                    }
                    $content .= "\n(";
                    for($j=0; $j<$fields_amount; $j++)  
                    { 
                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
                        if (isset($row[$j]))
                        {
                            $content .= '"'.$row[$j].'"' ; 
                        }
                        else 
                        {   
                            $content .= '""';
                        }     
                        if ($j<($fields_amount-1))
                        {
                                $content.= ',';
                        }      
                    }
                    $content .=")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) 
                    {   
                        $content .= ";";
                    } 
                    else 
                    {
                        $content .= ",";
                    } 
                    $st_counter=$st_counter+1;
                }
            } $content .="\n";
        }
        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";

        $lines = explode("\n", $content);

        file_exists($PATH.'/'.$DB_LOCAL.'.sql');
        $file = fopen($PATH.'/'.$DB_LOCAL.'.sql', 'w');
        foreach ($lines as $line) {
            fwrite($file, $line.PHP_EOL);
        }
        fclose($file);

        /*header('Content-Type: application/octet-stream');   
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"".$DB_LOCAL.'.sql'."\"");
        echo $content; exit;*/
    }

   /* private static function importSql($HOST_LOCAL, $USER_LOCAL, $PASS_LOCAL, $DB_LOCAL, $PATH){
        $filename = $PATH.'/'.$DB_LOCAL.'.sql';
        $maxRuntime = 8; // less then your max script execution limit

        $deadline = time()+$maxRuntime; 
        $progressFilename = $filename.'_filepointer'; // tmp file for progress
        $errorFilename = $filename.'_error'; // tmp file for erro

        mysql_connect($HOST_LOCAL, $USER_LOCAL, $PASS_LOCAL) OR die('connecting to host: '.$HOST_LOCAL.' failed: '.mysql_error());
        mysql_select_db($DB_LOCAL) OR die('select db: '.$DB_LOCAL.' failed: '.mysql_error());

        ($fp = fopen($filename, 'r')) OR die('failed to open file:'.$filename);

        // check for previous error
        if( file_exists($errorFilename) ){
            die('<pre> previous error: '.file_get_contents($errorFilename));
        }

        // go to previous file position
        $filePosition = 0;
        if( file_exists($progressFilename) ){
            $filePosition = file_get_contents($progressFilename);
            fseek($fp, $filePosition);
        }

        $queryCount = 0;
        $query = '';
        while( $deadline>time() AND ($line=fgets($fp, 1024000)) ){
            if(substr($line,0,2)=='--' OR trim($line)=='' ){
                continue;
            }

            $query .= $line;
            if( substr(trim($query),-1)==';' ){
                if( !mysql_query($query) ){
                    $error = 'Error performing query \'<strong>' . $query . '\': ' . mysql_error();
                    file_put_contents($errorFilename, $error."\n");
                    exit;
                }
                $query = '';
                file_put_contents($progressFilename, ftell($fp)); // save the current file position for 
                $queryCount++;
            }
        }

        if( feof($fp) ){
            echo 'dump successfully restored!';
        }else{
            echo ftell($fp).'/'.filesize($filename).' '.(round(ftell($fp)/filesize($filename), 2)*100).'%'."\n";
            echo $queryCount.' queries processed! please reload or wait for automatic browser refresh!';
        }
    }*/

    private static function importSql($HOST_LOCAL, $USER_LOCAL, $PASS_LOCAL, $DB_LOCAL, $PATH){
        $mysqli = new \mysqli($HOST_LOCAL, $USER_LOCAL, $PASS_LOCAL, $DB_LOCAL);
        $mysqli->select_db($DB_LOCAL); 
        $mysqli->query("SET NAMES 'utf8'");

        $filename = $PATH.'/'.$DB_LOCAL.'.sql'; //How to Create SQL File Step : url:http://localhost/phpmyadmin->detabase select->table select->Export(In Upper Toolbar)->Go:DOWNLOAD .SQL FILE
        $op_data = '';
        $lines = file($filename);
        foreach ($lines as $line)
        {
            if (substr($line, 0, 2) == '--' || $line == '')//This IF Remove Comment Inside SQL FILE
            {
                continue;
            }
            $op_data .= $line;
            if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY
            {
                $mysqli->query($op_data);
                $op_data = '';
            }
        }
        echo "Table Created Inside " . $filename . " Database.......";
    }

    private static function deleteData($HOST_LOCAL,$USER_LOCAL,$PASS_LOCAL,$DB_LOCAL, $TABLES){
        
        $mysqli = new \mysqli($HOST_LOCAL,$USER_LOCAL,$PASS_LOCAL,$DB_LOCAL); 
        $mysqli->select_db($DB_LOCAL); 
        $mysqli->query("SET NAMES 'utf8'");

        foreach ($TABLES as $table) {
            $mysqli->query('DELETE FROM '.$table);
        }
    }
}
