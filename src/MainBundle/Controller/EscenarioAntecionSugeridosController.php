<?php

namespace MainBundle\Controller;

use MainBundle\Entity\EscenarioAntecionSugeridos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Escenarioantecionsugerido controller.
 *
 */
class EscenarioAntecionSugeridosController extends Controller
{
    /**
     * Lists all escenarioAntecionSugerido entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $escenarioAntecionSugeridos = $em->getRepository('MainBundle:EscenarioAntecionSugeridos')->findAll();

        return $this->render('escenarioantecionsugeridos/index.html.twig', array(
            'escenarioAntecionSugeridos' => $escenarioAntecionSugeridos,
        ));
    }

    /**
     * Creates a new escenarioAntecionSugerido entity.
     *
     */
    public function newAction(Request $request)
    {
        $escenarioAntecionSugerido = new Escenarioantecionsugerido();
        $form = $this->createForm('MainBundle\Form\EscenarioAntecionSugeridosType', $escenarioAntecionSugerido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($escenarioAntecionSugerido);
            $em->flush($escenarioAntecionSugerido);

            return $this->redirectToRoute('escenarioantecionsugeridos_show', array('id' => $escenarioAntecionSugerido->getId()));
        }

        return $this->render('escenarioantecionsugeridos/new.html.twig', array(
            'escenarioAntecionSugerido' => $escenarioAntecionSugerido,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a escenarioAntecionSugerido entity.
     *
     */
    public function showAction(EscenarioAntecionSugeridos $escenarioAntecionSugerido)
    {
        $deleteForm = $this->createDeleteForm($escenarioAntecionSugerido);

        return $this->render('escenarioantecionsugeridos/show.html.twig', array(
            'escenarioAntecionSugerido' => $escenarioAntecionSugerido,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing escenarioAntecionSugerido entity.
     *
     */
    public function editAction(Request $request, EscenarioAntecionSugeridos $escenarioAntecionSugerido)
    {
        $deleteForm = $this->createDeleteForm($escenarioAntecionSugerido);
        $editForm = $this->createForm('MainBundle\Form\EscenarioAntecionSugeridosType', $escenarioAntecionSugerido);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('escenarioantecionsugeridos_edit', array('id' => $escenarioAntecionSugerido->getId()));
        }

        return $this->render('escenarioantecionsugeridos/edit.html.twig', array(
            'escenarioAntecionSugerido' => $escenarioAntecionSugerido,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a escenarioAntecionSugerido entity.
     *
     */
    public function deleteAction(Request $request, EscenarioAntecionSugeridos $escenarioAntecionSugerido)
    {
        $form = $this->createDeleteForm($escenarioAntecionSugerido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($escenarioAntecionSugerido);
            $em->flush($escenarioAntecionSugerido);
        }

        return $this->redirectToRoute('escenarioantecionsugeridos_index');
    }

    /**
     * Creates a form to delete a escenarioAntecionSugerido entity.
     *
     * @param EscenarioAntecionSugeridos $escenarioAntecionSugerido The escenarioAntecionSugerido entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EscenarioAntecionSugeridos $escenarioAntecionSugerido)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('escenarioantecionsugeridos_delete', array('id' => $escenarioAntecionSugerido->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
