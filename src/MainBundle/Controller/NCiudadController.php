<?php

namespace MainBundle\Controller;

use MainBundle\Entity\NCiudad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Nciudad controller.
 *
 */
class NCiudadController extends Controller
{
    /**
     * Lists all nCiudad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $nCiudads = $em->getRepository('MainBundle:NCiudad')->findAll();

        return $this->render('nciudad/index.html.twig', array(
            'nCiudads' => $nCiudads,
        ));
    }

    /**
     * Creates a new nCiudad entity.
     *
     */
    public function newAction(Request $request)
    {
        $nCiudad = new Nciudad();
        $form = $this->createForm('MainBundle\Form\NCiudadType', $nCiudad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($nCiudad);
            $em->flush($nCiudad);

            return $this->redirectToRoute('nciudad_show', array('id' => $nCiudad->getId()));
        }

        return $this->render('nciudad/new.html.twig', array(
            'nCiudad' => $nCiudad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a nCiudad entity.
     *
     */
    public function showAction(NCiudad $nCiudad)
    {
        $deleteForm = $this->createDeleteForm($nCiudad);

        return $this->render('nciudad/show.html.twig', array(
            'nCiudad' => $nCiudad,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing nCiudad entity.
     *
     */
    public function editAction(Request $request, NCiudad $nCiudad)
    {
        $deleteForm = $this->createDeleteForm($nCiudad);
        $editForm = $this->createForm('MainBundle\Form\NCiudadType', $nCiudad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('nciudad_edit', array('id' => $nCiudad->getId()));
        }

        return $this->render('nciudad/edit.html.twig', array(
            'nCiudad' => $nCiudad,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a nCiudad entity.
     *
     */
    public function deleteAction(Request $request, NCiudad $nCiudad)
    {
        $form = $this->createDeleteForm($nCiudad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($nCiudad);
            $em->flush($nCiudad);
        }

        return $this->redirectToRoute('nciudad_index');
    }

    /**
     * Creates a form to delete a nCiudad entity.
     *
     * @param NCiudad $nCiudad The nCiudad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(NCiudad $nCiudad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('nciudad_delete', array('id' => $nCiudad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
