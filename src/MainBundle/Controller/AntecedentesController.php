<?php

namespace MainBundle\Controller;

use MainBundle\Entity\Antecedentes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Antecedente controller.
 *
 */
class AntecedentesController extends Controller
{
    /**
     * Lists all antecedente entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $antecedentes = $em->getRepository('MainBundle:Antecedentes')->findAll();

        return $this->render('antecedentes/index.html.twig', array(
            'antecedentes' => $antecedentes,
        ));
    }

    /**
     * Creates a new antecedente entity.
     *
     */
    public function newAction(Request $request)
    {
        $antecedente = new Antecedente();
        $form = $this->createForm('MainBundle\Form\AntecedentesType', $antecedente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($antecedente);
            $em->flush($antecedente);

            return $this->redirectToRoute('antecedentes_show', array('id' => $antecedente->getId()));
        }

        return $this->render('antecedentes/new.html.twig', array(
            'antecedente' => $antecedente,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a antecedente entity.
     *
     */
    public function showAction(Antecedentes $antecedente)
    {
        $deleteForm = $this->createDeleteForm($antecedente);

        return $this->render('antecedentes/show.html.twig', array(
            'antecedente' => $antecedente,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing antecedente entity.
     *
     */
    public function editAction(Request $request, Antecedentes $antecedente)
    {
        $deleteForm = $this->createDeleteForm($antecedente);
        $editForm = $this->createForm('MainBundle\Form\AntecedentesType', $antecedente);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('antecedentes_edit', array('id' => $antecedente->getId()));
        }

        return $this->render('antecedentes/edit.html.twig', array(
            'antecedente' => $antecedente,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a antecedente entity.
     *
     */
    public function deleteAction(Request $request, Antecedentes $antecedente)
    {
        $form = $this->createDeleteForm($antecedente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($antecedente);
            $em->flush($antecedente);
        }

        return $this->redirectToRoute('antecedentes_index');
    }

    /**
     * Creates a form to delete a antecedente entity.
     *
     * @param Antecedentes $antecedente The antecedente entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Antecedentes $antecedente)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('antecedentes_delete', array('id' => $antecedente->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
