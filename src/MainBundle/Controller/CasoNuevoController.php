<?php

namespace MainBundle\Controller;

use MainBundle\Entity\CasoNuevo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Casonuevo controller.
 *
 */
class CasoNuevoController extends Controller
{

    public function buscarAction(Request $request)
    {
        $form = $request->get("form");
        $em = $this->getDoctrine()->getManager();
        array_map('unlink', glob($this->get('kernel')->getRootDir() . '/../web/pdf/cn/*.pdf'));

        // print_r($form['protocoloEvaluacion']);exit;
        $qb = $em->createQueryBuilder();
        $qb->select('c')
            ->from('MainBundle:CasoNuevo', 'c')
            ->where('c.id > 0');

        if ($form['nombre'] != "") {
            $qb->andWhere('c.nombre = :nombre');
            $qb->setParameter('nombre', $form['nombre']);
        }
        if ($form['annoRegistro'] != "" || $form['mesRegistro'] != "") {

            if ($form['annoRegistro'] != "") {
                $annoActual = $form['annoRegistro'];
            } else {
                $annoActual = date('Y');
            }

            if ($form['mesRegistro'] != "") {
                $mes = $form['mesRegistro'];
                $fecha = new \DateTime();
                $fecha2 = new \DateTime();
                $fechaInicio = $fecha->setDate($annoActual, $mes, 1);
                $fechaFin = $fecha2->setDate($annoActual, $mes, 30);

            } else {
                $fecha = new \DateTime();
                $fecha2 = new \DateTime();
                $fechaInicio = $fecha->setDate($annoActual, 1, 1);
                $fechaFin = $fecha2->setDate($annoActual, 12, 31);
            }

            $qb->andWhere('c.fechaRegistroCaso >= :fechaInicio');
            $qb->andWhere('c.fechaRegistroCaso <= :fechaFin');
            $qb->setParameter('fechaInicio', \date($fechaInicio->format('Y-m-d')));
            $qb->setParameter('fechaFin', \date($fechaFin->format('Y-m-d')));

        }
        if ($form['procedencia'] != "") {
            $qb->andWhere('c.procedencia = :procedencia');
            $qb->setParameter('procedencia', $form['procedencia']);
        }
        if ($form['especialista'] != "") {
            $qb->andWhere('c.especialista = :especialista');
            $qb->setParameter('especialista', $form['especialista']);
        }
        if ($form['tipoEspecialistaRemision'] != "") {
            $qb->andWhere('c.tipoEspecialistaRemision = :tipoEspecialistaRemision');
            $qb->setParameter('tipoEspecialistaRemision', $form['tipoEspecialistaRemision']);
        }
        if ($form['sexo'] != "") {
            $qb->andWhere('c.sexo = :sexo');
            $qb->setParameter('sexo', $form['sexo']);
        }
        if (array_key_exists('protocoloEvaluacion', $form)) {
            if ($form['protocoloEvaluacion'] != "") {
                $protocolos = $form['protocoloEvaluacion'];

                $qb->join('c.protocoloEvaluacion', 'p');
                foreach ($protocolos as $key => $value) {
                    $protActual = CasoNuevoController::getProtocolo($value);
                    $query = 'p.' . $protActual . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
                // $qb->andWhere($qb->expr()->in('c.protocoloEvaluacion',$form['protocoloEvaluacion']));
            }
        }
        if (array_key_exists('hipotesisDiagnostica', $form)) {
            if ($form['hipotesisDiagnostica'] != "") {
                $hipotesis = $form['hipotesisDiagnostica'];

                $qb->join('c.hipotesisDiagnostica', 'h');
                foreach ($hipotesis as $key => $value) {
                    $hActual = CasoNuevoController::getHipotesis($value);
                    $query = 'h.' . $hActual . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
                // $qb->andWhere($qb->expr()->in('c.protocoloEvaluacion',$form['protocoloEvaluacion']));
            }
        }
        if (array_key_exists('escenarioAtencionSugeridos', $form)) {
            if ($form['escenarioAtencionSugeridos'] != "") {
                $escenarios = $form['escenarioAtencionSugeridos'];

                $qb->join('c.escenarioAtencionSugeridos', 'e');
                foreach ($escenarios as $key => $value) {
                    $eActual = CasoNuevoController::getEscenarioAtencion($value);
                    $query = 'e.' . $eActual . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
                // $qb->andWhere($qb->expr()->in('c.protocoloEvaluacion',$form['protocoloEvaluacion']));
            }
        }
        if (array_key_exists('servicioAtencionSugerido', $form)) {
            if ($form['servicioAtencionSugerido'] != "") {
                $servicios = $form['servicioAtencionSugerido'];

                $qb->join('c.servicioAtencionSugerido', 's');
                foreach ($servicios as $key => $value) {
                    $sActual = CasoNuevoController::getServicioAtencion($value);
                    $query = 's.' . $sActual . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
                // $qb->andWhere($qb->expr()->in('c.protocoloEvaluacion',$form['protocoloEvaluacion']));
            }
        }
        if ($form['edad'] != "") {
            switch ($form['edad']) {
                case 0:
                    $qb->andWhere($qb->expr()->between('c.edad', 0, 5));
                    break;
                case 6:
                    $qb->andWhere($qb->expr()->between('c.edad', 6, 10));
                    break;
                case 11 :
                    $qb->andWhere($qb->expr()->between('c.edad', 11, 15));
                    break;
                case 16:
                    $qb->andWhere($qb->expr()->between('c.edad', 16, 20));
                    break;
                case 21:
                    $qb->andWhere($qb->expr()->between('c.edad', 21, 25));
                    break;
                case 26:
                    $qb->andWhere($qb->expr()->between('c.edad', 26, 30));
                    break;
                case 31:
                    $qb->andWhere($qb->expr()->between('c.edad', 31, 35));
                    break;

                default:
                    $qb->andWhere($qb->expr()->between('c.edad', 36, 90));
                    break;
            }
        }
        $query = $qb->getQuery();
        $casoNuevos = $query->getResult();
        $total = count($casoNuevos);
        /*GENERAR PDF*/
        $date = date_create();
        $fecha = date_timestamp_get($date);
        $nombrePdf = $fecha . '-resul-filtro.pdf';
        $nombreExcl = $fecha . 'resultado-filtro.xlsx';
        $dirPdf = $this->get('kernel')->getRootDir() . '/../web/pdf/cn/' . $nombrePdf;
        if ($request->getHost() == 'localhost') {
            $httpDirPDf = 'http://localhost:8000/pdf/cn/' . $nombrePdf;
            $httpDirExcl = 'http://localhost:8000/pdf/cn/' . $nombreExcl;
        } else {
            $httpDirPDf = 'http://' . $request->getHost() . '/hic/web/pdf/cn/' . $nombrePdf;
            $httpDirExcl = 'http://' . $request->getHost() . '/hic/web/pdf/cn/' . $nombreExcl;
        }
        $this->get('knp_snappy.pdf')->generateFromHtml(
            $this->renderView(
                'MainBundle:Default:result_filtro_cn_pdf.html.twig',
                array(
                    'total' => $total,
                    'casosNuevos' => $casoNuevos,
                    'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
                )
            ), $dirPdf

        );
        /*GENERAR EXCl*/
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator("RedDiversidad");
        $columna = 3;
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Cantidad de resultados:' . count($casoNuevos));
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Nombre')
            ->setCellValue('B2', 'Apellido Paterno')
            ->setCellValue('C2', 'Apellido Materno')
            ->setCellValue('D2', 'Edad')
            ->setCellValue('E2', 'Procedencia')
            ->setCellValue('F2', 'Profesional')
            ->setCellValue('G2', 'Fecha Registro');
        foreach ($casoNuevos as $caso) {
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A' . $columna, $caso->getNombre())
                ->setCellValue('B' . $columna, $caso->getApellidoPaterno())
                ->setCellValue('C' . $columna, $caso->getApellidoMaterno())
                ->setCellValue('D' . $columna, $caso->getEdad())
                ->setCellValue('E' . $columna, $caso->getProcedencia())
                ->setCellValue('F' . $columna, $caso->getEspecialista())
                ->setCellValue('G' . $columna, $caso->getFechaRegistroCaso());
            $columna++;
        }

        $phpExcelObject->getActiveSheet()->setTitle('Simple');
        $phpExcelObject->setActiveSheetIndex(0);
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // The save method is documented in the official PHPExcel library
        $dirExcel = $this->get('kernel')->getRootDir() . '/../web/pdf/cn/' . $nombreExcl;
        $writer->save($dirExcel);

        return $this->render('casonuevo/resultado_busqueda_cn.html.twig', array(
            'casoNuevos' => $casoNuevos,
            'cantidadResultados' => count($casoNuevos),
            'httpDirPDf' => $httpDirPDf,
            'httpDirExcl' => $httpDirExcl
        ));
    }

    public static function getProtocolo($posicion)
    {
        switch ($posicion) {
            case 1:
                return "formatoAtencionTemprana";
                break;
            case 2:
                return "evaluacionDesarrollo";
                break;
            case 3:
                return "neuropsicologia";
                break;
            case 4:
                return "protocolaTea";
                break;
            case 5:
                return "observacionEscolar";
                break;
            case 6:
                return "comunicacionLenguaje";
                break;
            case 7:
                return "inteligencia";
                break;
            case 8:
                return "habilidadesSociales";
                break;
            case 9:
                return "protocoloTda";
                break;
            case 10:
                return "emocionales";
                break;
            case 11:
                return "motricidad";
                break;
            case 12:
                return "pedagogiaAcademica";
                break;
            case 13:
                return "autonomia";
                break;
            case 14:
                return "protocoloAprendizaje";
                break;
            case 15:
                return "sensoriales";

            default:
                # code...
                break;
        }
        return null;
    }

    public static function getHipotesis($posicion)
    {
        switch ($posicion) {
            case 1:
                return "alteracionesSocioComunicativas";
                break;
            case 2:
                return "difAprendAcademico";
                break;
            case 3:
                return "deficitAtencional";
                break;
            case 4:
                return "retardoGlobalDesarrollo";
                break;
            case 5:
                return "difDesarrolloLenguaje";
                break;
            case 6:
                return "difMotoras";
                break;
            case 7:
                return "alteracionesComportamiento";
                break;
            case 8:
                return "necesidadApoyoEmocional";
                break;
            case 9:
                return "otra";
                break;

            default:
                # code...
                break;
        }
    }

    public static function getEscenarioAtencion($posicion)
    {
        switch ($posicion) {
            case 1:
                return "consultaTerap";
                break;
            case 2:
                return "sistemaAmbulatorio";
                break;
            case 3:
                return "ambienteEducativo";
                break;
            case 4:
                return "ambitoSocial";
                break;
            case 5:
                return "otro";
                break;
            default:
                # code...
                break;
        }
    }

    public static function getServicioAtencion($posicion)
    {
        switch ($posicion) {
            case 1:
                return "atencionTemprana";
                break;
            case 2:
                return "intervNeuroPsicologica";
                break;
            case 3:
                return "desarrolloHabilSociales";
                break;
            case 4:
                return "transicionVidaAdulta";
                break;
            case 5:
                return "apoyoFamiliar";
                break;
            case 6:
                return "otro";
                break;
            default:
                # code...
                break;
        }
    }

    /**
     * Lists all casoNuevo entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $casoNuevos = $em->getRepository('MainBundle:CasoNuevo')->findBy(array(), array('fechaRegistroCaso' => 'DESC'));
        } else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $casoNuevos = $em->getRepository('MainBundle:CasoNuevo')->findBy(array('especialista' => $user), array('fechaRegistroCaso' => 'ASC'));
        }
        $form = $this->createFormBuilder()
            ->add('nombre', 'text', array('required' => false))
            ->add('especialista', EntityType::class, array(
                'class' => 'UsuarioBundle:Usuario',
                'required' => false,
            ))
            ->add('mesRegistro', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '1' => 'Enero',
                    '2' => 'Febrero',
                    '3' => 'Marzo',
                    '4' => 'Abril',
                    '5' => 'Mayo',
                    '6' => 'Junio',
                    '7' => 'Julio',
                    '8' => 'Agosto',
                    '9' => 'Septiembre',
                    '10' => 'Octubre',
                    '11' => 'Noviembre',
                    '12' => 'Diciembre',
                )
            ))
            ->add('annoRegistro', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '2015' => '2015',
                    '2016' => '2016',
                    '2017' => '2017',
                    '2018' => '2018',
                    '2019' => '2019',
                    '2020' => '2020',
                    '2021' => '2021',
                    '2022' => '2022',
                ),
                //'months'=>array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'),
                'choice_translation_domain' => true
            ))
            ->add('tipoEspecialistaRemision', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'escuela' => 'escuela',
                    'otrafamilia' => 'Otra Familia',
                    'neuropediatra' => 'neuropediatra',
                    'Paidopsiquiatra' => 'Paidopsiquiatra',
                    'tlenguaje' => 'T. Lenguaje',
                    'pediatra' => 'Pediatra',
                    'fisioterapia' => 'Fisioterapia',
                    'otro' => 'otro',
                ),
            ))
            // ->add('apellidoMaterno', 'text', array('required'=>false))
            // ->add('apellidoMaterno', 'text', array('required'=>false))
            ->add('procedencia', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'Querétaro' => 'Querétaro',
                    'Colima' => 'Colima',
                    'Monterrey' => 'Monterrey',
                    'Veracruz' => 'Veracruz',
                    'Puebla' => 'Puebla',
                    'Aguascalientes' => 'Aguascalientes',
                    'Baja California' => 'Baja California',
                    'Baja California Sur' => 'Baja California Sur',
                    'Campeche' => 'Campeche',
                    'Chihuahua' => 'Chihuahua',
                    'Coahuila' => 'Coahuila',
                    'Chiapas' => 'Chiapas',
                    'Ciudad México' => 'Ciudad México',
                    'Durango' => 'Durango',
                    'Estado de México' => 'Estado de México',
                    'Guanajuato' => 'Guanajuato',
                    'Guerrero' => 'Guerrero',
                    'Hidalgo' => 'Hidalgo',
                    'Jalisco' => 'Jalisco',
                    'Michoacán' => 'Michoacán',
                    'Morelos' => 'Morelos',
                    'Nayarit' => 'Nayarit',
                    'Nuevo León' => 'Nuevo León',
                    'Quintana Roo' => 'Quintana Roo',
                    'San Luis Potosí' => 'San Luis Potosí',
                    'Sonora' => 'Sonora',
                    'Tabasco' => 'Tabasco',
                    'Tamaulipas' => 'Tamaulipas',
                    'Tlaxcala' => 'Tlaxcala',
                    'Yucatán' => 'Yucatán',
                    'Zacatecas' => 'Zacatecas',
                )))
            ->add('edad', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0-5',
                    '6' => '6-10',
                    '11' => '11-15',
                    '16' => '16-20',
                    '21' => '21-25',
                    '26' => '26-30',
                    '31' => '31-35',
                    '36' => '> 36',
                )
            ))
            ->add('sexo', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'masculino' => 'masculino',
                    'femenino' => 'femenino',
                )
            ))
            ->add('protocoloEvaluacion', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '1' => 'Formato Atención Temprana',
                    '2' => 'Evaluación del Desarrollo',
                    '3' => 'Neuropsicológica',
                    '4' => 'Protocolo TEA',
                    '5' => 'Observación Escolar',
                    '6' => 'Comunicación y Lenguaje',
                    '7' => 'Inteligencia',
                    '8' => 'Habilidades Sociales',
                    '9' => 'Protocolo TDA',
                    '10' => 'Emocionales',
                    '11' => 'Motricidad',
                    '12' => 'Pedagogía y académicas',
                    '13' => 'Autonomía',
                    '14' => 'Protocolo Aprendizaje',
                    '15' => 'Sensoriales',
                )
            ))
            ->add('hipotesisDiagnostica', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '1' => 'Alteraciones Socio Comunicativas ',
                    '2' => 'Dificultades del aprendizaje académico ',
                    '3' => 'Déficit atencional',
                    '4' => 'Retardo global del desarrollo ',
                    '5' => 'Dificultades en el desarrollo del lenguaje ',
                    '6' => ' Dificultades motoras ',
                    '7' => ' Alteraciones del comportamiento ',
                    '8' => ' Necesidades de apoyo emocional ',
                    '9' => ' Otra ',
                )
            ))
            ->add('escenarioAtencionSugeridos', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '1' => ' Consulta terapéutica ',
                    '2' => ' Sistema ambulatorio  ',
                    '3' => ' Ambiente educativo ',
                    '4' => ' Ambito social ',
                    '5' => ' Otro ',
                )
            ))
            ->add('servicioAtencionSugerido', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '1' => 'Atención temprana ',
                    '2' => 'Intervención neuropsicológica ',
                    '3' => 'Desarrollo de habilidades sociales ',
                    '4' => 'Transición adolescencia y vida adulta ',
                    '6' => 'Apoyo familiar  ',
                    '7' => 'Otro ',
                )
            ))
            ->getForm();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $casoNuevos, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('casonuevo/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
            'total' => count($casoNuevos)
        ));
    }


    public function createPdfAction(Request $request, CasoNuevo $casoNuevo)
    {

        $html = $this->renderView('MainBundle:Default:casonuevo_pdf.html.twig', array(
            'caso' => $casoNuevo,
            'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
        ));
        $fechaNacimiento = $casoNuevo->getFechaNacimiento()->format('Y-m-d');
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fechaNacimiento . '-' . $casoNuevo->getNombre() . $casoNuevo->getApellidoPaterno() . '-' . $casoNuevo->getEspecialista() . '-' . $casoNuevo->getProcedencia() . '.pdf"'
            )
        );
    }

    /**
     * Creates a new casoNuevo entity.
     *
     */
    public function newAction(Request $request)
    {
        $casoNuevo = new Casonuevo();
        $form = $this->createForm('MainBundle\Form\CasoNuevoType', $casoNuevo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fechaNac = $casoNuevo->getFechaNacimiento();
            $anoNac = $fechaNac->format('Y');
            $fechaActual = date_create();
            $anoActual = $fechaActual->format('Y');
            $edad = $anoActual - $anoNac;
            $casoNuevo->setEdad($edad);
            $em->persist($casoNuevo);
            $em->flush($casoNuevo);
            $html = $this->renderView('MainBundle:Default:casonuevo_pdf.html.twig', array(
                'caso' => $casoNuevo,
                'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
            ));
            $date = date_create();
            $fecha = date_timestamp_get($date);
            $url = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . '/' . $fecha . 'pdf_casonuevo.pdf';
            $this->get('knp_snappy.pdf')->generateFromHtml($html, $url);
            /*enviar email*/
            $message = \Swift_Message::newInstance()
                ->setSubject('Registro de Nuevo Caso')
                ->setFrom('reddiversidadstats@gmail.com')
                ->setTo(['edellopeza@yahoo.com', $casoNuevo->getEspecialista()->getEmail()])
                ->setBody($this->renderView('MainBundle:Default:email_cn.txt.twig',
                    array(
                        'paciente' => $casoNuevo->getNombre(),
                        'apellidoPaterno' => $casoNuevo->getApellidoPaterno(),
                        'apellidoMaterno' => $casoNuevo->getApellidoMaterno(),
                        'ciudad' => $casoNuevo->getProcedencia(),
                    )
                ));
            $message->attach(\Swift_Attachment::fromPath($url));
            $this->get('mailer')->send($message);
            //return $this->redirectToRoute('casonuevo_index');
            return $this->redirectToRoute('casonuevo_edit', array('id' => $casoNuevo->getId()));

        }

        return $this->render('casonuevo/new.html.twig', array(
            'casoNuevo' => $casoNuevo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a casoNuevo entity.
     *
     */
    public function showAction(CasoNuevo $casoNuevo)
    {
        $deleteForm = $this->createDeleteForm($casoNuevo);

        return $this->render('casonuevo/show.html.twig', array(
            'casoNuevo' => $casoNuevo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing casoNuevo entity.
     *
     */
    public function editAction(Request $request, CasoNuevo $casoNuevo)
    {
        $deleteForm = $this->createDeleteForm($casoNuevo);
        $editForm = $this->createForm('MainBundle\Form\CasoNuevoType', $casoNuevo);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->render('casonuevo/editar.html.twig', array(
            'casoNuevo' => $casoNuevo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a casoNuevo entity.
     *
     */
    public function deleteAction(Request $request, CasoNuevo $casoNuevo)
    {
        $form = $this->createDeleteForm($casoNuevo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($casoNuevo);
            $em->flush($casoNuevo);
        }

        return $this->redirectToRoute('casonuevo_index');
    }

    /**
     * Creates a form to delete a casoNuevo entity.
     *
     * @param CasoNuevo $casoNuevo The casoNuevo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CasoNuevo $casoNuevo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('casonuevo_delete', array('id' => $casoNuevo->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
