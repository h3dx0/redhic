<?php

namespace MainBundle\Controller;

use MainBundle\Entity\EstadoSaludEnfermedades;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Estadosaludenfermedade controller.
 *
 */
class EstadoSaludEnfermedadesController extends Controller
{
    /**
     * Lists all estadoSaludEnfermedade entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $estadoSaludEnfermedades = $em->getRepository('MainBundle:EstadoSaludEnfermedades')->findAll();

        return $this->render('estadosaludenfermedades/index.html.twig', array(
            'estadoSaludEnfermedades' => $estadoSaludEnfermedades,
        ));
    }

    /**
     * Creates a new estadoSaludEnfermedade entity.
     *
     */
    public function newAction(Request $request)
    {
        $estadoSaludEnfermedade = new Estadosaludenfermedade();
        $form = $this->createForm('MainBundle\Form\EstadoSaludEnfermedadesType', $estadoSaludEnfermedade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($estadoSaludEnfermedade);
            $em->flush($estadoSaludEnfermedade);

            return $this->redirectToRoute('estadosaludenfermedades_show', array('id' => $estadoSaludEnfermedade->getId()));
        }

        return $this->render('estadosaludenfermedades/new.html.twig', array(
            'estadoSaludEnfermedade' => $estadoSaludEnfermedade,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a estadoSaludEnfermedade entity.
     *
     */
    public function showAction(EstadoSaludEnfermedades $estadoSaludEnfermedade)
    {
        $deleteForm = $this->createDeleteForm($estadoSaludEnfermedade);

        return $this->render('estadosaludenfermedades/show.html.twig', array(
            'estadoSaludEnfermedade' => $estadoSaludEnfermedade,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing estadoSaludEnfermedade entity.
     *
     */
    public function editAction(Request $request, EstadoSaludEnfermedades $estadoSaludEnfermedade)
    {
        $deleteForm = $this->createDeleteForm($estadoSaludEnfermedade);
        $editForm = $this->createForm('MainBundle\Form\EstadoSaludEnfermedadesType', $estadoSaludEnfermedade);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('estadosaludenfermedades_edit', array('id' => $estadoSaludEnfermedade->getId()));
        }

        return $this->render('estadosaludenfermedades/edit.html.twig', array(
            'estadoSaludEnfermedade' => $estadoSaludEnfermedade,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a estadoSaludEnfermedade entity.
     *
     */
    public function deleteAction(Request $request, EstadoSaludEnfermedades $estadoSaludEnfermedade)
    {
        $form = $this->createDeleteForm($estadoSaludEnfermedade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($estadoSaludEnfermedade);
            $em->flush($estadoSaludEnfermedade);
        }

        return $this->redirectToRoute('estadosaludenfermedades_index');
    }

    /**
     * Creates a form to delete a estadoSaludEnfermedade entity.
     *
     * @param EstadoSaludEnfermedades $estadoSaludEnfermedade The estadoSaludEnfermedade entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EstadoSaludEnfermedades $estadoSaludEnfermedade)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estadosaludenfermedades_delete', array('id' => $estadoSaludEnfermedade->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
