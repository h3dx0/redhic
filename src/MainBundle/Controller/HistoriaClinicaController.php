<?php

namespace MainBundle\Controller;

use MainBundle\Entity\HistoriaClinica;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Historiaclinica controller.
 *
 */
class HistoriaClinicaController extends Controller
{
    /**
     * Lists all historiaClinica entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $historiaClinicas = $em->getRepository('MainBundle:HistoriaClinica')->findBy(array(), array('id' => 'DESC'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $historiaClinicas, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            /*limit per page*/
            10
        );
        return $this->render('historiaclinica/index.html.twig', array(
            'pagination' => $pagination,
            'total' => count($historiaClinicas)
        ));
    }

    public function finAction()
    {
        return $this->render('historiaclinica/finencuesta.html.twig');
    }

    public function ejecutarFiltroAction(Request $request)
    {
        $form = $request->get("form");
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('h')
            ->from('MainBundle:HistoriaClinica', 'h')
            ->join('h.paciente', 'p')
            ->where('h.id > 0');

        if ($form['nombre'] != "") {
            $qb->andWhere('p.nombre = :nombre');
            $qb->setParameter('nombre', $form['nombre']);
        }
        if ($form['procedencia'] != "") {
            $qb->andWhere('p.ciudad = :ciudad');
            $qb->setParameter('ciudad', $form['procedencia']);
        }

        if ($form['sexo'] != "") {
            $qb->andWhere('h.hcDgnSexo = :sexo');
            $qb->setParameter('sexo', $form['sexo']);
        }
        if ($form['edad'] != "") {
            switch ($form['edad']) {
                case 0:
                    $qb->andWhere($qb->expr()->between('p.edad', 0, 5));
                    break;
                case 6:
                    $qb->andWhere($qb->expr()->between('p.edad', 6, 10));
                    break;
                case 11 :
                    $qb->andWhere($qb->expr()->between('p.edad', 11, 15));
                    break;
                case 16:
                    $qb->andWhere($qb->expr()->between('p.edad', 16, 20));
                    break;
                case 21:
                    $qb->andWhere($qb->expr()->between('p.edad', 21, 25));
                    break;
                case 26:
                    $qb->andWhere($qb->expr()->between('p.edad', 26, 30));
                    break;
                case 31:
                    $qb->andWhere($qb->expr()->between('p.edad', 31, 35));
                    break;

                default:
                    $qb->andWhere($qb->expr()->between('p.edad', 36, 90));
                    break;
            }
        }

        if ($form['quien_remite'] != "") {
            $valor = $form['quien_remite'];
            switch ($valor) {
                case 'neuropediatra':
                    $qb->andWhere('h.hcRemNenopediatra = :hcRemNenopediatra');
                    $qb->setParameter('hcRemNenopediatra', 1);
                    break;
                case 'psicologo':
                    $qb->andWhere('h.hcRemPsicologo = :hcRemPsicologo');
                    $qb->setParameter('hcRemPsicologo', 1);
                    break;
                case 'psiquiatra':
                    $qb->andWhere('h.hcRemPsiquiatra = :hcRemPsiquiatra');
                    $qb->setParameter('hcRemPsiquiatra', 1);
                    break;
                case 'terapia_lenguaje':
                    $qb->andWhere('h.hcRemTerapiaLenguaje = :hcRemTerapiaLenguaje');
                    $qb->setParameter('hcRemTerapiaLenguaje', 1);
                    break;
                case 'otro':
                    $qb->andWhere('h.hcRemOtro = :hcRemOtro');
                    $qb->setParameter('hcRemOtro', 1);
                    break;
                case 'propia_familia':
                    $qb->andWhere('h.hcRemBusqueda = :hcRemBusqueda');
                    $qb->setParameter('hcRemBusqueda', 1);
                    break;
            }
        }

        if ($form['antecendes_atencion'] != "") {
            $valor = $form['antecendes_atencion'];
//            print_r($valor);exit;
            switch ($valor) {
                case 'terapia_de_lenguaje':
                    $qb->andWhere('h.hcAdaTerapiaLenguaje = :hcAdaTerapiaLenguaje');
                    $qb->setParameter('hcAdaTerapiaLenguaje', 1);
                    break;
                case 'farmacosn_neurológicos':
                    $qb->andWhere('h.hcAdaFarmacosNeurologicos = :hcAdaFarmacosNeurologicos');
                    $qb->setParameter('hcAdaFarmacosNeurologicos', 1);
                    break;

                case 'apoyo_pedagógico':
                    $qb->andWhere('h.hcAdaApoyoPedagogico = :hcAdaApoyoPedagogico');
                    $qb->setParameter('hcAdaApoyoPedagogico', 1);
                    break;
                case 'abordaje_nutricional':
                    $qb->andWhere('h.hcAdaTerapiaOcupacional = :hcAdaTerapiaOcupacional');
                    $qb->setParameter('hcAdaTerapiaOcupacional', 1);
                    break;
                case 'terapia_asistida_animales':
                    $qb->andWhere('h.hcAdaTerapiaAsistAnimales = :hcAdaTerapiaAsistAnimales');
                    $qb->setParameter('hcAdaTerapiaAsistAnimales', 1);
                    break;
                case 'terapias_sensoriales':
                    $qb->andWhere('h.hcAdaTerapiasSensoriales = :hcAdaTerapiasSensoriales');
                    $qb->setParameter('hcAdaTerapiasSensoriales', 1);
                    break;
                case 'estimulación_temprana':
                    $qb->andWhere('h.hcAdaEstimulacionTemprana = :hcAdaEstimulacionTemprana');
                    $qb->setParameter('hcAdaEstimulacionTemprana', 1);
                    break;
                case 'apoyo_psicológico':
                    $qb->andWhere('h.hcAdaApoyoPsicologico = :hcAdaApoyoPsicologico');
                    $qb->setParameter('hcAdaApoyoPsicologico', 1);
                    break;
                case 'otro':
                    $qb->andWhere('h.hcAdaOtro = :hcAdaOtro');
                    $qb->setParameter('hcAdaOtro', 1);
                    break;
            }
        }
        if ($form['examenes'] != "") {
            $valor = $form['examenes'];
            switch ($valor) {
                case 'tac':
                    $qb->andWhere('h.hcExaTac = :hcExaTac');
                    $qb->setParameter('hcExaTac', 1);
                    break;

                case 'potenciales _evocados':
                    $qb->andWhere('h.hcExaPotencialesEmocados = :hcExaPotencialesEmocados');
                    $qb->setParameter('hcExaPotencialesEmocados', 1);
                    break;
                case 'valoración_genética':
                    $qb->andWhere('h.hcExaValoracionGenetica = :hcExaValoracionGenetica');
                    $qb->setParameter('hcExaValoracionGenetica', 1);
                    break;
                case 'tamiz_metabólico':
                    $qb->andWhere('h.hcTamizMetabolico = :hcTamizMetabolico');
                    $qb->setParameter('hcTamizMetabolico', 1);
                    break;
                case 'resonancia_magnética':
                    $qb->andWhere('h.hcExaResonanciaMagnetica = :hcExaResonanciaMagnetica');
                    $qb->setParameter('hcExaResonanciaMagnetica', 1);
                    break;
                case 'evaluación_neuropsicológica':
                    $qb->andWhere('h.hsEvalNenopsicologica = :hsEvalNenopsicologica');
                    $qb->setParameter('hsEvalNenopsicologica', 1);
                    break;
                case 'evaluación_lenguaje':
                    $qb->andWhere('h.hcExaEvalLenguaje = :hcExaEvalLenguaje');
                    $qb->setParameter('hcExaEvalLenguaje', 1);
                    break;
                case 'otro':
                    $qb->andWhere('h.hcExaOtra = :hcExaOtra');
                    $qb->setParameter('hcExaOtra', 1);
                    break;
            }
        }
        if ($form['edad_mama'] != "") {
            $valor = $form['edad_mama'];
            switch ($valor) {
                case '15':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 15, 20));
                    break;
                case '21':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 21, 25));
                    break;

                case '26':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 26, 30));
                    break;
                case '31':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 31, 35));
                    break;
                case '36':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 36, 40));
                    break;
                case '41':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 41, 45));
                    break;
                case '46':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 46, 50));
                    break;
                case '51':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 51, 55));
                    break;
                case '56':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 56, 60));
                    break;
                case '61':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadMama', 61, 65));
                    break;


            }
        }

        if ($form['edad_papa'] != "") {
            $valor = $form['edad_mama'];
            switch ($valor) {
                case '15':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 15, 20));
                    break;
                case '21':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 21, 25));
                    break;

                case '26':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 26, 30));
                    break;
                case '31':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 31, 35));
                    break;
                case '36':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 36, 40));
                    break;
                case '41':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 41, 45));
                    break;
                case '46':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 46, 50));
                    break;
                case '51':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 51, 55));
                    break;
                case '56':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 56, 60));
                    break;
                case '60':
                    $qb->andWhere($qb->expr()->between('h.hcEmbEdadPapa', 60, 65));
                    break;


            }
        }

        if ($form['parto_fue']) {
            $valor = $form['parto_fue'];
            switch ($valor) {
                case 'natural':
                    $qb->andWhere('h.hcNacpPartoNatural = :hcNacpPartoNatural');
                    $qb->setParameter('hcNacpPartoNatural', 1);
                    break;
                case 'cesarea':
                    $qb->andWhere('h.hcNacpPartoCesarea = :hcNacpPartoCesarea');
                    $qb->setParameter('hcNacpPartoCesarea', 1);
                    break;
                case 'a_termino':
                    $qb->andWhere('h.hcNacpPartoTermino = :hcNacpPartoTermino');
                    $qb->setParameter('hcNacpPartoTermino', 1);
                    break;
                case 'prematuro':
                    $qb->andWhere('h.hcNacpPartoPretermino = :hcNacpPartoPretermino');
                    $qb->setParameter('hcNacpPartoPretermino', 1);
                    break;
                case 'post_termino':
                    $qb->andWhere('h.hcNacpPartoPostermino = :hcNacpPartoPostermino');
                    $qb->setParameter('hcNacpPartoPostermino', 1);
                    break;
            }
        }
        if ($form['llanto_fue']) {
            $valor = $form['llanto_fue'];
            switch ($valor) {
                case 'provocado':
                    $qb->andWhere('h.hcNacpLlantoProvocado = :hcNacpLlantoProvocado');
                    $qb->setParameter('hcNacpLlantoProvocado', 1);
                    break;
                case 'espontáneo':
                    $qb->andWhere('h.hcNacpLlantoEspontaneo = :hcNacpLlantoEspontaneo');
                    $qb->setParameter('hcNacpLlantoEspontaneo', 1);
                    break;
            }
        }
        if ($form['peso_fue']) {
            $valor = $form['peso_fue'];
            switch ($valor) {
                case 'bajo_peso':
                    $qb->andWhere('h.hcNacpPesoBajo = :hcNacpPesoBajo');
                    $qb->setParameter('hcNacpPesoBajo', 1);
                    break;
                case 'normal':
                    $qb->andWhere('h.hcNacpPesoNormal = :hcNacpPesoNormal');
                    $qb->setParameter('hcNacpPesoNormal', 1);
                    break;
                case 'sobre_peso':
                    $qb->andWhere('h.hcNacpPesoSobre = :hcNacpPesoSobre');
                    $qb->setParameter('hcNacpPesoSobre', 1);
                    break;
            }
        }
        if ($form['sufrimiento_fetal']) {
            $valor = $form['sufrimiento_fetal'];
            switch ($valor) {
                case 'si':
                    $qb->andWhere('h.hcNacpSufrimientoFetal = :hcNacpSufrimientoFetal');
                    $qb->setParameter('hcNacpSufrimientoFetal', 1);
                    break;
            }
        }
        if ($form['requirio_en_parto']) {
            $valor = $form['requirio_en_parto'];
            switch ($valor) {
                case 'hospitalizacion':
                    $qb->andWhere('h.hcNacpHospitalizacion = :hcNacpHospitalizacion');
                    $qb->setParameter('hcNacpHospitalizacion', 1);
                    break;
                case 'asistencia':
                    $qb->andWhere('h.hcNacpAsistenciaRespiratoria = :hcNacpAsistenciaRespiratoria');
                    $qb->setParameter('hcNacpAsistenciaRespiratoria', 1);
                    break;
                case 'incubadora':
                    $qb->andWhere('h.hcNacpIncubadora = :hcNacpIncubadora');
                    $qb->setParameter('hcNacpIncubadora', 1);
                    break;
                case 'reanimacion':
                    $qb->andWhere('h.hcNacpReanimacion = :hcNacpReanimacion');
                    $qb->setParameter('hcNacpReanimacion', 1);
                    break;
                case 'tratamiento_farmacolpgico':
                    $qb->andWhere('h.hcNacpTratFarmacologico = :hcNacpTratFarmacologico');
                    $qb->setParameter('hcNacpTratFarmacologico', 1);
                    break;
            }
        }
        if ($form['sufrio_en_parto']) {
            $valor = $form['sufrio_en_parto'];
            switch ($valor) {
                case 'accidentes':
                    $qb->andWhere('h.hcEmbAcgMama = :hcEmbAcgMama');
                    $qb->setParameter('hcEmbAcgMama', 1);
                    break;
                case 'enfermedad':
                    $qb->andWhere('h.hcEmbEdembMama = :hcEmbEdembMama');
                    $qb->setParameter('hcEmbEdembMama', 1);
                    break;
                case 'toxicos':
                    $qb->andWhere('h.hcEmbHtoxMama = :hcEmbHtoxMama');
                    $qb->setParameter('hcEmbHtoxMama', 1);
                    break;
                case 'estres':
                    $qb->andWhere('h.hcEmbEadMama = :hcEmbEadMama');
                    $qb->setParameter('hcEmbEadMama', 1);
                    break;
                case 'intoxicaciones':
                    $qb->andWhere('h.hcEmbIaalimMama = :hcEmbIaalimMama');
                    $qb->setParameter('hcEmbIaalimMama', 1);
                    break;
                case 'medicamento':
                    $qb->andWhere('h.hcEmbMedicamentosMama = :hcEmbMedicamentosMama');
                    $qb->setParameter('hcEmbMedicamentosMama', 1);
                    break;
                case 'peso_adecuado':
                    $qb->andWhere('h.hcEmbPesoOkMama = :hcEmbPesoOkMama');
                    $qb->setParameter('hcEmbPesoOkMama', 1);
                    break;
                case 'examenes_periodicos':
                    $qb->andWhere('h.hcEmbExperiodMama = :hcEmbExperiodMama');
                    $qb->setParameter('hcEmbExperiodMama', 1);
                    break;
            }
        }
        if ($form['llanto_fue']) {
            $valor = $form['primer_ano_normal'];
            switch ($valor) {
                case 'si':
                    $qb->andWhere('h.hcCdesTnpav = :hcCdesTnpav');
                    $qb->setParameter('hcCdesTnpav', 1);
                    break;
                case 'no':
                    $qb->andWhere('h.hcCdesTnpav = :hcCdesTnpav');
                    $qb->setParameter('hcCdesTnpav', 0);
                    break;
            }
        }
        if ($form['sonreir_rostro']) {
            $valor = $form['sonreir_rostro'];
            $qb->andWhere('h.hcDsmEdadSrca = :hcDsmEdadSrca');
            $qb->setParameter('hcDsmEdadSrca', $valor);
        }
        if ($form['tomar_objetos_mano']) {
            $valor = $form['tomar_objetos_mano'];
            $qb->andWhere('h.hcDsmEdadTomarObj = :hcDsmEdadTomarObj');
            $qb->setParameter('hcDsmEdadTomarObj', $valor);
        }
        if ($form['manipular_objetos']) {
            $valor = $form['manipular_objetos'];
            $qb->andWhere('h.hcDsmEdadMobjfunc = :hcDsmEdadMobjfunc');
            $qb->setParameter('hcDsmEdadMobjfunc', $valor);
        }
        if ($form['prestar_atencion']) {
            $valor = $form['prestar_atencion'];
            $qb->andWhere('h.hcDsmEdadSPadres = :hcDsmEdadSPadres');
            $qb->setParameter('hcDsmEdadSPadres', $valor);
        }
        if ($form['mantenerse_sentado']) {
            $valor = $form['mantenerse_sentado'];
            $qb->andWhere('h.hcDsmEdadMasen = :hcDsmEdadMasen');
            $qb->setParameter('hcDsmEdadMasen', $valor);
        }

        if ($form['responder_voz_padres']) {
            $valor = $form['responder_voz_padres'];
            $qb->andWhere('h.hcDsmEdadResponder = :hcDsmEdadResponder');
            $qb->setParameter('hcDsmEdadResponder', $valor);
        }
        if ($form['contacto_visual']) {
            $valor = $form['contacto_visual'];
            $qb->andWhere('h.hcDsmEdadPrimerasPalabras = :hcDsmEdadPrimerasPalabras');
            $qb->setParameter('hcDsmEdadPrimerasPalabras', $valor);
        }
        if ($form['primeros_pasos']) {
            $valor = $form['primeros_pasos'];
            $qb->andWhere('h.hcDsmEdadCaminar = :hcDsmEdadCaminar');
            $qb->setParameter('hcDsmEdadCaminar', $valor);
        }
        if ($form['senalar_objetos']) {
            $valor = $form['senalar_objetos'];
            $qb->andWhere('h.hcDsmEdadMasen = :hcDsmEdadMasen');
            $qb->setParameter('hcDsmEdadMasen', $valor);
        }
        if ($form['primeras_palabras']) {
            $valor = $form['primeras_palabras'];
            $qb->andWhere('h.hcDsmEdadJugar = :hcDsmEdadJugar');
            $qb->setParameter('hcDsmEdadJugar', $valor);
        }

        if ($form['imitar_acciones']) {
            $valor = $form['imitar_acciones'];
            $qb->andWhere('h.hcDsmEdadResponder = :hcDsmEdadResponder');
            $qb->setParameter('hcDsmEdadResponder', $valor);
        }
        if (array_key_exists('trastorno_autismo', $form)) {
            if ($form['trastorno_autismo'] != "") {
                $trastornos = $form['trastorno_autismo'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getTrastornosAutismo($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('discapacidad_intelectual', $form)) {
            if ($form['discapacidad_intelectual'] != "") {
                $trastornos = $form['discapacidad_intelectual'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getTrastornosDiscapacidad($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('enfermedad_psiquiatrica', $form)) {
            if ($form['enfermedad_psiquiatrica'] != "") {
                $trastornos = $form['enfermedad_psiquiatrica'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getEnfermPsiq($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('trastono_aprendizaje', $form)) {
            if ($form['trastono_aprendizaje'] != "") {
                $trastornos = $form['trastono_aprendizaje'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getTrastornoAprendizaje($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('trastono_deficit_atencion', $form)) {
            if ($form['trastono_deficit_atencion'] != "") {
                $trastornos = $form['trastono_deficit_atencion'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getTrastornosAtencion($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('trastono_conducta', $form)) {
            if ($form['trastono_conducta'] != "") {
                $trastornos = $form['trastono_conducta'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getTrastornoConducta($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('trastono_conducta', $form)) {
            if ($form['trastono_conducta'] != "") {
                $trastornos = $form['trastono_conducta'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getTrastornoConducta($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('trastorno_lenguaje', $form)) {
            if ($form['trastorno_lenguaje'] != "") {
                $trastornos = $form['trastorno_lenguaje'];
                foreach ($trastornos as $key => $value) {
                    $trastorno = HistoriaClinicaController::getTrastornoLenguaje($value);
                    $query = 'h.' . $trastorno . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if (array_key_exists('habito_autonomia', $form)) {
            if ($form['habito_autonomia'] != "") {
                $habitos = $form['habito_autonomia'];
                foreach ($habitos as $key => $value) {
                    $habito = HistoriaClinicaController::getHabitoAutonomia($value);
                    $query = 'h.' . $habito . ' = :valor';
                    $qb->andWhere($query);
                    $qb->setParameter('valor', 1);
                }
            }
        }
        if ($form['grado_escolar'] != "") {
            $qb->andWhere('p.gradoEscolar = :grado_escolar');
            $qb->setParameter('grado_escolar', $form['grado_escolar']);
        }
        $query = $qb->getQuery();
        $historiasClinicas = $query->getResult();
        $total = count($historiasClinicas);
        /*GENERAR PDF*/
        $date = date_create();
        $fecha = date_timestamp_get($date);
        $nombrePdf = $fecha.'-resul-filtro.pdf';
        $dirPdf =  $this->get('kernel')->getRootDir() . '/../web/pdf/hic/'.$nombrePdf;
        if($request->getHost() == 'localhost'){
            $httpDirPDf = 'http://localhost:8000/pdf/hic/'.$nombrePdf;
        }else{
            $httpDirPDf ='http://'.$request->getHost().'/hic/web/pdf/hic/'.$nombrePdf;
        }
        $this->get('knp_snappy.pdf')->generateFromHtml(
            $this->renderView(
                'MainBundle:Default:result_filtro_hic_pdf.html.twig',
                array(
                    'total'=>$total,
                    'historiasClinicas' => $historiasClinicas,
                    'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
                )
            ),$dirPdf

        );

        return $this->render('historiaclinica/resultado_busqueda_hc.html.twig', array(
            'historiasClinicas' => $historiasClinicas,
            'total' => $total,
            'httpDirPDf'=>$httpDirPDf
        ));
    }

    public static function getTrastornosAutismo($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAfTeaMama";
                break;
            case 1:
                return "hcAfTeaPapa";
                break;
            case 2:
                return "hcAfTeaHermano";
                break;

        }
    }

    public static function getTrastornosAtencion($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAfTpddaMama";
                break;
            case 1:
                return "hcAfTpddaPapa";
                break;
            case 2:
                return "hcAfTpddaHermano";
                break;

        }
    }

    public static function getTrastornosDiscapacidad($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAfDintlMama";
                break;
            case 1:
                return "hcAfDintlPapa";
                break;
            case 2:
                return "hcAfDintlHermano";
                break;

        }
    }

    public static function getEnfermPsiq($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAfEnfpsqMama";
                break;
            case 1:
                return "hcAfEnfpsqPapa";
                break;
            case 2:
                return "hcAfEnfpsqHermano";
                break;

        }
    }
    public static function getTrastornoAprendizaje($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAfTaprendMama";
                break;
            case 1:
                return "hcAfTaprendPapa";
                break;
            case 2:
                return "hcAfTaprenHermano";
                break;

        }
    }
    public static function getTrastornoConducta($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAfTccMama";
                break;
            case 1:
                return "hcAfTccPapa";
                break;
            case 2:
                return "hcAfTccHermano";
                break;

        }
    }
    public static function getTrastornoLenguaje($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAfTdlMama";
                break;
            case 1:
                return "hcAfTdlPapa";
                break;
            case 2:
                return "hcAfTdlHermano";
                break;

        }
    }
    public static function getHabitoAutonomia($valor)
    {
        switch ($valor) {
            case 0:
                return "hcAEasisteEscuela";
                break;
            case 1:
                return "hcAEapoyoCurricular";
                break;
            case 2:
                return "hcAEdesfasadoEdad";
                break;
            case 3:
                return "hcAEapoyoPsicopedagogico";
                break;
            case 4:
                return "hcAEmaestraSombra";
                break;
            case 5:
                return "hcAEparticipaManeraOrdinariaActividades";
                break;
            case 6:
                return "hcAEingregraGrupo";
                break;

        }
    }

    public function filtrarAction(Request $request)
    {
        array_map('unlink', glob($this->get('kernel')->getRootDir() . '/../web/pdf/hic/*.pdf'));

        $form = $this->createFormBuilder()
            ->add('nombre', 'text', array('required' => false))
            // ->add('apellidoMaterno', 'text', array('required'=>false))
            // ->add('apellidoMaterno', 'text', array('required'=>false))
            ->add('procedencia', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'Querétaro' => 'Querétaro',
                    'Colima' => 'Colima',
                    'Monterrey' => 'Monterrey',
                    'Veracruz' => 'Veracruz',
                    'Puebla' => 'Puebla',
                    'Aguascalientes' => 'Aguascalientes',
                    'Baja California' => 'Baja California',
                    'Baja California Sur' => 'Baja California Sur',
                    'Campeche' => 'Campeche',
                    'Chihuahua' => 'Chihuahua',
                    'Coahuila' => 'Coahuila',
                    'Chiapas' => 'Chiapas',
                    'Ciudad México' => 'Ciudad México',
                    'Durango' => 'Durango',
                    'Estado de México' => 'Estado de México',
                    'Guanajuato' => 'Guanajuato',
                    'Guerrero' => 'Guerrero',
                    'Hidalgo' => 'Hidalgo',
                    'Jalisco' => 'Jalisco',
                    'Michoacán' => 'Michoacán',
                    'Morelos' => 'Morelos',
                    'Nayarit' => 'Nayarit',
                    'Nuevo León' => 'Nuevo León',
                    'Quintana Roo' => 'Quintana Roo',
                    'San Luis Potosí' => 'San Luis Potosí',
                    'Sonora' => 'Sonora',
                    'Tabasco' => 'Tabasco',
                    'Tamaulipas' => 'Tamaulipas',
                    'Tlaxcala' => 'Tlaxcala',
                    'Yucatán' => 'Yucatán',
                    'Zacatecas' => 'Zacatecas',
                )))
            ->add('edad', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0-5',
                    '6' => '6-10',
                    '11' => '11-15',
                    '16' => '16-20',
                    '21' => '21-25',
                    '26' => '26-30',
                    '31' => '31-35',
                    '36' => '36-90',
                )
            ))
            ->add('sexo', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'masculino' => 'masculino',
                    'femenino' => 'femenino',
                )
            ))
            ->add('quien_remite', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'pediatra' => 'Pediatra',
                    'neuropediatra' => 'Neuropediatra',
                    'psicologo' => 'Psicólogo',
                    'psiquiatra' => 'Psiquiatra',
                    'terapia_lenguaje' => 'Terapia del Lenguaje',
                    'otro' => 'Otro',
                    'propia_familia' => 'Búsqueda de la propia familia',
                )
            ))
            ->add('antecendes_atencion', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'farmacosn_neurológicos' => 'Fármacos Neurológicos',
                    'terapia_de_lenguaje' => 'Terapia de lenguaje',
                    'apoyo_pedagógico' => 'Apoyo pedagógico',
                    'abordaje_nutricional' => 'Abordaje Nutricional',
                    'terapia_ocupacional' => 'Terapia Ocupacional',
                    'terapia_asistida_animales' => 'Terapia asistida por animales',
                    'terapias_sensoriales' => 'Terapias Sensoriales',
                    'estimulación_temprana' => 'Estimulación Temprana',
                    'psicomotricidad' => 'Psicomotricidad',
                    'apoyo_psicológico' => 'Apoyo Psicológico',
                    'otro' => 'Otro',
                )
            ))
            ->add('examenes', ChoiceType::class, array(
                'required' => false,
                //'multiple'=>true,
                'choices' => array(
                    'electroencefalograma' => 'Electroencefalograma',
                    'tac' => 'TAC',
                    'potenciales _evocados' => 'Potenciales Evocados',
                    'valoración_genética' => 'Valoración Genética',
                    'tamiz_metabólico' => 'Tamiz Metabólico',
                    'resonancia_magnética' => 'Resonancia Magnética',
                    'evaluación_neuropsicológica' => 'Evaluación Neuropsicológica',
                    'evaluación_lenguaje' => 'Evaluación del Lenguaje',
                    'otro' => 'Otro',
                )
            ))
            ->add('edad_mama', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '15' => '15-20',
                    '21' => '21-25',
                    '26' => '26-30',
                    '31' => '31-35',
                    '36' => '36-40',
                    '41' => '41-45',
                    '46' => '46-50',
                    '51' => '51-55',
                    '56' => '56-60',
                    '61' => '61-65',
                )
            ))
            ->add('edad_papa', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '15' => '15-20',
                    '21' => '21-25',
                    '26' => '26-30',
                    '31' => '31-35',
                    '36' => '36-40',
                    '41' => '41-45',
                    '46' => '46-50',
                    '51' => '51-55',
                    '56' => '56-60',
                    '61' => '61-65',
                )
            ))
            ->add('parto_fue', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'a_termino' => 'A término',
                    'natural' => 'Natural',
                    'cesarea' => 'Cesárea',
                    'prematuro' => 'Prematuro',
                    'post_termino' => 'Postermino',
                )
            ))
            ->add('llanto_fue', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'provocado' => 'Provocado',
                    'espontáneo' => 'Espontáneo',
                )
            ))
            ->add('peso_fue', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'bajo_peso' => 'Bajo peso',
                    'sobre_peso' => 'Sobre peso',
                    'normal' => 'Normal',
                )
            ))
            ->add('sufrimiento_fetal', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'si' => 'Si',
                    'no' => 'No',
                )
            ))
            ->add('requirio_en_parto', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'hospitalizacion' => 'Hospitalización',
                    'asistencia _respiratoria' => 'Asistencia Respiratoria',
                    'incubadora' => 'Incubadora',
                    'reanimacion' => 'Reanimación',
                    'tratamiento_farmacolpgico' => 'Tratamiento farmacológico',
                )
            ))
            ->add('sufrio_en_parto', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'accidentes' => 'Accidentes',
                    'enfermedad' => 'Enfermedad',
                    'toxicos' => 'Tuvo hábitos tóxicos (tabaco, alcohol, drogas) ',
                    'estres' => 'Tuvo estrés, ansiedad o depresión. ',
                    'intoxicaciones' => 'Tuvo intoxicaciones o alergias alimenticias. ',
                    'medicamento' => 'Tomó algún medicamento.',
                    'peso_adecuado' => 'Su peso era adecuado.',
                    'examenes_periodicos' => 'Le realizaron exámenes y revisiones periódicas. ',
                )
            ))
            /*caract desarrollo*/
            ->add('primer_ano_normal', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'si' => 'Si',
                    'no' => 'No',
                )
            ))
            ->add('sonreir_rostro', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('tomar_objetos_mano', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('prestar_atencion', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('manipular_objetos', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('mantenerse_sentado', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('responder_voz_padres', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('contacto_visual', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('primeros_pasos', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('senalar_objetos', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('primeras_palabras', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            ->add('imitar_acciones', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    '0' => '0 meses',
                    '1' => '1 mes',
                    '2' => '2 meses',
                    '3' => '3 meses',
                    '4' => '4 meses',
                    '5' => '5 meses',
                    '6' => '6 meses',
                    '7' => '7 meses',
                    '8' => '8 meses',
                    '9' => '9 meses',
                    '10' => '10 meses',
                    '11' => '11 meses',
                )
            ))
            /*antecedentes familiares*/
            ->add('trastorno_autismo', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Mamá',
                    '1' => 'Papá',
                    '2' => 'Hermano u otros',
                )
            ))
            ->add('trastorno_lenguaje', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Mamá',
                    '1' => 'Papá',
                    '2' => 'Hermano u otros',
                )
            ))
            ->add('discapacidad_intelectual', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Mamá',
                    '1' => 'Papá',
                    '2' => 'Hermano u otros',
                )
            ))
            ->add('enfermedad_psiquiatrica', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Mamá',
                    '1' => 'Papá',
                    '2' => 'Hermano u otros',
                )
            ))
            ->add('trastono_aprendizaje', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Mamá',
                    '1' => 'Papá',
                    '2' => 'Hermano u otros',
                )
            ))
            ->add('trastono_deficit_atencion', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Mamá',
                    '1' => 'Papá',
                    '2' => 'Hermano u otros',
                )
            ))
            ->add('trastono_conducta', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Mamá',
                    '1' => 'Papá',
                    '2' => 'Hermano u otros',
                )
            ))
            /*habito y autonomia*/
            ->add('habito_autonomia', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => 'Habito de sueño',
                    '1' => 'Habito alimenticio',
                    '2' => 'Hábitos Higiénicos',
                    '3' => 'Hábitos Inherentes al Control de Esfínteres',
                    '4' => 'Hábitos de Autocuidado',
                    '5' => 'Hábitos de Vestido',
                    '6' => 'Hábitos de Juego y Rutinas de Actividad',
                    '7' => 'Otros Hábitos',
                )
            ))
            /*antecedentes educativos*/
            ->add('habito_autonomia', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array(
                    '0' => '¿Asiste a la escuela?',
                    '1' => '¿Recibe o necesita algún tipo de apoyo curricular en el colegio?',
                    '2' => '¿Se encuentra desfasado con respecto a su edad?',
                    '3' => '¿Recibe apoyo psicopedagógico o de USAER en su colegio?
',
                    '4' => '¿Asiste con maestra sombra o auxiliar educativa?',
                    '5' => '¿Participa de manera ordinaria en todas las actividades y materias escolares? ',
                    '6' => '¿Se integra de manera funcional a su grupo de compañeros escolares?',
                )
            ))
            ->add('grado_escolar', ChoiceType::class, array(
                'required' => false,
                'choices' => array(
                    'maternal' => 'maternal',
                    '1erKinder' => '1erKinder',
                    '2doKinder' => '2doKinder',
                    '3roKinder' => '3roKinder',
                    '1ero' => '1ero',
                    '2do' => '2do',
                    '3ro' => '3ro',
                    '4to' => '4to',
                    '5to' => '5to',
                    '6to' => '6to',
                    '7mo' => '7mo',
                    '8vo' => '8vo',
                    '9no' => '9no',
                    '10mo' => '10mo',
                    '11no' => '11no',
                    '12vo' => '12vo',
                    'Universidad' => 'Universidad',
                )))
            ->getForm();
        return $this->render('historiaclinica/filtrar.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Instrucciones.
     *
     */
    public
    function instruccionesAction()
    {
        return $this->render('historiaclinica/instrucciones.html.twig');
    }

    /*Crear PDF*/

    public
    function createPdfAction(Request $request, HistoriaClinica $historiaClinica)
    {

        $html = $this->renderView('MainBundle:Default:hic_pdf.html.twig', array(
            'hic' => $historiaClinica,
            'base_dir' => $this->get('kernel')->getRootDir() . '/../web/pdf/hic/' . $request->getBasePath()
        ));
        $nombrePaciente = $historiaClinica->getPaciente()->getNombre();
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="historia_clinica_' . $nombrePaciente . '.pdf"'
            )
        );
    }
    /**
     * Creates a new historiaClinica entity.
     *
     */
    public
    function newAction(Request $request)
    {
        $historiaClinica = new Historiaclinica();
        $form = $this->createForm('MainBundle\Form\HistoriaClinicaType', $historiaClinica);
        $form->handleRequest($request);
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            //throw $this->createAccessDeniedException();
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $emails = ['edellopeza@yahoo.com', $user->getEmail()];
        } else {
            $emails = ['edellopeza@yahoo.com'];
        }


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($historiaClinica);
            $em->flush($historiaClinica);

            $html = $this->renderView('MainBundle:Default:hic_pdf.html.twig', array(
                'hic' => $historiaClinica,
                'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
            ));
            $date = date_create();
            $fecha = date_timestamp_get($date);
            $url = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath() . '/' . $fecha . 'pdf_hic.pdf';
            $this->get('knp_snappy.pdf')->generateFromHtml($html, $url);
            /*enviar email*/
            $message = \Swift_Message::newInstance()
                ->setSubject('Registro de Historia Clinica')
                ->setFrom('reddiversidadstats@gmail.com')
                ->setTo($emails)
                ->setBody($this->renderView('MainBundle:Default:email.txt.twig',
                    array(
                        'paciente' => $historiaClinica->getPaciente()->getNombre(),
                        'apellidoPaterno' => $historiaClinica->getPaciente()->getApellidoPaterno(),
                        'apellidoMaterno' => $historiaClinica->getPaciente()->getApellidoMaterno(),
                        'ciudad' => $historiaClinica->getPaciente()->getCiudad(),
                    )
                ));
            $message->attach(\Swift_Attachment::fromPath($url));
            $this->get('mailer')->send($message);


            return $this->redirectToRoute('historiaclinica_fin');
        }

        return $this->render('historiaclinica/new.html.twig', array(
            'historiaClinica' => $historiaClinica,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a historiaClinica entity.
     *
     */
    public
    function showAction(HistoriaClinica $historiaClinica)
    {
        $deleteForm = $this->createDeleteForm($historiaClinica);

        return $this->render('historiaclinica/show.html.twig', array(
            'hic' => $historiaClinica,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing historiaClinica entity.
     *
     */
    public
    function editAction(Request $request, HistoriaClinica $historiaClinica)
    {
        $deleteForm = $this->createDeleteForm($historiaClinica);
        $editForm = $this->createForm('MainBundle\Form\HistoriaClinicaType', $historiaClinica);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('historiaclinica_edit', array('id' => $historiaClinica->getId()));
        }

        return $this->render('historiaclinica/edit.html.twig', array(
            'historiaClinica' => $historiaClinica,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a historiaClinica entity.
     *
     */
    public
    function deleteAction(Request $request, HistoriaClinica $historiaClinica)
    {
        $form = $this->createDeleteForm($historiaClinica);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($historiaClinica);
            $em->flush($historiaClinica);
        }

        return $this->redirectToRoute('historiaclinica_index');
    }

    /**
     * Creates a form to delete a historiaClinica entity.
     *
     * @param HistoriaClinica $historiaClinica The historiaClinica entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createDeleteForm(HistoriaClinica $historiaClinica)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('historiaclinica_delete', array('id' => $historiaClinica->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
