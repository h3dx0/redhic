<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
class HistoriaClinicaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder//->add('fechaCreacion')//->add('hcDgnEdad',null,array('label'=>'Label'))
        ->add('hcDgnSexo',ChoiceType::class, array(
                'choices'  => array(
                    'Masculino' => 'masculino',
                    'Femenino' => 'femenino',
                )))
        //->add('hcDgnEdadMadre')
       // ->add('hcDgnEdadPadre')
        //->add('hcDgnEdadTutor')
        ->add('hcRemColegio')
        ->add('hcRemNenopediatra')
        ->add('hcRemPsiquiatra')
        ->add('hcRemPediatra')
        //->add('hcRemFamiliar')
        ->add('hcRemBusqueda')
        ->add('hcRemOtro')
        ->add('hcRemTerapiaLenguaje')
        ->add('hcRemPsicologo')
       
        ->add('hcAdaFarmacosNeurologicos')
        ->add('hcAdaTerapiaLenguaje')
        ->add('hcAdaApoyoPedagogico')
        ->add('hcAdaAbordajeNutricional')
        ->add('hcAdaTerapiaOcupacional')
        ->add('hcAdaTerapiaAsistAnimales')
        ->add('hcAdaTerapiasSensoriales')
        ->add('hcAdaEstimulacionTemprana')
        ->add('hcAdaPsicomotricidad')
        ->add('hcAdaApoyoPsicologico')
        ->add('hcAdaOtro')
        ->add('descripcionAntecedentesAtencion')
       
        ->add('hcExaElectroencefalograma')
        ->add('hcExaTac')
        ->add('hcExaPotencialesEmocados')
        ->add('hcExaValoracionGenetica')
        ->add('hcTamizMetabolico')
        ->add('hcExaResonanciaMagnetica')
        ->add('hsEvalNenopsicologica')
        ->add('hcExaEvalLenguaje')
        ->add('hcExaOtra')
        ->add('descripcionAntecedentesAtencionExamenes')
        
        ->add('hcEmbEdadMama')
        ->add('hcEmbEdadPapa')
        //->add('hcEmbAbortosMama')
        ->add('hcEmbAcgMama')
        ->add('hcEmbEdembMama')
        ->add('hcEmbHtoxMama')
        ->add('hcEmbEadMama')
        ->add('hcEmbIaalimMama')
        ->add('hcEmbMedicamentosMama')
        ->add('hcEmbPesoOkMama')
        ->add('hcEmbExperiodMama')

        ->add('hcNacpPartoNatural')
        ->add('hcNacpPartoCesarea')
        ->add('hcNacpPartoTermino')
        ->add('hcNacpPartoPretermino')
        ->add('hcNacpPartoPostermino')
        ->add('hcNacpLlantoEspontaneo')
        ->add('hcNacpLlantoProvocado')
        ->add('hcNacpPesoNormal')
        ->add('hcNacpPesoBajo')
        ->add('hcNacpPesoSobre')
        ->add('hcNacpSufrimientoFetal')
        //->add('hcNacpSufrimientoFetalDesc')
       
        ->add('hcCdesTnpav')
        //->add('hcCdesEdadCca')
        ->add('hcDsmEdadSrca')
        ->add('hcDsmEdadSPadres')
        ->add('hcDsmEdadTomarObj')
        ->add('hcDsmEdadMobjfunc')
        ->add('hcDsmEdadGatear')
        ->add('hcDsmEdadCaminar')
        ->add('hcDsmEdadPrimerasPalabras')
        ->add('hcDsmEdadSoaint')
        ->add('hcDsmEdadMasen')
        ->add('hcDsmEdadJugar')
        ->add('hcDsmEdadResponder')
        ->add('hcHesMedicamentos')
        ->add('hcAfTeaMama')
        ->add('hcAfTeaPapa')
        ->add('hcAfTeaHermano')
        ->add('hcAfTdlMama')
        ->add('hcAfTdlPapa')
        ->add('hcAfTdlHermano')
        ->add('hcAfDintlMama')
        ->add('hcAfDintlPapa')
        ->add('hcAfDintlHermano')
        ->add('hcAfEnfpsqMama')
        ->add('hcAfEnfpsqPapa')
        ->add('hcAfEnfpsqHermano')
        ->add('hcAfTaprendMama')
        ->add('hcAfTaprendPapa')
        ->add('hcAfTaprenHermano')
        ->add('hcAfTpddaMama')
        ->add('hcAfTpddaPapa')
        ->add('hcAfTpddaHermano')
        ->add('hcAfTccMama')
        ->add('hcAfTccPapa')
        ->add('hcAfTccHermano')
        ->add('descripcionAntecedentesFamiliares')
        /*habitos y autonomia*/

        ->add('hcHabAutSueno')
        ->add('hcHabAutSuenoDescripcion')        
        ->add('hcHabAutAlimenticio')
        ->add('hcHabAutAlimenticioDesc')
        ->add('hcHabAutHigienicos')
        ->add('hcHabAutHigienicosDesc')
        ->add('hcHabAutEsfinteres')        
        ->add('hcHabAutEsfinteresDesc')        
        ->add('hcHabAutAutocuidado')
        ->add('hcHabAutAutocuidadoDesc')
        ->add('hcHabAutVestido')
        ->add('hcHabAutVestidoDesc')
        ->add('hcHabAutJuegoRutinas')
        ->add('hcHabAutJuegoRutinasDesc')
        ->add('hcHabAutOtros')
        ->add('hcHabAutOtrosDesc')
        /*fin habitos y autonomia*/
        //->add('hcHiInstitucionesEducativas')
        /*enfermedades*/

        /*fin enfermedades*/
        /*antecendentes educativos*/
        ->add('hcAEasisteEscuela')
        ->add('hcAEapoyoCurricular')
        ->add('hcAEdesfasadoEdad')
        ->add('hcAEapoyoPsicopedagogico')
        ->add('hcAEmaestraSombra')
        ->add('hcAEparticipaManeraOrdinariaActividades')
         ->add('hcAEingregraGrupo')
        ->add('hcAEDescripcionExperiencias')

        /*fin AE*/
        ->add('hcNacpHospitalizacion')
        ->add('hcNacpAsistenciaRespiratoria')
        ->add('hcNacpIncubadora')
        ->add('hcNacpReanimacion')
        ->add('hcNacpTratFarmacologico')
        ->add('hcDescripcionFinal')
        
        //->add('hcEstadoSaludActual')
        //->add('hcHiCentrosEducativosDesc')
       // ->add('antecedentesFamiliaresOtro', new \MainBundle\Form\AntecedentesFamiliaresOtroType())
        ->add('caracteristicasDesarrollo',new \MainBundle\Form\CaracteristicasDesarrolloType())
       // ->add('datosCentroTrabajo',new \MainBundle\Form\DatosCentroTrabajoType())

       // ->add('DatosFamiliaresHermano',new \MainBundle\Form\DatosFamiliaresHermanoType())

        ->add('DatosFamiliaresMadre',new \MainBundle\Form\DatosFamiliaresMadreType())

        ->add('DatosFamiliaresPadre',new \MainBundle\Form\DatosFamiliaresPadreType())

      //  ->add('descripcionAntecedentesAtencion',new \MainBundle\Form\DescripcionAntecedentesAtencionType())

       ->add('descripcionEmbarazo')
       ->add('profesionalAtiende')
       ->add('estadoCaso')

        //->add('habitosHijo',new \MainBundle\Form\HabitosHijoType())

        ->add('MotivoConsulta',new \MainBundle\Form\MotivoConsultaType())

        ->add('nacimientoParto',new \MainBundle\Form\NacimientoPartoType())

        ->add('paciente',new \MainBundle\Form\PacienteType())
        ->add('estado_salud_enfermedades',new \MainBundle\Form\EstadoSaludEnfermedadesType())

        //->add('tratamientoMedico',new \MainBundle\Form\TratamientoMedicoType())   

    //     ->add('historiaEstadoSalud', CollectionType::class, array(
    // // each entry in the array will be an "email" field
    //             'entry_type'   => \MainBundle\Form\HistoriaEstadoSaludType::class,
    //             'allow_add'    => true,
    //             ))
       ;        
        }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\HistoriaClinica'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_historiaclinica';
    }


}
