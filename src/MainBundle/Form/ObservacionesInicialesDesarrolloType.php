<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObservacionesInicialesDesarrolloType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('usoMateriales')->add('funcionesComunicacion')->add('lenguaje')->add('adecuacionComportamental')->add('relacionFamilia')->add('habilidadesCognitivas')->add('conductaAutonomia')->add('aspectosMotores')->add('otros')        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\ObservacionesInicialesDesarrollo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_observacionesinicialesdesarrollo';
    }


}
