<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProtocoloEvaluacionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('formatoAtencionTemprana')
->add('evaluacionDesarrollo')
->add('neuropsicologia')
->add('protocolaTea')
->add('observacionEscolar')
->add('comunicacionLenguaje')
->add('inteligencia')
->add('habilidadesSociales')
->add('protocoloTda')
->add('emocionales')
->add('motricidad')
->add('pedagogiaAcademica')
->add('autonomia')
->add('protocoloAprendizaje')
->add('sensoriales')        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\ProtocoloEvaluacion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_protocoloevaluacion';
    }


}
