<?php

namespace MainBundle\Form;

use MainBundle\MainBundle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
class CasoNuevoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre')
        ->add('apellidoPaterno')
        ->add('apellidoMaterno')
        ->add('edad', HiddenType::class, array('required'=>false))
        ->add('procedencia', ChoiceType::class, array(
                'required'=>false,
               'choices'  => array(
                   'Querétaro' => 'Querétaro',
                   'Colima' => 'Colima',
                   'Monterrey' => 'Monterrey',
                   'Veracruz' => 'Veracruz',
                   'Puebla' => 'Puebla',
                   'Aguascalientes' => 'Aguascalientes',
                   'Baja California' => 'Baja California',
                   'Baja California Sur' => 'Baja California Sur',
                   'Campeche' => 'Campeche',
                   'Chihuahua' => 'Chihuahua',
                   'Coahuila' => 'Coahuila',
                   'Chiapas' => 'Chiapas',
                   'Ciudad México' => 'Ciudad México',
                   'Durango' => 'Durango',
                   'Estado de México' => 'Estado de México',
                   'Guanajuato' => 'Guanajuato',
                   'Guerrero' => 'Guerrero',
                   'Hidalgo' => 'Hidalgo',
                   'Jalisco' => 'Jalisco',
                   'Michoacán' => 'Michoacán',
                   'Morelos' => 'Morelos',
                   'Nayarit' => 'Nayarit',
                   'Nuevo León' => 'Nuevo León',
                   'Quintana Roo' => 'Quintana Roo',
                   'San Luis Potosí' => 'San Luis Potosí',
                   'Sonora' => 'Sonora',
                   'Tabasco' => 'Tabasco',
                   'Tamaulipas' => 'Tamaulipas',
                   'Tlaxcala' => 'Tlaxcala',
                   'Yucatán' => 'Yucatán',
                   'Zacatecas' => 'Zacatecas',
               )))
        ->add('nombreMama')
        ->add('telefonoMama')
        ->add('emailMama')
        ->add('nombrePapa')
        ->add('telefonoPapa')
        ->add('emailPapa')
        ->add('fechaRegistroCaso')
       ->add('fechaNacimiento', DateType::class,array(
                'years'=>array(
                    '1980',
                    '1981',
                    '1982',
                    '1983',
                    '1984',
                    '1985',
                    '1986',
                    '1987',
                    '1988',
                    '1989',
                    '1990',
                    '1991',
                    '1992',
                    '1993',
                    '1994',
                    '1995',
                    '1996',
                    '1997',
                    '1998',
                    '1999',
                    '2000',
                    '2001',
                    '2002',
                    '2003',
                    '2004',
                    '2005',
                    '2006',
                    '2007',
                    '2008',
                    '2009',
                    '2010',
                    '2011',
                    '2012',
                    '2013',
                    '2014',
                    '2015',
                    '2016',
                    '2017',
                    '2018',
                    '2019',
                    '2020',
                    '2021',
                    ),
                //'months'=>array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'),
                'choice_translation_domain'=>true
            ))
        ->add('nombreEspecialistaRemision')
        ->add('tipoEspecialistaRemision', ChoiceType::class, array(
                'choices'  => array(
                    'escuela' => 'escuela',
                    'otrafamilia' => 'Otra Familia',
                    'neuropediatra' => 'neuropediatra',
                    'Paidopsiquiatra' => 'Paidopsiquiatra',
                    'tlenguaje' => 'T. Lenguaje',
                    'pediatra' => 'Pediatra',
                    'fisioterapia' => 'Fisioterapia',
                    'otro' => 'otro',

                )))
        ->add('sexo',ChoiceType::class, array(
           'choices'  => array(
                'masculino' => 'masculino',
                'femenino' => 'femenino',
            )))
        ->add('motivoConsulta')
        ->add('derivacionesInterconsultas')
        ->add('especificacionesEvaluativas')
        ->add('comentariosFinales')
        ->add('escenarioAtencionSugeridos', new \MainBundle\Form\EscenarioAntecionSugeridosType())
        ->add('hipotesisDiagnostica', new \MainBundle\Form\HipotesisDiagnosticaType())
        ->add('servicioAtencionSugerido', new \MainBundle\Form\ServicioAtencionSugeridoType())
        ->add('propuestaAtencion')
        ->add('especialista')
        ->add('antecedentesEvaluativos', new \MainBundle\Form\AntecedentesEvaluativosType())
        ->add('antecedentes', new \MainBundle\Form\AntecedentesType())
        ->add('observacionesInicialesDesarrollo', new \MainBundle\Form\ObservacionesInicialesDesarrolloType())
        ->add('protocoloEvaluacion', new \MainBundle\Form\ProtocoloEvaluacionType())
         ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\CasoNuevo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_casonuevo';
    }


}
