<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
class PacienteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre')->add('apellidoPaterno')->add('apellidoMaterno')
        ->add('fechaNacimiento', DateType::class,array(
                'years'=>array(
                    '1980',
                    '1981',
                    '1982',
                    '1983',
                    '1984',
                    '1985',
                    '1986',
                    '1987',
                    '1988',
                    '1989',
                    '1990',
                    '1991',
                    '1992',
                    '1993',
                    '1994',
                    '1995',
                    '1996',
                    '1997',
                    '1998',
                    '1999',
                    '2000',
                    '2001',
                    '2002',
                    '2003',
                    '2004',
                    '2005',
                    '2006',
                    '2007',
                    '2008',
                    '2009',
                    '2010',
                    '2011',
                    '2012',
                    '2013',
                    '2014',
                    '2015',
                    '2016',
                    '2017',
                    '2018',
                    '2019',
                    '2020',
                    '2021',
                    ),
                //'months'=>array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'),
                'choice_translation_domain'=>true
            ))
        ->add('edad',ChoiceType::class, array(
                'choices'  => array(
                    '0' => 0,
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    '10' => 10,
                    '11' => 11,
                    '12' => 12,
                    '13' => 13,
                    '14' => 14,
                    '15' => 15,
                    '16' => 16,
                    '17' => 17,
                    '18' => 18,
                    '19' => 19,
                    '20' => 20,
                    '21' => 21,
                    '22' => 22,
                    '23' => 23,
                    '24' => 24,
                    '25' => 25,
                    '26' => 26,
                    '27' => 27,
                    '28' => 28,
                    '29' => 29,
                    '30' => 30,
                    '31' => 31,
                    '32' => 32,
                    '33' => 33,
                    '34' => 34,
                    '35' => 35,
                    '36' => 36,
                    '37' => 37,
                    '38' => 38,
                    '39' => 39,
                    '40' => 40,
                    '41' => 41,
                    '42' => 42,
                    '43' => 43,
                    '44' => 44,
                    '45' => 45,
                    '46' => 46,
                    '47' => 47,
                    '48' => 48,
                    '49' => 49,
                    '50' => 50,
                )))
        ->add('telefonoEscuela')->add('gradoEscolar',ChoiceType::class, array(
                'choices'  => array(
                    'maternal' => 'maternal',
                    '1erKinder' => '1erKinder',
                    '2doKinder' => '2doKinder',
                    '3roKinder' => '3roKinder',
                    '1ero' => '1ero',
                    '2do' => '2do',
                    '3ro' => '3ro',
                    '4to' => '4to',
                    '5to' => '5to',
                    '6to' => '6to',
                    '7mo' => '7mo',
                    '8vo' => '8vo',
                    '9no' => '9no',
                    '10mo' => '10mo',
                    '11no' => '11no',
                    '12vo' => '12vo',
                    'Universidad' => 'Universidad',
                )))
         ->add('nombreEscuela') ->add('ciudad',ChoiceType::class, array(
                'choices'  => array(
                    'Querétaro' => 'Querétaro',
                    'Colima' => 'Colima',
                    'Monterrey' => 'Monterrey',
                    'Veracruz' => 'Veracruz',
                    'Puebla' => 'Puebla',
                    'Aguascalientes' => 'Aguascalientes',
                    'Baja California' => 'Baja California',
                    'Baja California Sur' => 'Baja California Sur',
                    'Campeche' => 'Campeche',
                    'Chihuahua' => 'Chihuahua',
                    'Coahuila' => 'Coahuila',
                    'Chiapas' => 'Chiapas',
                    'Ciudad México' => 'Ciudad México',
                    'Durango' => 'Durango',
                    'Estado de México' => 'Estado de México',
                    'Guanajuato' => 'Guanajuato',
                    'Guerrero' => 'Guerrero',
                    'Hidalgo' => 'Hidalgo',
                    'Jalisco' => 'Jalisco',
                    'Michoacán' => 'Michoacán',
                    'Morelos' => 'Morelos',
                    'Nayarit' => 'Nayarit',
                    'Nuevo León' => 'Nuevo León',
                    'Quintana Roo' => 'Quintana Roo',
                    'San Luis Potosí' => 'San Luis Potosí',
                    'Sonora' => 'Sonora',
                    'Tabasco' => 'Tabasco',
                    'Tamaulipas' => 'Tamaulipas',
                    'Tlaxcala' => 'Tlaxcala',
                    'Yucatán' => 'Yucatán',
                    'Zacatecas' => 'Zacatecas',
                )))       ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Paciente'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_paciente';
    }


}
